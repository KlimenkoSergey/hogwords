<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "words_pars_google".
 *
 * @property int $id
 * @property string $text_page
 * @property int $word_id
 *
 * @property Words $word
 */
class WordsParsGoogle extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'words_pars_google';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['text_page', 'word_id'], 'required'],
            [['text_page'], 'string'],
            [['word_id'], 'integer'],
            [['word_id'], 'exist', 'skipOnError' => true, 'targetClass' => Words::className(), 'targetAttribute' => ['word_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'text_page' => 'Text Page',
            'word_id' => 'Word ID',
        ];
    }

    /**
     * Gets query for [[Word]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getWord()
    {
        return $this->hasOne(Words::className(), ['id' => 'word_id']);
    }
}
