<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "translate".
 *
 * @property int $id
 * @property int $word_id
 * @property string $value
 * @property int $parts_of_speech_id
 * @property int|null $sort
 *
 * @property Examples[] $examples
 * @property PartsOfSpeech $partsOfSpeech
 * @property Words $word
 */
class Translate extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'translate';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['word_id', 'value', 'parts_of_speech_id'], 'required'],
            [['word_id', 'parts_of_speech_id', 'sort'], 'integer'],
            [['value'], 'string', 'max' => 255],
            [['parts_of_speech_id'], 'exist', 'skipOnError' => true, 'targetClass' => PartsOfSpeech::className(), 'targetAttribute' => ['parts_of_speech_id' => 'id']],
            [['word_id'], 'exist', 'skipOnError' => true, 'targetClass' => Words::className(), 'targetAttribute' => ['word_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'word_id' => 'Word ID',
            'value' => 'Value',
            'parts_of_speech_id' => 'Parts Of Speech ID',
            'sort' => 'Sort',
        ];
    }

    /**
     * Gets query for [[Examples]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getExamples()
    {
        return $this->hasMany(Examples::className(), ['translate_id' => 'id']);
    }

    /**
     * Gets query for [[PartsOfSpeech]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPartsOfSpeech()
    {
        return $this->hasOne(PartsOfSpeech::className(), ['id' => 'parts_of_speech_id']);
    }

    /**
     * Gets query for [[Word]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getWord()
    {
        return $this->hasOne(Words::className(), ['id' => 'word_id']);
    }
}
