<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "playlistwords".
 *
 * @property int $id
 * @property int $wordId
 * @property int $playListId
 * @property int $sort
 *
 * @property PlayList $playList
 * @property Words $word
 */
class Playlistwords extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'playlistwords';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['wordId', 'playListId'], 'required'],
            [['wordId', 'playListId', 'sort'], 'integer'],
            [['playListId'], 'exist', 'skipOnError' => true, 'targetClass' => PlayList::className(), 'targetAttribute' => ['playListId' => 'id']],
            [['wordId'], 'exist', 'skipOnError' => true, 'targetClass' => Words::className(), 'targetAttribute' => ['wordId' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'wordId' => 'Word ID',
            'playListId' => 'Play List ID',
            'sort' => 'Сортировка',
        ];
    }

    /**
     * Gets query for [[PlayList]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPlayList()
    {
        return $this->hasOne(PlayList::className(), ['id' => 'playListId']);
    }

    /**
     * Gets query for [[Word]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getWord()
    {
        return $this->hasOne(Words::className(), ['id' => 'wordId']);
    }
}
