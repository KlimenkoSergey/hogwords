<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "commets_soc_seti".
 *
 * @property int $id
 * @property string $comments
 * @property string|null $input1
 * @property int|null $input2
 */
class CommetsSocSeti extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'commets_soc_seti';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['comments'], 'required'],
            [['comments', 'input1'], 'string'],
            [['input2'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'comments' => 'Комментарий',
            'input1' => 'Пол',
            'input2' => 'Input2',
        ];
    }
}
