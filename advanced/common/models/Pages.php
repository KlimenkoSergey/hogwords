<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "pages".
 *
 * @property int $id
 * @property string $name
 * @property string $title
 * @property string $alias
 * @property string|null $content
 * @property string|null $metaKeywords
 * @property string|null $metaDescription
 * @property string $createdAt
 * @property string|null $updatedAt
 */
class Pages extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pages';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'title', 'alias', 'createdAt'], 'required'],
            [['content'], 'string'],
            [['createdAt', 'updatedAt'], 'safe'],
            [['name', 'title', 'alias'], 'string', 'max' => 255],
            [['metaKeywords', 'metaDescription'], 'string', 'max' => 300],
            [['alias'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'title' => 'Title',
            'alias' => 'Alias',
            'content' => 'Content',
            'metaKeywords' => 'Meta Keywords',
            'metaDescription' => 'Meta Description',
            'createdAt' => 'Created At',
            'updatedAt' => 'Updated At',
        ];
    }
}
