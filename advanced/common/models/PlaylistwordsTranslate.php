<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "playlistwords_translate".
 *
 * @property int $id
 * @property int $playlistwords_id
 * @property int $translate_id
 * @property int $sort
 */
class PlaylistwordsTranslate extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'playlistwords_translate';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['playlistwords_id', 'translate_id', 'sort'], 'required'],
            [['playlistwords_id', 'translate_id', 'sort'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'playlistwords_id' => 'Playlistwords ID',
            'translate_id' => 'Translate ID',
            'sort' => 'Sort',
        ];
    }
}
