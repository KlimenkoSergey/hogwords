<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "payment_history".
 *
 * @property int $id
 * @property int $user_id
 * @property float $amount
 * @property int $kassa_id
 * @property int $created_at
 */
class PaymentHistory extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'payment_history';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'amount', 'kassa_id', 'created_at'], 'required'],
            [['user_id', 'kassa_id', 'created_at'], 'integer'],
            [['amount'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'amount' => 'Amount',
            'kassa_id' => 'Kassa ID',
            'created_at' => 'Created At',
        ];
    }
}
