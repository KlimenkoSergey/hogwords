<?php

namespace common\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\CommetsSocSeti;

/**
 * CommetsSocSetiSearch represents the model behind the search form of `common\models\CommetsSocSeti`.
 */
class CommetsSocSetiSearch extends CommetsSocSeti
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'input2'], 'integer'],
            [['comments', 'input1'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CommetsSocSeti::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'input2' => $this->input2,
        ]);

        $query->andFilterWhere(['like', 'comments', $this->comments])
            ->andFilterWhere(['like', 'input1', $this->input1]);

        return $dataProvider;
    }
}
