<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "words".
 *
 * @property int $id
 * @property int|null $sort
 * @property string $word
 * @property string|null $transcription
 * @property string|null $video
 * @property int $status
 * @property string|null $translate
 */
class Words extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'words';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sort', 'status'], 'integer'],
            [['word'], 'required'],
            [['word', 'transcription', 'translate'], 'string', 'max' => 500],
            [['video'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sort' => 'Sort',
            'word' => 'Word',
            'transcription' => 'Transcription',
            'video' => 'Video',
            'status' => 'Status',
            'translate' => 'Translate',
        ];
    }

    /**
     * Gets query for [[PartsOfSpeechWords]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPartsOfSpeechWords()
    {
        return $this->hasMany(PartsOfSpeechWords::className(), ['word_id' => 'id']);
    }

    /**
     * Gets query for [[Translates]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTranslates()
    {
        return $this->hasMany(Translate::className(), ['word_id' => 'id']);
    }


}
