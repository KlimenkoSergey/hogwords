<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property string $username
 * @property string $auth_key
 * @property string $password_hash
 * @property string|null $password_reset_token
 * @property string|null $email
 * @property int $status
 * @property int $created_at
 * @property int $updated_at
 * @property string|null $verification_token
 * @property string|null $identity
 * @property string|null $network
 * @property string|null $full_name
 * @property float $balance_user
 * @property int $lists_limit
 * @property int $words_limit
 * @property int $translates_limit
 * @property int|null $subscription_int
 * @property int|null $status_account
 */
class UserOptions extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['username', 'auth_key', 'password_hash', 'created_at', 'updated_at'], 'required'],
            [['status', 'created_at', 'updated_at', 'lists_limit', 'words_limit', 'translates_limit', 'subscription_int', 'status_account'], 'integer'],
            [['balance_user'], 'number'],
            [['username', 'password_hash', 'password_reset_token', 'email', 'verification_token', 'identity', 'network', 'full_name'], 'string', 'max' => 255],
            [['auth_key'], 'string', 'max' => 32],
            [['username'], 'unique'],
            [['password_reset_token'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'auth_key' => 'Auth Key',
            'password_hash' => 'Password Hash',
            'password_reset_token' => 'Password Reset Token',
            'email' => 'Email',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'verification_token' => 'Verification Token',
            'identity' => 'Identity',
            'network' => 'Network',
            'full_name' => 'Full Name',
            'balance_user' => 'Balance User',
            'lists_limit' => 'Lists Limit',
            'words_limit' => 'Words Limit',
            'translates_limit' => 'Translates Limit',
            'subscription_int' => 'Subscription Int',
            'status_account' => 'Status Account',
        ];
    }
}
