<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "exercises_options".
 *
 * @property int $id
 * @property int $type_exercises
 * @property int $correct_word
 * @property int|null $sort
 * @property int|null $other_word1
 * @property int|null $other_word2
 * @property int|null $other_word3
 */
class ExercisesOptions extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'exercises_options';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['type_exercises', 'correct_word'], 'required'],
            [['type_exercises', 'correct_word', 'sort', 'other_word1', 'other_word2', 'other_word3'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type_exercises' => 'Type Exercises',
            'correct_word' => 'Correct Word',
            'sort' => 'Sort',
            'other_word1' => 'Other Word1',
            'other_word2' => 'Other Word2',
            'other_word3' => 'Other Word3',
        ];
    }
}
