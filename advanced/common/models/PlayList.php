<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "play_list".
 *
 * @property int $id
 * @property int $user_id
 * @property string $name
 * @property int|null $sort
 * @property string|null $image_list
 */
class PlayList extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'play_list';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'name'], 'required'],
            [['user_id', 'sort'], 'integer'],
            [['name', 'image_list'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'name' => 'Name',
            'sort' => 'Sort',
            'image_list' => 'Image List',
        ];
    }
}
