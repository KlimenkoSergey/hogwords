<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "examples".
 *
 * @property int $id
 * @property int $translate_id
 * @property string $en_text
 * @property string $ru_text
 * @property int|null $sort
 *
 * @property Translate $translate
 */
class Examples extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'examples';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['translate_id', 'en_text', 'ru_text'], 'required'],
            [['translate_id', 'sort'], 'integer'],
            [['en_text', 'ru_text'], 'string'],
            [['translate_id'], 'exist', 'skipOnError' => true, 'targetClass' => Translate::className(), 'targetAttribute' => ['translate_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'translate_id' => 'Translate ID',
            'en_text' => 'En Text',
            'ru_text' => 'Ru Text',
            'sort' => 'Sort',
        ];
    }

    /**
     * Gets query for [[Translate]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTranslate()
    {
        return $this->hasOne(Translate::className(), ['id' => 'translate_id']);
    }
}
