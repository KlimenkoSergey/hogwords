<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "words_conts_translates".
 *
 * @property int $id
 * @property int $word_id
 * @property int $count_translates
 */
class WordsContsTranslates extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'words_conts_translates';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['word_id', 'count_translates'], 'required'],
            [['word_id', 'count_translates'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'word_id' => 'Word ID',
            'count_translates' => 'Count Translates',
        ];
    }
}
