<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "irregular_verbs_words".
 *
 * @property int $id
 * @property int $irregular_verbs_id
 * @property int $word_id
 *
 * @property IrregularVerbs $irregularVerbs
 * @property Words $word
 */
class IrregularVerbsWords extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'irregular_verbs_words';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['irregular_verbs_id', 'word_id'], 'required'],
            [['irregular_verbs_id', 'word_id'], 'integer'],
            [['irregular_verbs_id'], 'exist', 'skipOnError' => true, 'targetClass' => IrregularVerbs::className(), 'targetAttribute' => ['irregular_verbs_id' => 'id']],
            [['word_id'], 'exist', 'skipOnError' => true, 'targetClass' => Words::className(), 'targetAttribute' => ['word_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'irregular_verbs_id' => 'Irregular Verbs ID',
            'word_id' => 'Word ID',
        ];
    }

    /**
     * Gets query for [[IrregularVerbs]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIrregularVerbs()
    {
        return $this->hasOne(IrregularVerbs::className(), ['id' => 'irregular_verbs_id']);
    }

    /**
     * Gets query for [[Word]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getWord()
    {
        return $this->hasOne(Words::className(), ['id' => 'word_id']);
    }
}
