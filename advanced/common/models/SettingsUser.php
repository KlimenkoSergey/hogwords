<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "settings_user".
 *
 * @property int $id
 * @property string $name_param
 * @property string $value_param
 * @property int $user_id
 */
class SettingsUser extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'settings_user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name_param', 'value_param', 'user_id'], 'required'],
            [['user_id'], 'integer'],
            [['name_param', 'value_param'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name_param' => 'Name Param',
            'value_param' => 'Value Param',
            'user_id' => 'User ID',
        ];
    }
}
