<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "parts_of_speech".
 *
 * @property int $id
 * @property string $value
 *
 * @property PartsOfSpeechWords[] $partsOfSpeechWords
 * @property Translate[] $translates
 */
class PartsOfSpeech extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'parts_of_speech';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['value'], 'required'],
            [['value'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'value' => 'Value',
        ];
    }

    /**
     * Gets query for [[PartsOfSpeechWords]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPartsOfSpeechWords()
    {
        return $this->hasMany(PartsOfSpeechWords::className(), ['parts_of_speech_id' => 'id']);
    }

    /**
     * Gets query for [[Translates]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTranslates()
    {
        return $this->hasMany(Translate::className(), ['parts_of_speech_id' => 'id']);
    }
}
