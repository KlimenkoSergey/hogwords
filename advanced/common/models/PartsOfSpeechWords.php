<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "parts_of_speech_words".
 *
 * @property int $id
 * @property int $word_id
 * @property int $parts_of_speech_id
 * @property int|null $sort
 *
 * @property PartsOfSpeech $partsOfSpeech
 * @property Words $word
 */
class PartsOfSpeechWords extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'parts_of_speech_words';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['word_id', 'parts_of_speech_id'], 'required'],
            [['word_id', 'parts_of_speech_id', 'sort'], 'integer'],
            [['parts_of_speech_id'], 'exist', 'skipOnError' => true, 'targetClass' => PartsOfSpeech::className(), 'targetAttribute' => ['parts_of_speech_id' => 'id']],
            [['word_id'], 'exist', 'skipOnError' => true, 'targetClass' => Words::className(), 'targetAttribute' => ['word_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'word_id' => 'Word ID',
            'parts_of_speech_id' => 'Parts Of Speech ID',
            'sort' => 'Sort',
        ];
    }

    /**
     * Gets query for [[PartsOfSpeech]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPartsOfSpeech()
    {
        return $this->hasOne(PartsOfSpeech::className(), ['id' => 'parts_of_speech_id']);
    }

    /**
     * Gets query for [[Word]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getWord()
    {
        return $this->hasOne(Words::className(), ['id' => 'word_id']);
    }
}
