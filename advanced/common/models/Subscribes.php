<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "subscribes".
 *
 * @property int $id
 * @property string $email_user
 * @property int|null $status
 * @property int|null $created_at
 */
class Subscribes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'subscribes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['email_user'], 'required'],
            [['status', 'created_at'], 'integer'],
            [['email_user'], 'string', 'max' => 255],
            [['email_user'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'email_user' => 'Email User',
            'status' => 'Status',
            'created_at' => 'Created At',
        ];
    }
}
