<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "irregular_verbs".
 *
 * @property int $id
 * @property string|null $infinitive
 * @property string|null $transcription_1
 * @property string|null $past_simple_active
 * @property string|null $transcription_2
 * @property string|null $past_participle
 * @property string|null $transcription_3
 * @property string|null $translation
 */
class IrregularVerbs extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'irregular_verbs';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['infinitive', 'transcription_1', 'past_simple_active', 'transcription_2', 'past_participle', 'transcription_3', 'translation'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'infinitive' => 'Infinitive',
            'transcription_1' => 'Transcription 1',
            'past_simple_active' => 'Past Simple Active',
            'transcription_2' => 'Transcription 2',
            'past_participle' => 'Past Participle',
            'transcription_3' => 'Transcription 3',
            'translation' => 'Translation',
        ];
    }
}
