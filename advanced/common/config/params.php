<?php
return [
    'adminEmail' => 'info@hogwords.net',
    'supportEmail' => 'info@hogwords.net',
    'senderEmail' => 'info@hogwords.net',
    'senderName' => 'hogwords.net mailer',
    'user.passwordResetTokenExpire' => 3600,
    'user.passwordMinLength' => 8,
];
