<?php

/* @var $this \yii\web\View */

/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;

AppAsset::register($this);

if (class_exists('yii\debug\Module')) {
    $this->off(\yii\web\View::EVENT_END_BODY, [\yii\debug\Module::getInstance(), 'renderToolbar']);
}

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" id="stm-site-preloader"
      class="no-js js_active vc_desktop vc_transform vc_transform skrollr skrollr-desktop stm-site-loaded">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=0">
<!--    --><?//= $this->render("blocks/css/style1.php") ?>

    <?php
    $parse_url = explode('/' , $_SERVER["REQUEST_URI"] );
    $parse_url = $parse_url[1];
    if ($parse_url == 'exercises') { ?><? } ?>

    <link rel="stylesheet" href="/static/css/mainbigcss.css">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <link href="https://fonts.gstatic.com" crossorigin="anonymous" rel="preconnect">

    <link rel="icon"
          href="/static/img/cropped-favicon-150x150.png"
          sizes="32x32">
    <link rel="icon"
          href="/static/img/cropped-favicon-300x300.png"
          sizes="192x192">
    <link rel="apple-touch-icon"
          href="/static/img/cropped-favicon-180x180.png">
    <meta name="msapplication-TileImage"
          content="/static/img/cropped-favicon-300x300.png">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>-->
    <link rel="stylesheet" href="/static/css/style3.css">
    <link rel="stylesheet" href="/static/css/training.css">
    <script src="/static/js/training.js"></script>




</head>
<body class="page-template-default page page-id-2541 theme-smarty woocommerce-js wpb-js-composer js-comp-ver-6.5.0 vc_responsive has_envato_iframe">
<?php $this->beginBody() ?>
<div id="wrapper">
    <?= $this->render("blocks/menu.php") ?>
    <?= $this->render("blocks/breadcrumbs.php") ?>

    <div class="content content_type_vc">
        <div class="container">

            <main class="main">
                <?= Alert::widget() ?>
                <?= $content ?>

                <!--                <script type="text/javascript">-->
                <!--                    $(document).ready(function(){-->
                <!--                        alert(jQuery.fn.jquery);-->
                <!--                    });-->
                <!--                </script>-->

<!--                --><?//= $this->render("blocks/main_content.php") ?>

            </main>
        </div>
    </div>
    <?= $this->render("blocks/footer.php") ?>
    <div class="container">

        <table>
            <tr>
                <td width="200"></td>
                <td width="200" ></td>
                <td></td>

            </tr>

        </table>

<!--    <!--LiveInternet counter--><a href="https://www.liveinternet.ru/click"-->
<!--                                  target="_blank"><img id="licnt9E37" width="88" height="31" style="border:0"-->
<!--                                                       title="LiveInternet: показано число просмотров за 24 часа, посетителей за 24 часа и за сегодня"-->
<!--                                                       src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAEALAAAAAABAAEAAAIBTAA7"-->
<!--                                                       alt=""/></a><script>(function(d,s){d.getElementById("licnt9E37").src=-->
<!--            "https://counter.yadro.ru/hit?t11.6;r"+escape(d.referrer)+-->
<!--            ((typeof(s)=="undefined")?"":";s"+s.width+"*"+s.height+"*"+-->
<!--                (s.colorDepth?s.colorDepth:s.pixelDepth))+";u"+escape(d.URL)+-->
<!--            ";h"+escape(d.title.substring(0,150))+";"+Math.random()})-->
<!--        (document,screen)</script><!--/LiveInternet-->-->

</div>

<!--    <script defer type='text/javascript' src='/static/js/js_composer_front.min.js?ver=6.5.0' id='wpb_composer_front_js-js'></script>-->

    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
