<?php if (Yii::$app->user->isGuest) {  ?>

    <li id="menu-item-3308"
        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-3308">
        <a href="/site/signup">Регистрация</a>

    </li>

    <li id="menu-item-3278"
        class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-3278 general__position">
        <a href="/site/login">Вход</a>

    </li>
<?php } else {  ?>
    <li id="menu-item-3303"
        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-3303">
        <a href="/add-words/select-from-all?page=1&per-page=30">Добавить слова к изучению</a>
        <ul class="sub-menu">
<!--            <li id="menu-item-3304"-->
<!--                class="menu-item menu-item-type-post_type menu-item-object-stm_event menu-item-3304">-->
<!--                <a href="/add-words/add-all">Учить все по популярности</a></li>-->
            <li id="menu-item-4066"
                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4066">
                <a href="/add-words/select-from-all?page=1&per-page=100">Выбрать из всех по популярности</a></li>
            <li id="menu-item-3889"
                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3889">
                <a href="/add-words/index">Указать свои слова к изучению</a>
            </li>
        </ul>
    </li>
    <li id="menu-item-3277"
        class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-3277">
    <a href="/my-play-list/index">Мои списки слов</a>
    <ul class="sub-menu">
    <?php
    $sub_lists = \frontend\controllers\MyPlayListController::getLists();
    if ($sub_lists) {
        foreach ($sub_lists as $val ) { ?>
            <li id="menu-item-<?php echo $val->id; ?>"
                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-<?php echo $val->id; ?>">
                <a href="/training/index?id=<?php echo $val->id; ?>"><?php echo $val->name; ?></a>

            </li>


        <? } ?>



    <?  }    ?>
    </ul>

    <?php



    $get_user = \common\models\User::findOne(Yii::$app->user->getId());

    ?>

    <li id="menu-item-4277"
        class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-4277">
        <a href="#">Аккаунт(<?php
            if (strlen($get_user->username) > 20) {
                $get_user->username = iconv_substr ($get_user->username, 0 , 20 , 'UTF-8' );
//                $get_user->username = substr($get_user->username, 0, 20);
                $get_user->username = "".$get_user->username."...";
            }

            echo $get_user->username; ?>)</a>
        <ul class="sub-menu">
            <li id="menu-item-3331" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3330"><a href="/subscribe/index">Оформить подписку </a></li>
<!--            <li id="menu-item-3331" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3331"><a href="/balance/add">Пополнить баланс</a></li>-->
            <li id="menu-item-3332" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3332"><a href="/user/logout">Выход </a></li>

        </ul>
    </li>




<?php }  ?>



