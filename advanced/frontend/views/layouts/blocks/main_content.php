<article id="post-2541" class="post-2541 page type-page status-publish hentry">
    <div class="entry-content">
        <div class="vc_row wpb_row vc_row-fluid">
            <div class="wpb_column vc_column_container vc_col-sm-12">
                <div class="vc_column-inner vc_custom_1475068946177">
                    <div class="wpb_wrapper"><h1
                            class="stm-title stm-font_color_lilac stm-title_sep_bottom"
                            style="text-align:center;margin-top:-7px;margin-bottom:25px">
                            Shortcodes </h1></div>
                </div>
            </div>
        </div>
        <div class="vc_row wpb_row vc_row-fluid vc_custom_1475071355125">
            <div class="wpb_column vc_column_container vc_col-sm-6">
                <div class="vc_column-inner">
                    <div class="wpb_wrapper"><h3
                            class="stm-title stm-font_color_lilac stm-title_sep_bottom"
                            style="text-align:left"> Accordion mode </h3>
                        <div class="vc_tta-container" data-vc-action="collapse">
                            <div class="vc_general vc_tta vc_tta-accordion vc_tta-o-shape-group vc_tta-gap-4 vc_custom_1456037177307 vc_tta-accordion_style_outline vc_tta-accordion_color_yellow">
                                <div class="vc_tta-panels-container">
                                    <div class="vc_tta-panels">
                                        <div class="vc_tta-panel vc_active"
                                             id="1456032341159-f2e644fc-117b"
                                             data-vc-content=".vc_tta-panel-body">
                                            <div class="vc_tta-panel-heading"><h4
                                                    class="vc_tta-panel-title vc_tta-controls-icon-position-right">
                                                    <a href="#1456032341159-f2e644fc-117b"
                                                       data-vc-accordion=""
                                                       data-vc-container=".vc_tta-container"><span
                                                            class="vc_tta-title-text">Jack Petchey Awards Evening?</span><i
                                                            class="vc_tta-controls-icon vc_tta-controls-icon-plus"></i></a>
                                                </h4></div>
                                            <div class="vc_tta-panel-body">
                                                <div class="wpb_text_column wpb_content_element">
                                                    <div class="wpb_wrapper"><p><span
                                                                style="letter-spacing: -0.35px;">If you are submitting your application online, you can pay the Registration Fee online using either a debit or credit card. Applications can only be processed once the Registration Fee has been paid.</span>
                                                        </p></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="vc_tta-panel" id="1456032341266-f4dc5125-bc0c"
                                             data-vc-content=".vc_tta-panel-body">
                                            <div class="vc_tta-panel-heading"><h4
                                                    class="vc_tta-panel-title vc_tta-controls-icon-position-right">
                                                    <a href="#1456032341266-f4dc5125-bc0c"
                                                       data-vc-accordion=""
                                                       data-vc-container=".vc_tta-container"><span
                                                            class="vc_tta-title-text">Holocaust Survivor visits our school?</span><i
                                                            class="vc_tta-controls-icon vc_tta-controls-icon-plus"></i></a>
                                                </h4></div>
                                            <div class="vc_tta-panel-body">
                                                <div class="wpb_text_column wpb_content_element">
                                                    <div class="wpb_wrapper"><p><span
                                                                style="letter-spacing: -0.35px;">If you are submitting your application online, you can pay the Registration Fee online using either a debit or credit card. Applications can only be processed once the Registration Fee has been paid.</span>
                                                        </p></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="vc_tta-panel" id="1456032341368-b0effc54-48c0"
                                             data-vc-content=".vc_tta-panel-body">
                                            <div class="vc_tta-panel-heading"><h4
                                                    class="vc_tta-panel-title vc_tta-controls-icon-position-right">
                                                    <a href="#1456032341368-b0effc54-48c0"
                                                       data-vc-accordion=""
                                                       data-vc-container=".vc_tta-container"><span
                                                            class="vc_tta-title-text">Children in Need Armistice Day?</span><i
                                                            class="vc_tta-controls-icon vc_tta-controls-icon-plus"></i></a>
                                                </h4></div>
                                            <div class="vc_tta-panel-body">
                                                <div class="wpb_text_column wpb_content_element">
                                                    <div class="wpb_wrapper"><p><span
                                                                style="letter-spacing: -0.35px;">If you are submitting your application online, you can pay the Registration Fee online using either a debit or credit card. Applications can only be processed once the Registration Fee has been paid.</span>
                                                        </p></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="vc_tta-panel" id="1456032341477-71f0788d-59ee"
                                             data-vc-content=".vc_tta-panel-body">
                                            <div class="vc_tta-panel-heading"><h4
                                                    class="vc_tta-panel-title vc_tta-controls-icon-position-right">
                                                    <a href="#1456032341477-71f0788d-59ee"
                                                       data-vc-accordion=""
                                                       data-vc-container=".vc_tta-container"><span
                                                            class="vc_tta-title-text">Can I pay the Registration Fee online?</span><i
                                                            class="vc_tta-controls-icon vc_tta-controls-icon-plus"></i></a>
                                                </h4></div>
                                            <div class="vc_tta-panel-body">
                                                <div class="wpb_text_column wpb_content_element">
                                                    <div class="wpb_wrapper"><p><span
                                                                style="letter-spacing: -0.35px;">If you are submitting your application online, you can pay the Registration Fee online using either a debit or credit card. Applications can only be processed once the Registration Fee has been paid.</span>
                                                        </p></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="vc_tta-panel" id="1456032341611-570c8a93-ef86"
                                             data-vc-content=".vc_tta-panel-body">
                                            <div class="vc_tta-panel-heading"><h4
                                                    class="vc_tta-panel-title vc_tta-controls-icon-position-right">
                                                    <a href="#1456032341611-570c8a93-ef86"
                                                       data-vc-accordion=""
                                                       data-vc-container=".vc_tta-container"><span
                                                            class="vc_tta-title-text">Do I have to complete an Application form for each child?</span><i
                                                            class="vc_tta-controls-icon vc_tta-controls-icon-plus"></i></a>
                                                </h4></div>
                                            <div class="vc_tta-panel-body">
                                                <div class="wpb_text_column wpb_content_element">
                                                    <div class="wpb_wrapper"><p><span
                                                                style="letter-spacing: -0.35px;">If you are submitting your application online, you can pay the Registration Fee online using either a debit or credit card. Applications can only be processed once the Registration Fee has been paid.</span>
                                                        </p></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="vc_tta-panel" id="1456032341743-74239763-7019"
                                             data-vc-content=".vc_tta-panel-body">
                                            <div class="vc_tta-panel-heading"><h4
                                                    class="vc_tta-panel-title vc_tta-controls-icon-position-right">
                                                    <a href="#1456032341743-74239763-7019"
                                                       data-vc-accordion=""
                                                       data-vc-container=".vc_tta-container"><span
                                                            class="vc_tta-title-text">Do I have to submit the Student References/Evaluations &amp; Student Questionnaires at the same time as the Application?</span><i
                                                            class="vc_tta-controls-icon vc_tta-controls-icon-plus"></i></a>
                                                </h4></div>
                                            <div class="vc_tta-panel-body">
                                                <div class="wpb_text_column wpb_content_element">
                                                    <div class="wpb_wrapper"><p><span
                                                                style="letter-spacing: -0.35px;">If you are submitting your application online, you can pay the Registration Fee online using either a debit or credit card. Applications can only be processed once the Registration Fee has been paid.</span>
                                                        </p></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="vc_tta-panel" id="1456032341876-bd97617a-4430"
                                             data-vc-content=".vc_tta-panel-body">
                                            <div class="vc_tta-panel-heading"><h4
                                                    class="vc_tta-panel-title vc_tta-controls-icon-position-right">
                                                    <a href="#1456032341876-bd97617a-4430"
                                                       data-vc-accordion=""
                                                       data-vc-container=".vc_tta-container"><span
                                                            class="vc_tta-title-text">Can I contact the Admissions Office if I have any questions?</span><i
                                                            class="vc_tta-controls-icon vc_tta-controls-icon-plus"></i></a>
                                                </h4></div>
                                            <div class="vc_tta-panel-body">
                                                <div class="wpb_text_column wpb_content_element">
                                                    <div class="wpb_wrapper"><p><span
                                                                style="letter-spacing: -0.35px;">If you are submitting your application online, you can pay the Registration Fee online using either a debit or credit card. Applications can only be processed once the Registration Fee has been paid.</span>
                                                        </p></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="wpb_column vc_column_container vc_col-sm-6">
                <div class="vc_column-inner">
                    <div class="wpb_wrapper"><h3
                            class="stm-title stm-font_color_lilac stm-title_sep_bottom"
                            style="text-align:left"> Toggle mode </h3>
                        <div class="vc_tta-container" data-vc-action="collapseAll">
                            <div class="vc_general vc_tta vc_tta-accordion vc_tta-o-shape-group vc_tta-gap-4 vc_tta-o-all-clickable vc_custom_1476254229487 vc_tta-accordion_style_outline vc_tta-accordion_color_yellow">
                                <div class="vc_tta-panels-container">
                                    <div class="vc_tta-panels">
                                        <div class="vc_tta-panel vc_active"
                                             id="1456032243547-61e30fa2-0cee"
                                             data-vc-content=".vc_tta-panel-body">
                                            <div class="vc_tta-panel-heading"><h4
                                                    class="vc_tta-panel-title vc_tta-controls-icon-position-right">
                                                    <a href="#1456032243547-61e30fa2-0cee"
                                                       data-vc-accordion=""
                                                       data-vc-container=".vc_tta-container"><i
                                                            class="vc_tta-icon fa fa-bell"></i><span
                                                            class="vc_tta-title-text">Jack Petchey Awards Evening?</span><i
                                                            class="vc_tta-controls-icon vc_tta-controls-icon-plus"></i></a>
                                                </h4></div>
                                            <div class="vc_tta-panel-body">
                                                <div class="wpb_text_column wpb_content_element">
                                                    <div class="wpb_wrapper"><p><span
                                                                style="letter-spacing: -0.35px;">If you are submitting your application online, you can pay the Registration Fee online using either a debit or credit card. Applications can only be processed once the Registration Fee has been paid.</span>
                                                        </p></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="vc_tta-panel" id="1456032266403-0f26efe8-7ead"
                                             data-vc-content=".vc_tta-panel-body">
                                            <div class="vc_tta-panel-heading"><h4
                                                    class="vc_tta-panel-title vc_tta-controls-icon-position-right">
                                                    <a href="#1456032266403-0f26efe8-7ead"
                                                       data-vc-accordion=""
                                                       data-vc-container=".vc_tta-container"><i
                                                            class="vc_tta-icon fa fa-calculator"></i><span
                                                            class="vc_tta-title-text">Holocaust Survivor visits our school?</span><i
                                                            class="vc_tta-controls-icon vc_tta-controls-icon-plus"></i></a>
                                                </h4></div>
                                            <div class="vc_tta-panel-body">
                                                <div class="wpb_text_column wpb_content_element">
                                                    <div class="wpb_wrapper"><p><span
                                                                style="letter-spacing: -0.35px;">If you are submitting your application online, you can pay the Registration Fee online using either a debit or credit card. Applications can only be processed once the Registration Fee has been paid.</span>
                                                        </p></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="vc_tta-panel" id="1456032277324-37cbdc6b-a753"
                                             data-vc-content=".vc_tta-panel-body">
                                            <div class="vc_tta-panel-heading"><h4
                                                    class="vc_tta-panel-title vc_tta-controls-icon-position-right">
                                                    <a href="#1456032277324-37cbdc6b-a753"
                                                       data-vc-accordion=""
                                                       data-vc-container=".vc_tta-container"><i
                                                            class="vc_tta-icon fa fa-graduation-cap"></i><span
                                                            class="vc_tta-title-text">Children in Need Armistice Day?</span><i
                                                            class="vc_tta-controls-icon vc_tta-controls-icon-plus"></i></a>
                                                </h4></div>
                                            <div class="vc_tta-panel-body">
                                                <div class="wpb_text_column wpb_content_element">
                                                    <div class="wpb_wrapper"><p><span
                                                                style="letter-spacing: -0.35px;">If you are submitting your application online, you can pay the Registration Fee online using either a debit or credit card. Applications can only be processed once the Registration Fee has been paid.</span>
                                                        </p></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="vc_tta-panel" id="1456032289105-587a456f-d0d7"
                                             data-vc-content=".vc_tta-panel-body">
                                            <div class="vc_tta-panel-heading"><h4
                                                    class="vc_tta-panel-title vc_tta-controls-icon-position-right">
                                                    <a href="#1456032289105-587a456f-d0d7"
                                                       data-vc-accordion=""
                                                       data-vc-container=".vc_tta-container"><i
                                                            class="vc_tta-icon fa fa-book"></i><span
                                                            class="vc_tta-title-text">Can I pay the Registration Fee online?</span><i
                                                            class="vc_tta-controls-icon vc_tta-controls-icon-plus"></i></a>
                                                </h4></div>
                                            <div class="vc_tta-panel-body">
                                                <div class="wpb_text_column wpb_content_element">
                                                    <div class="wpb_wrapper"><p><span
                                                                style="letter-spacing: -0.35px;">If you are submitting your application online, you can pay the Registration Fee online using either a debit or credit card. Applications can only be processed once the Registration Fee has been paid.</span>
                                                        </p></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="vc_tta-panel" id="1456032299960-b419bce3-cd16"
                                             data-vc-content=".vc_tta-panel-body">
                                            <div class="vc_tta-panel-heading"><h4
                                                    class="vc_tta-panel-title vc_tta-controls-icon-position-right">
                                                    <a href="#1456032299960-b419bce3-cd16"
                                                       data-vc-accordion=""
                                                       data-vc-container=".vc_tta-container"><i
                                                            class="vc_tta-icon fa fa-calendar"></i><span
                                                            class="vc_tta-title-text">Do I have to complete an Application form for each child?</span><i
                                                            class="vc_tta-controls-icon vc_tta-controls-icon-plus"></i></a>
                                                </h4></div>
                                            <div class="vc_tta-panel-body">
                                                <div class="wpb_text_column wpb_content_element">
                                                    <div class="wpb_wrapper"><p><span
                                                                style="letter-spacing: -0.35px;">If you are submitting your application online, you can pay the Registration Fee online using either a debit or credit card. Applications can only be processed once the Registration Fee has been paid.</span>
                                                        </p></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="vc_tta-panel" id="1456032311129-d10bb406-6344"
                                             data-vc-content=".vc_tta-panel-body">
                                            <div class="vc_tta-panel-heading"><h4
                                                    class="vc_tta-panel-title vc_tta-controls-icon-position-right">
                                                    <a href="#1456032311129-d10bb406-6344"
                                                       data-vc-accordion=""
                                                       data-vc-container=".vc_tta-container"><i
                                                            class="vc_tta-icon fa fa-briefcase"></i><span
                                                            class="vc_tta-title-text">Do I have to submit the Student References/Evaluations &amp; Student Questionnaires at the same time as the Application?</span><i
                                                            class="vc_tta-controls-icon vc_tta-controls-icon-plus"></i></a>
                                                </h4></div>
                                            <div class="vc_tta-panel-body">
                                                <div class="wpb_text_column wpb_content_element">
                                                    <div class="wpb_wrapper"><p><span
                                                                style="letter-spacing: -0.35px;">If you are submitting your application online, you can pay the Registration Fee online using either a debit or credit card. Applications can only be processed once the Registration Fee has been paid.</span>
                                                        </p></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="vc_tta-panel" id="1456032324728-5896dcce-e415"
                                             data-vc-content=".vc_tta-panel-body">
                                            <div class="vc_tta-panel-heading"><h4
                                                    class="vc_tta-panel-title vc_tta-controls-icon-position-right">
                                                    <a href="#1456032324728-5896dcce-e415"
                                                       data-vc-accordion=""
                                                       data-vc-container=".vc_tta-container"><i
                                                            class="vc_tta-icon fa fa-bus"></i><span
                                                            class="vc_tta-title-text">Can I contact the Admissions Office if I have any questions?</span><i
                                                            class="vc_tta-controls-icon vc_tta-controls-icon-plus"></i></a>
                                                </h4></div>
                                            <div class="vc_tta-panel-body">
                                                <div class="wpb_text_column wpb_content_element">
                                                    <div class="wpb_wrapper"><p><span
                                                                style="letter-spacing: -0.35px;">If you are submitting your application online, you can pay the Registration Fee online using either a debit or credit card. Applications can only be processed once the Registration Fee has been paid.</span>
                                                        </p></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="vc_row wpb_row vc_row-fluid">
            <div class="wpb_column vc_column_container vc_col-sm-12">
                <div class="vc_column-inner">
                    <div class="wpb_wrapper">
                        <div class="vc_row wpb_row vc_inner vc_row-fluid">
                            <div class="wpb_column vc_column_container vc_col-sm-6 vc_col-lg-7 vc_col-md-7">
                                <div class="vc_column-inner">
                                    <div class="wpb_wrapper"><h3
                                            class="stm-title stm-font_color_lilac stm-title_sep_bottom"
                                            style="text-align:left;margin-bottom:32px">
                                            Testimonials </h3>
                                        <div class="stm-testimonials">
                                            <div class="stm-carousel stm-carousel_type_testimonials stm-carousel_dots_blue owl-carousel owl-theme owl-loaded">
                                                <div class="owl-stage-outer">
                                                    <div class="owl-stage"
                                                         style="transform: translate3d(-1725px, 0px, 0px); transition: all 0.7s ease 0s; width: 2415px;">
                                                        <div class="owl-item cloned"
                                                             style="width: 345px; margin-right: 0px;">
                                                            <div class="stm-testimonial stm-testimonial_style_1">
                                                                <div class="stm-testimonial__avatar_box">
                                                                    <div class="stm-testimonial__avatar">
                                                                        <noscript><img
                                                                                onload="Wpfcll.r(this,true);"
                                                                                src="https://smartyschool.stylemixthemes.com/kindergarten-two/wp-content/plugins/wp-fastest-cache-premium/pro/images/blank.gif"
                                                                                wpfc-data-original-src="https://smartyschool.stylemixthemes.com/kindergarten-two/wp-content/uploads/2015/12/testimonial_2-100x100.jpg"
                                                                                width="100"
                                                                                height="100"
                                                                                alt="testimonial_2"
                                                                                title="testimonial_2"/>
                                                                        </noscript>
                                                                        <img class="lazyload"
                                                                             src="data:image/svg+xml,%3Csvg%20xmlns=%22http://www.w3.org/2000/svg%22%20viewBox=%220%200%20100%20100%22%3E%3C/svg%3E"
                                                                             data-src="https://smartyschool.stylemixthemes.com/kindergarten-two/wp-content/uploads/2015/12/testimonial_2-100x100.jpg"
                                                                             width="100" height="100"
                                                                             alt="testimonial_2"
                                                                             title="testimonial_2">
                                                                    </div>
                                                                </div>
                                                                <div class="stm-testimonial__content">
                                                                    <div class="stm-testimonial__author">
                                                                        Maria Bimmer
                                                                    </div>
                                                                    <div class="stm-testimonial__text">
                                                                        <p>Smarty Kindergarten is a
                                                                            great place for my daughter
                                                                            to start her schooling
                                                                            experience. It’s welcoming
                                                                            and safe and my daughter
                                                                            loves being there! Her skill
                                                                            level is significantly
                                                                            better since attending
                                                                            Smarty!</p></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="owl-item cloned"
                                                             style="width: 345px; margin-right: 0px;">
                                                            <div class="stm-testimonial stm-testimonial_style_1">
                                                                <div class="stm-testimonial__avatar_box">
                                                                    <div class="stm-testimonial__avatar">
                                                                        <noscript><img
                                                                                onload="Wpfcll.r(this,true);"
                                                                                src="https://smartyschool.stylemixthemes.com/kindergarten-two/wp-content/plugins/wp-fastest-cache-premium/pro/images/blank.gif"
                                                                                wpfc-data-original-src="https://smartyschool.stylemixthemes.com/kindergarten-two/wp-content/uploads/2015/12/testimonial_3-100x100.jpg"
                                                                                width="100"
                                                                                height="100"
                                                                                alt="testimonial_3"
                                                                                title="testimonial_3"/>
                                                                        </noscript>
                                                                        <img class=" ls-is-cached lazyloaded"
                                                                             src="https://smartyschool.stylemixthemes.com/kindergarten-two/wp-content/uploads/2015/12/testimonial_3-100x100.jpg"
                                                                             data-src="https://smartyschool.stylemixthemes.com/kindergarten-two/wp-content/uploads/2015/12/testimonial_3-100x100.jpg"
                                                                             width="100" height="100"
                                                                             alt="testimonial_3"
                                                                             title="testimonial_3">
                                                                    </div>
                                                                </div>
                                                                <div class="stm-testimonial__content">
                                                                    <div class="stm-testimonial__author">
                                                                        Sven Bender
                                                                    </div>
                                                                    <div class="stm-testimonial__text">
                                                                        <p>I am sure that my son is
                                                                            studying at Smarty
                                                                            Education, I really like
                                                                            their approach to children,
                                                                            I like their curriculum and
                                                                            really like their
                                                                            interesting teaching
                                                                            methods.</p></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="owl-item"
                                                             style="width: 345px; margin-right: 0px;">
                                                            <div class="stm-testimonial stm-testimonial_style_1">
                                                                <div class="stm-testimonial__avatar_box">
                                                                    <div class="stm-testimonial__avatar">
                                                                        <noscript><img
                                                                                onload="Wpfcll.r(this,true);"
                                                                                src="https://smartyschool.stylemixthemes.com/kindergarten-two/wp-content/plugins/wp-fastest-cache-premium/pro/images/blank.gif"
                                                                                wpfc-data-original-src="https://smartyschool.stylemixthemes.com/kindergarten-two/wp-content/uploads/2015/12/testimonial_1-100x100.jpg"
                                                                                width="100"
                                                                                height="100"
                                                                                alt="testimonial_1"
                                                                                title="testimonial_1"/>
                                                                        </noscript>
                                                                        <img class=" ls-is-cached lazyloaded"
                                                                             src="https://smartyschool.stylemixthemes.com/kindergarten-two/wp-content/uploads/2015/12/testimonial_1-100x100.jpg"
                                                                             data-src="https://smartyschool.stylemixthemes.com/kindergarten-two/wp-content/uploads/2015/12/testimonial_1-100x100.jpg"
                                                                             width="100" height="100"
                                                                             alt="testimonial_1"
                                                                             title="testimonial_1">
                                                                    </div>
                                                                </div>
                                                                <div class="stm-testimonial__content">
                                                                    <div class="stm-testimonial__author">
                                                                        Rosamund Johnson
                                                                    </div>
                                                                    <div class="stm-testimonial__text">
                                                                        <p>We decided to actively
                                                                            cooperate with
                                                                            SmartyEduction because of
                                                                            their quality services,
                                                                            including because of their
                                                                            on-the-job training program
                                                                            and other incentives for
                                                                            students to help reduce
                                                                            learning time.</p></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="owl-item"
                                                             style="width: 345px; margin-right: 0px;">
                                                            <div class="stm-testimonial stm-testimonial_style_1">
                                                                <div class="stm-testimonial__avatar_box">
                                                                    <div class="stm-testimonial__avatar">
                                                                        <noscript><img
                                                                                onload="Wpfcll.r(this,true);"
                                                                                src="https://smartyschool.stylemixthemes.com/kindergarten-two/wp-content/plugins/wp-fastest-cache-premium/pro/images/blank.gif"
                                                                                wpfc-data-original-src="https://smartyschool.stylemixthemes.com/kindergarten-two/wp-content/uploads/2015/12/testimonial_2-100x100.jpg"
                                                                                width="100"
                                                                                height="100"
                                                                                alt="testimonial_2"
                                                                                title="testimonial_2"/>
                                                                        </noscript>
                                                                        <img class=" ls-is-cached lazyloaded"
                                                                             src="https://smartyschool.stylemixthemes.com/kindergarten-two/wp-content/uploads/2015/12/testimonial_2-100x100.jpg"
                                                                             data-src="https://smartyschool.stylemixthemes.com/kindergarten-two/wp-content/uploads/2015/12/testimonial_2-100x100.jpg"
                                                                             width="100" height="100"
                                                                             alt="testimonial_2"
                                                                             title="testimonial_2">
                                                                    </div>
                                                                </div>
                                                                <div class="stm-testimonial__content">
                                                                    <div class="stm-testimonial__author">
                                                                        Maria Bimmer
                                                                    </div>
                                                                    <div class="stm-testimonial__text">
                                                                        <p>Smarty Kindergarten is a
                                                                            great place for my daughter
                                                                            to start her schooling
                                                                            experience. It’s welcoming
                                                                            and safe and my daughter
                                                                            loves being there! Her skill
                                                                            level is significantly
                                                                            better since attending
                                                                            Smarty!</p></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="owl-item"
                                                             style="width: 345px; margin-right: 0px;">
                                                            <div class="stm-testimonial stm-testimonial_style_1">
                                                                <div class="stm-testimonial__avatar_box">
                                                                    <div class="stm-testimonial__avatar">
                                                                        <noscript><img
                                                                                onload="Wpfcll.r(this,true);"
                                                                                src="https://smartyschool.stylemixthemes.com/kindergarten-two/wp-content/plugins/wp-fastest-cache-premium/pro/images/blank.gif"
                                                                                wpfc-data-original-src="https://smartyschool.stylemixthemes.com/kindergarten-two/wp-content/uploads/2015/12/testimonial_3-100x100.jpg"
                                                                                width="100"
                                                                                height="100"
                                                                                alt="testimonial_3"
                                                                                title="testimonial_3"/>
                                                                        </noscript>
                                                                        <img class=" ls-is-cached lazyloaded"
                                                                             src="https://smartyschool.stylemixthemes.com/kindergarten-two/wp-content/uploads/2015/12/testimonial_3-100x100.jpg"
                                                                             data-src="https://smartyschool.stylemixthemes.com/kindergarten-two/wp-content/uploads/2015/12/testimonial_3-100x100.jpg"
                                                                             width="100" height="100"
                                                                             alt="testimonial_3"
                                                                             title="testimonial_3">
                                                                    </div>
                                                                </div>
                                                                <div class="stm-testimonial__content">
                                                                    <div class="stm-testimonial__author">
                                                                        Sven Bender
                                                                    </div>
                                                                    <div class="stm-testimonial__text">
                                                                        <p>I am sure that my son is
                                                                            studying at Smarty
                                                                            Education, I really like
                                                                            their approach to children,
                                                                            I like their curriculum and
                                                                            really like their
                                                                            interesting teaching
                                                                            methods.</p></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="owl-item cloned active"
                                                             style="width: 345px; margin-right: 0px;">
                                                            <div class="stm-testimonial stm-testimonial_style_1">
                                                                <div class="stm-testimonial__avatar_box">
                                                                    <div class="stm-testimonial__avatar">
                                                                        <noscript><img
                                                                                onload="Wpfcll.r(this,true);"
                                                                                src="https://smartyschool.stylemixthemes.com/kindergarten-two/wp-content/plugins/wp-fastest-cache-premium/pro/images/blank.gif"
                                                                                wpfc-data-original-src="https://smartyschool.stylemixthemes.com/kindergarten-two/wp-content/uploads/2015/12/testimonial_1-100x100.jpg"
                                                                                width="100"
                                                                                height="100"
                                                                                alt="testimonial_1"
                                                                                title="testimonial_1"/>
                                                                        </noscript>
                                                                        <img class=" ls-is-cached lazyloaded"
                                                                             src="https://smartyschool.stylemixthemes.com/kindergarten-two/wp-content/uploads/2015/12/testimonial_1-100x100.jpg"
                                                                             data-src="https://smartyschool.stylemixthemes.com/kindergarten-two/wp-content/uploads/2015/12/testimonial_1-100x100.jpg"
                                                                             width="100" height="100"
                                                                             alt="testimonial_1"
                                                                             title="testimonial_1">
                                                                    </div>
                                                                </div>
                                                                <div class="stm-testimonial__content">
                                                                    <div class="stm-testimonial__author">
                                                                        Rosamund Johnson
                                                                    </div>
                                                                    <div class="stm-testimonial__text">
                                                                        <p>We decided to actively
                                                                            cooperate with
                                                                            SmartyEduction because of
                                                                            their quality services,
                                                                            including because of their
                                                                            on-the-job training program
                                                                            and other incentives for
                                                                            students to help reduce
                                                                            learning time.</p></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="owl-item cloned"
                                                             style="width: 345px; margin-right: 0px;">
                                                            <div class="stm-testimonial stm-testimonial_style_1">
                                                                <div class="stm-testimonial__avatar_box">
                                                                    <div class="stm-testimonial__avatar">
                                                                        <noscript><img
                                                                                onload="Wpfcll.r(this,true);"
                                                                                src="https://smartyschool.stylemixthemes.com/kindergarten-two/wp-content/plugins/wp-fastest-cache-premium/pro/images/blank.gif"
                                                                                wpfc-data-original-src="https://smartyschool.stylemixthemes.com/kindergarten-two/wp-content/uploads/2015/12/testimonial_2-100x100.jpg"
                                                                                width="100"
                                                                                height="100"
                                                                                alt="testimonial_2"
                                                                                title="testimonial_2"/>
                                                                        </noscript>
                                                                        <img class=" ls-is-cached lazyloaded"
                                                                             src="https://smartyschool.stylemixthemes.com/kindergarten-two/wp-content/uploads/2015/12/testimonial_2-100x100.jpg"
                                                                             data-src="https://smartyschool.stylemixthemes.com/kindergarten-two/wp-content/uploads/2015/12/testimonial_2-100x100.jpg"
                                                                             width="100" height="100"
                                                                             alt="testimonial_2"
                                                                             title="testimonial_2">
                                                                    </div>
                                                                </div>
                                                                <div class="stm-testimonial__content">
                                                                    <div class="stm-testimonial__author">
                                                                        Maria Bimmer
                                                                    </div>
                                                                    <div class="stm-testimonial__text">
                                                                        <p>Smarty Kindergarten is a
                                                                            great place for my daughter
                                                                            to start her schooling
                                                                            experience. It’s welcoming
                                                                            and safe and my daughter
                                                                            loves being there! Her skill
                                                                            level is significantly
                                                                            better since attending
                                                                            Smarty!</p></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="owl-controls">
                                                    <div class="owl-nav">
                                                        <div class="owl-prev" style="display: none;">
                                                            <span class="stm-icon stm-icon-arrow-l"></span>
                                                        </div>
                                                        <div class="owl-next" style="display: none;">
                                                            <span class="stm-icon stm-icon-arrow-r"></span>
                                                        </div>
                                                    </div>
                                                    <div class="owl-dots" style="">
                                                        <div class="owl-dot active"><span></span></div>
                                                        <div class="owl-dot"><span></span></div>
                                                        <div class="owl-dot"><span></span></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="wpb_column vc_column_container vc_col-sm-1 vc_hidden-sm vc_hidden-xs">
                                <div class="vc_column-inner">
                                    <div class="wpb_wrapper"></div>
                                </div>
                            </div>
                            <div class="wpb_column vc_column_container vc_col-sm-6 vc_col-lg-4 vc_col-md-4">
                                <div class="vc_column-inner">
                                    <div class="wpb_wrapper"><h3
                                            class="stm-title stm-font_color_lilac stm-title_sep_bottom"
                                            style="text-align:left;margin-bottom:44px"> Quote </h3>
                                        <div class="stm-blockquote stm-blockquote_bordered"><p>These two
                                                fantastic events are always a highlight of our festive
                                                calendar. Our students performed brilliantly and we hope
                                                the audiences enjoyed the recitals, as our festive
                                                efforts!</p>
                                            <footer><cite><b>Ms. Emilia Kingsley</b>, Head of
                                                    Music</cite></footer>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="vc_row wpb_row vc_row-fluid">
            <div class="wpb_column vc_column_container vc_col-sm-12">
                <div class="vc_column-inner">
                    <div class="wpb_wrapper"><h3
                            class="stm-title stm-font_color_lilac stm-title_sep_bottom"
                            style="text-align:center"> Icon boxes </h3></div>
                </div>
            </div>
        </div>
        <div data-vc-full-width="true" data-vc-full-width-init="true" data-vc-parallax="1.5"
             data-vc-parallax-image="https://smartyschool.stylemixthemes.com/kindergarten-two/wp-content/uploads/2016/02/bg7.jpg"
             class="vc_row wpb_row vc_row-fluid vc_custom_1475236985606 vc_row-has-fill vc_general vc_parallax vc_parallax-content-moving"
             style="position: relative; left: -93.5px; box-sizing: border-box; width: 937px; padding-left: 93.5px; padding-right: 93.5px;">
            <div class="wpb_column vc_column_container vc_col-sm-12">
                <div class="vc_column-inner">
                    <div class="wpb_wrapper">
                        <div class="vc_row wpb_row vc_inner vc_row-fluid row-five-columns">
                            <div class="wpb_column vc_column_container vc_col-sm-2 vc_col-lg-2 vc_col-md-2">
                                <div class="vc_column-inner vc_custom_1475236994875">
                                    <div class="wpb_wrapper">
                                        <div class="stm-stats stm-stats_counter clearfix stm-stats_icon_center vc_custom_1476097144490">
                                            <div class="stm-stats__icon stm-font_color_pink"
                                                 style="padding-top:7px;padding-bottom:48px">
                                                <object id="stm-stats-svg-1495799928"
                                                        type="image/svg+xml"
                                                        data="https://smartyschool.stylemixthemes.com/kindergarten-two/wp-content/uploads/2015/12/icon1.svg"
                                                        class="skrollable skrollable-between"></object>
                                            </div>
                                            <div class="stm-stats__content">
                                                <div class="stm-stats__value"><span
                                                        class="stats__value-number"
                                                        id="counter_1495799928"
                                                        style="color:#ffffff">864</span></div>
                                                <div class="stm-stats__descr" style="color:#ffdd00">
                                                    teaching hours
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="wpb_column vc_column_container vc_col-sm-2 vc_col-lg-2 vc_col-md-2">
                                <div class="vc_column-inner vc_custom_1475237001111">
                                    <div class="wpb_wrapper">
                                        <div class="stm-stats stm-stats_counter clearfix stm-stats_icon_center vc_custom_1476097160355">
                                            <div class="stm-stats__icon stm-font_color_pink"
                                                 style="padding-bottom:40px">
                                                <object id="stm-stats-svg-1135503306"
                                                        type="image/svg+xml"
                                                        data="https://smartyschool.stylemixthemes.com/kindergarten-two/wp-content/uploads/2015/12/icon2.svg"
                                                        class="skrollable skrollable-between"></object>
                                            </div>
                                            <div class="stm-stats__content">
                                                <div class="stm-stats__value"><span
                                                        class="stats__value-number"
                                                        id="counter_1135503306"
                                                        style="color:#ffffff">928</span></div>
                                                <div class="stm-stats__descr" style="color:#ffdd00">
                                                    meals per year
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="wpb_column vc_column_container vc_col-sm-2 vc_col-lg-2 vc_col-md-2">
                                <div class="vc_column-inner vc_custom_1475237006676">
                                    <div class="wpb_wrapper">
                                        <div class="stm-stats stm-stats_counter clearfix stm-stats_icon_center vc_custom_1476097176088">
                                            <div class="stm-stats__icon stm-font_color_pink"
                                                 style="padding-top:4px;padding-bottom:45px">
                                                <object id="stm-stats-svg-1866704219"
                                                        type="image/svg+xml"
                                                        data="https://smartyschool.stylemixthemes.com/kindergarten-two/wp-content/uploads/2015/12/icon3.svg"
                                                        class="skrollable skrollable-between"></object>
                                            </div>
                                            <div class="stm-stats__content">
                                                <div class="stm-stats__value"><span
                                                        class="stats__value-number"
                                                        id="counter_1866704219"
                                                        style="color:#ffffff">46</span></div>
                                                <div class="stm-stats__descr" style="color:#ffdd00">
                                                    morning sessions
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="wpb_column vc_column_container vc_col-sm-2 vc_col-lg-2 vc_col-md-2">
                                <div class="vc_column-inner vc_custom_1475237012906">
                                    <div class="wpb_wrapper">
                                        <div class="stm-stats stm-stats_counter clearfix stm-stats_icon_center vc_custom_1476097190470">
                                            <div class="stm-stats__icon stm-font_color_pink"
                                                 style="padding-bottom:40px">
                                                <object id="stm-stats-svg-612619439"
                                                        type="image/svg+xml"
                                                        data="https://smartyschool.stylemixthemes.com/kindergarten-two/wp-content/uploads/2015/12/icon4.svg"
                                                        class="skrollable skrollable-between"></object>
                                            </div>
                                            <div class="stm-stats__content">
                                                <div class="stm-stats__value"><span
                                                        class="stats__value-number"
                                                        id="counter_612619439"
                                                        style="color:#ffffff">65</span></div>
                                                <div class="stm-stats__descr" style="color:#ffdd00">full
                                                    daycare
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="wpb_column vc_column_container vc_col-sm-2 vc_col-lg-2 vc_col-md-2">
                                <div class="vc_column-inner vc_custom_1475237018181">
                                    <div class="wpb_wrapper">
                                        <div class="stm-stats stm-stats_counter clearfix stm-stats_icon_center vc_custom_1476097217955">
                                            <div class="stm-stats__icon stm-font_color_pink"
                                                 style="padding-bottom:40px">
                                                <object id="stm-stats-svg-1310704001"
                                                        type="image/svg+xml"
                                                        data="https://smartyschool.stylemixthemes.com/kindergarten-two/wp-content/uploads/2015/12/icon5.svg"
                                                        class="skrollable skrollable-between"></object>
                                            </div>
                                            <div class="stm-stats__content">
                                                <div class="stm-stats__value"><span
                                                        class="stats__value-number"
                                                        id="counter_1310704001"
                                                        style="color:#ffffff">54</span></div>
                                                <div class="stm-stats__descr" style="color:#ffdd00">full
                                                    daycare
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="vc_parallax-inner skrollable skrollable-before" data-bottom-top="top: -50%;"
                 data-top-bottom="top: 0%;"
                 style="height: 150%; background-image: url(&quot;https://smartyschool.stylemixthemes.com/kindergarten-two/wp-content/uploads/2016/02/bg7.jpg&quot;); top: -50%;"></div>
        </div>
        <div class="vc_row-full-width vc_clearfix"></div>
        <div data-vc-full-width="true" data-vc-full-width-init="true"
             class="vc_row wpb_row vc_row-fluid vc_custom_1475072260225 vc_row-has-fill"
             style="position: relative; left: -93.5px; box-sizing: border-box; width: 937px; padding-left: 93.5px; padding-right: 93.5px;">
            <div class="wpb_column vc_column_container vc_col-sm-12">
                <div class="vc_column-inner">
                    <div class="wpb_wrapper">
                        <div class="vc_row wpb_row vc_inner vc_row-fluid">
                            <div class="wpb_column vc_column_container vc_col-sm-3 vc_col-lg-3 vc_col-md-3">
                                <div class="vc_column-inner">
                                    <div class="wpb_wrapper">
                                        <div class="stm-stats stm-stats_counter clearfix stm-stats_icon_left vc_custom_1476250524327">
                                            <div class="stm-stats__icon stm-font_color_pink"
                                                 style="padding-top:2px">
                                                <object id="stm-stats-svg-66317958" type="image/svg+xml"
                                                        data="https://smartyschool.stylemixthemes.com/kindergarten-two/wp-content/uploads/2015/12/icon1.svg"
                                                        class="skrollable skrollable-between"></object>
                                            </div>
                                            <div class="stm-stats__content">
                                                <div class="stm-stats__value"><span
                                                        class="stats__value-number"
                                                        id="counter_66317958">864</span></div>
                                                <div class="stm-stats__descr">teaching hours</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="wpb_column vc_column_container vc_col-sm-3 vc_col-lg-3 vc_col-md-3">
                                <div class="vc_column-inner">
                                    <div class="wpb_wrapper">
                                        <div class="stm-stats stm-stats_counter clearfix stm-stats_icon_left vc_custom_1476250722177">
                                            <div class="stm-stats__icon stm-font_color_pink">
                                                <object id="stm-stats-svg-1447296330"
                                                        type="image/svg+xml"
                                                        data="https://smartyschool.stylemixthemes.com/kindergarten-two/wp-content/uploads/2015/12/icon2.svg"
                                                        class="skrollable skrollable-between"></object>
                                            </div>
                                            <div class="stm-stats__content">
                                                <div class="stm-stats__value"><span
                                                        class="stats__value-number"
                                                        id="counter_1447296330">928</span></div>
                                                <div class="stm-stats__descr">meals per year</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="wpb_column vc_column_container vc_col-sm-3 vc_col-lg-3 vc_col-md-3">
                                <div class="vc_column-inner">
                                    <div class="wpb_wrapper">
                                        <div class="stm-stats stm-stats_counter clearfix stm-stats_icon_left vc_custom_1476250668959">
                                            <div class="stm-stats__icon stm-font_color_pink">
                                                <object id="stm-stats-svg-260564176"
                                                        type="image/svg+xml"
                                                        data="https://smartyschool.stylemixthemes.com/kindergarten-two/wp-content/uploads/2015/12/icon3.svg"
                                                        class="skrollable skrollable-between"></object>
                                            </div>
                                            <div class="stm-stats__content">
                                                <div class="stm-stats__value"><span
                                                        class="stats__value-number"
                                                        id="counter_260564176">364</span></div>
                                                <div class="stm-stats__descr">morning sessions</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="wpb_column vc_column_container vc_col-sm-3 vc_col-lg-3 vc_col-md-3">
                                <div class="vc_column-inner">
                                    <div class="wpb_wrapper">
                                        <div class="stm-stats stm-stats_counter clearfix stm-stats_icon_left vc_custom_1476250685064">
                                            <div class="stm-stats__icon stm-font_color_pink">
                                                <object id="stm-stats-svg-643555703"
                                                        type="image/svg+xml"
                                                        data="https://smartyschool.stylemixthemes.com/kindergarten-two/wp-content/uploads/2015/12/icon4.svg"
                                                        class="skrollable skrollable-between"></object>
                                            </div>
                                            <div class="stm-stats__content">
                                                <div class="stm-stats__value"><span
                                                        class="stats__value-number"
                                                        id="counter_643555703">546</span></div>
                                                <div class="stm-stats__descr">full daycare</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="vc_row-full-width vc_clearfix"></div>
        <div class="vc_row wpb_row vc_row-fluid vc_custom_1475123564531">
            <div class="wpb_column vc_column_container vc_col-sm-12">
                <div class="vc_column-inner">
                    <div class="wpb_wrapper"><h3
                            class="stm-title stm-font_color_lilac stm-title_sep_bottom"
                            style="text-align:left;margin-bottom:44px"> Pricing table </h3>
                        <div class="stm-pricing stm-pricing_three" id="stm-pricing-60d561399f15d">
                            <div class="stm-pricing__content">
                                <div class="stm-pricing__side-panel">
                                    <div class="stm-pricing__filters">
                                        <ul>
                                            <li class="stm-pricing__filter stm-pricing__filter_active">
                                                <a data-period-filter="monthly" href="#">monthly</a>
                                            </li>
                                            <li class="stm-pricing__filter"><a
                                                    data-period-filter="yearly" href="#">yearly</a>
                                            </li>
                                            <li class="stm-pricing__filter_fluid-hover"
                                                style="width: 132.609px;"></li>
                                        </ul>
                                    </div>
                                    <ul class="stm-pricing__features">
                                        <li class="stm-pricing__feature">Languages: Chinese, Spanish,
                                            Japanese
                                        </li>
                                        <li class="stm-pricing__feature">Cafeteria: Breakfast, Lunch,
                                            Supper
                                        </li>
                                        <li class="stm-pricing__feature">Sport games and activities</li>
                                        <li class="stm-pricing__feature">Camping on Mountains</li>
                                        <li class="stm-pricing__feature">School Bus</li>
                                        <li class="stm-pricing__feature">Cafeteria: Breakfast, Lunch,
                                            Supper
                                        </li>
                                        <li class="stm-pricing__feature">Sport games</li>
                                    </ul>
                                </div>
                                <div class="stm-pricing__tables">
                                    <div class="stm-pricing__tables-row">
                                        <div class="stm-pricing__tables-col">
                                            <div class="stm-pricing-table" style="border-color:#ffaf40">
                                                <div class="stm-pricing-table__title">Basic</div>
                                                <ul class="stm-pricing-table__periods">
                                                    <li class="stm-pricing-table__periods-item stm-pricing-table__periods-item_active"
                                                        data-period="monthly">
                                                        <div class="stm-pricing-table__price"
                                                             style="color:#ffaf40"> $16
                                                        </div>
                                                        <div class="stm-pricing-table__period"> per
                                                            month
                                                        </div>
                                                    </li>
                                                    <li class="stm-pricing-table__periods-item"
                                                        data-period="yearly">
                                                        <div class="stm-pricing-table__price"
                                                             style="color:#ffaf40"> $14
                                                        </div>
                                                        <div class="stm-pricing-table__period"> per
                                                            month
                                                        </div>
                                                    </li>
                                                </ul>
                                                <ul class="stm-pricing-table__features">
                                                    <li class="stm-pricing-table__feature">
                                                        <div class="stm-pricing-table__feature-label">
                                                            Languages: Chinese, Spanish, Japanese
                                                        </div>
                                                        <div class="stm-pricing-table__feature-value"><i
                                                                class="stm-icon stm-icon-check"></i>
                                                        </div>
                                                    </li>
                                                    <li class="stm-pricing-table__feature">
                                                        <div class="stm-pricing-table__feature-label">
                                                            Cafeteria: Breakfast, Lunch, Supper
                                                        </div>
                                                        <div class="stm-pricing-table__feature-value"><i
                                                                class="stm-icon stm-icon-check"></i>
                                                        </div>
                                                    </li>
                                                    <li class="stm-pricing-table__feature">
                                                        <div class="stm-pricing-table__feature-label">
                                                            Sport games and activities
                                                        </div>
                                                        <div class="stm-pricing-table__feature-value"><i
                                                                class="stm-icon stm-icon-check"></i>
                                                        </div>
                                                    </li>
                                                    <li class="stm-pricing-table__feature">
                                                        <div class="stm-pricing-table__feature-label">
                                                            Camping on Mountains
                                                        </div>
                                                        <div class="stm-pricing-table__feature-value"><i
                                                                class="stm-icon stm-icon-check"></i>
                                                        </div>
                                                    </li>
                                                    <li class="stm-pricing-table__feature">
                                                        <div class="stm-pricing-table__feature-label">
                                                            School Bus
                                                        </div>
                                                        <div class="stm-pricing-table__feature-value"><i
                                                                class="stm-icon stm-icon-times"></i>
                                                        </div>
                                                    </li>
                                                    <li class="stm-pricing-table__feature">
                                                        <div class="stm-pricing-table__feature-label">
                                                            Cafeteria: Breakfast, Lunch, Supper
                                                        </div>
                                                        <div class="stm-pricing-table__feature-value"><i
                                                                class="stm-icon stm-icon-times"></i>
                                                        </div>
                                                    </li>
                                                    <li class="stm-pricing-table__feature">
                                                        <div class="stm-pricing-table__feature-label">
                                                            Sport games
                                                        </div>
                                                        <div class="stm-pricing-table__feature-value"><i
                                                                class="stm-icon stm-icon-times"></i>
                                                        </div>
                                                    </li>
                                                </ul>
                                                <div class="stm-pricing-table__action"><a href="#"
                                                                                          style="border-color:#ffaf40"
                                                                                          target="_self"
                                                                                          class="stm-btn stm-btn_outline stm-btn_md stm-btn_icon-left"><i
                                                            class="stm-icon stm-icon-duck"
                                                            style="margin-top:-4px;margin-right:19px"></i>
                                                        Purchase</a></div>
                                            </div>
                                        </div>
                                        <div class="stm-pricing__tables-col">
                                            <div class="stm-pricing-table" style="border-color:#ff6666">
                                                <div class="stm-pricing-table__title">Standart</div>
                                                <ul class="stm-pricing-table__periods">
                                                    <li class="stm-pricing-table__periods-item stm-pricing-table__periods-item_active"
                                                        data-period="monthly">
                                                        <div class="stm-pricing-table__price"
                                                             style="color:#ff6666"> $30
                                                        </div>
                                                        <div class="stm-pricing-table__period"> per
                                                            month
                                                        </div>
                                                    </li>
                                                    <li class="stm-pricing-table__periods-item"
                                                        data-period="yearly">
                                                        <div class="stm-pricing-table__price"
                                                             style="color:#ff6666"> $28
                                                        </div>
                                                        <div class="stm-pricing-table__period"> per
                                                            month
                                                        </div>
                                                    </li>
                                                </ul>
                                                <ul class="stm-pricing-table__features">
                                                    <li class="stm-pricing-table__feature">
                                                        <div class="stm-pricing-table__feature-label">
                                                            Languages: Chinese, Spanish, Japanese
                                                        </div>
                                                        <div class="stm-pricing-table__feature-value"><i
                                                                class="stm-icon stm-icon-check"></i>
                                                        </div>
                                                    </li>
                                                    <li class="stm-pricing-table__feature">
                                                        <div class="stm-pricing-table__feature-label">
                                                            Cafeteria: Breakfast, Lunch, Supper
                                                        </div>
                                                        <div class="stm-pricing-table__feature-value"><i
                                                                class="stm-icon stm-icon-check"></i>
                                                        </div>
                                                    </li>
                                                    <li class="stm-pricing-table__feature">
                                                        <div class="stm-pricing-table__feature-label">
                                                            Sport games and activities
                                                        </div>
                                                        <div class="stm-pricing-table__feature-value"><i
                                                                class="stm-icon stm-icon-check"></i>
                                                        </div>
                                                    </li>
                                                    <li class="stm-pricing-table__feature">
                                                        <div class="stm-pricing-table__feature-label">
                                                            Camping on Mountains
                                                        </div>
                                                        <div class="stm-pricing-table__feature-value"><i
                                                                class="stm-icon stm-icon-check"></i>
                                                        </div>
                                                    </li>
                                                    <li class="stm-pricing-table__feature">
                                                        <div class="stm-pricing-table__feature-label">
                                                            School Bus
                                                        </div>
                                                        <div class="stm-pricing-table__feature-value"><i
                                                                class="stm-icon stm-icon-check"></i>
                                                        </div>
                                                    </li>
                                                    <li class="stm-pricing-table__feature">
                                                        <div class="stm-pricing-table__feature-label">
                                                            Cafeteria: Breakfast, Lunch, Supper
                                                        </div>
                                                        <div class="stm-pricing-table__feature-value"><i
                                                                class="stm-icon stm-icon-times"></i>
                                                        </div>
                                                    </li>
                                                    <li class="stm-pricing-table__feature">
                                                        <div class="stm-pricing-table__feature-label">
                                                            Sport games
                                                        </div>
                                                        <div class="stm-pricing-table__feature-value"><i
                                                                class="stm-icon stm-icon-times"></i>
                                                        </div>
                                                    </li>
                                                </ul>
                                                <div class="stm-pricing-table__action"><a href="#"
                                                                                          style="border-color:#ff6666"
                                                                                          target="_self"
                                                                                          class="stm-btn stm-btn_outline stm-btn_md stm-btn_icon-left"><i
                                                            class="stm-icon stm-icon-duck"
                                                            style="margin-top:-4px;margin-right:19px"></i>
                                                        Purchase</a></div>
                                            </div>
                                        </div>
                                        <div class="stm-pricing__tables-col">
                                            <div class="stm-pricing-table" style="border-color:#bd66ff">
                                                <div class="stm-pricing-table__title">Premium</div>
                                                <ul class="stm-pricing-table__periods">
                                                    <li class="stm-pricing-table__periods-item stm-pricing-table__periods-item_active"
                                                        data-period="monthly">
                                                        <div class="stm-pricing-table__price"
                                                             style="color:#bd66ff"> $40
                                                        </div>
                                                        <div class="stm-pricing-table__period"> per
                                                            month
                                                        </div>
                                                    </li>
                                                    <li class="stm-pricing-table__periods-item"
                                                        data-period="yearly">
                                                        <div class="stm-pricing-table__price"
                                                             style="color:#bd66ff"> $38
                                                        </div>
                                                        <div class="stm-pricing-table__period"> per
                                                            month
                                                        </div>
                                                    </li>
                                                </ul>
                                                <ul class="stm-pricing-table__features">
                                                    <li class="stm-pricing-table__feature">
                                                        <div class="stm-pricing-table__feature-label">
                                                            Languages: Chinese, Spanish, Japanese
                                                        </div>
                                                        <div class="stm-pricing-table__feature-value"><i
                                                                class="stm-icon stm-icon-check"></i>
                                                        </div>
                                                    </li>
                                                    <li class="stm-pricing-table__feature">
                                                        <div class="stm-pricing-table__feature-label">
                                                            Cafeteria: Breakfast, Lunch, Supper
                                                        </div>
                                                        <div class="stm-pricing-table__feature-value"><i
                                                                class="stm-icon stm-icon-check"></i>
                                                        </div>
                                                    </li>
                                                    <li class="stm-pricing-table__feature">
                                                        <div class="stm-pricing-table__feature-label">
                                                            Sport games and activities
                                                        </div>
                                                        <div class="stm-pricing-table__feature-value"><i
                                                                class="stm-icon stm-icon-check"></i>
                                                        </div>
                                                    </li>
                                                    <li class="stm-pricing-table__feature">
                                                        <div class="stm-pricing-table__feature-label">
                                                            Camping on Mountains
                                                        </div>
                                                        <div class="stm-pricing-table__feature-value"><i
                                                                class="stm-icon stm-icon-check"></i>
                                                        </div>
                                                    </li>
                                                    <li class="stm-pricing-table__feature">
                                                        <div class="stm-pricing-table__feature-label">
                                                            School Bus
                                                        </div>
                                                        <div class="stm-pricing-table__feature-value"><i
                                                                class="stm-icon stm-icon-check"></i>
                                                        </div>
                                                    </li>
                                                    <li class="stm-pricing-table__feature">
                                                        <div class="stm-pricing-table__feature-label">
                                                            Cafeteria: Breakfast, Lunch, Supper
                                                        </div>
                                                        <div class="stm-pricing-table__feature-value"><i
                                                                class="stm-icon stm-icon-check"></i>
                                                        </div>
                                                    </li>
                                                    <li class="stm-pricing-table__feature">
                                                        <div class="stm-pricing-table__feature-label">
                                                            Sport games
                                                        </div>
                                                        <div class="stm-pricing-table__feature-value"><i
                                                                class="stm-icon stm-icon-times"></i>
                                                        </div>
                                                    </li>
                                                </ul>
                                                <div class="stm-pricing-table__action"><a href="#"
                                                                                          style="border-color:#bd66ff"
                                                                                          target="_self"
                                                                                          class="stm-btn stm-btn_outline stm-btn_md stm-btn_icon-left"><i
                                                            class="stm-icon stm-icon-duck"
                                                            style="margin-top:-4px;margin-right:19px"></i>
                                                        Purchase</a></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="vc_row wpb_row vc_row-fluid vc_custom_1475124406861">
            <div class="wpb_column vc_column_container vc_col-sm-12">
                <div class="vc_column-inner">
                    <div class="wpb_wrapper"><h3
                            class="stm-title stm-font_color_lilac stm-title_sep_bottom"
                            style="text-align:center;margin-bottom:64px"> Counters </h3>
                        <div class="vc_row wpb_row vc_inner vc_row-fluid">
                            <div class="wpb_column vc_column_container vc_col-sm-2 vc_hidden-sm vc_hidden-xs">
                                <div class="vc_column-inner">
                                    <div class="wpb_wrapper"></div>
                                </div>
                            </div>
                            <div class="wpb_column vc_column_container vc_col-sm-3 vc_col-lg-2 vc_col-md-2">
                                <div class="vc_column-inner">
                                    <div class="wpb_wrapper">
                                        <div class="vc_pie_chart wpb_content_element vc_ready"
                                             data-pie-value="50" data-pie-label-value="50"
                                             data-pie-units="%" data-pie-color="#95cc47">
                                            <div class="wpb_wrapper">
                                                <div class="vc_pie_wrapper" style="width: 126px;"><span
                                                        class="vc_pie_chart_back"
                                                        style="border-color: rgb(149, 204, 71); width: 126px; height: 126px;"></span>
                                                    <div class="vc_pie_chart_group">
                                                        <div class="vc_pie_chart_value"
                                                             style="width: 126px; height: 126px; line-height: 126px;"></div>
                                                        <div class="vc_pie_chart_label">Algebra</div>
                                                    </div>
                                                    <canvas width="126px" height="126px"></canvas>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="wpb_column vc_column_container vc_col-sm-3 vc_col-lg-2 vc_col-md-2">
                                <div class="vc_column-inner">
                                    <div class="wpb_wrapper">
                                        <div class="vc_pie_chart wpb_content_element vc_ready"
                                             data-pie-value="63" data-pie-label-value="63"
                                             data-pie-units="%" data-pie-color="#bd66ff">
                                            <div class="wpb_wrapper">
                                                <div class="vc_pie_wrapper" style="width: 126px;"><span
                                                        class="vc_pie_chart_back"
                                                        style="border-color: rgb(189, 102, 255); width: 126px; height: 126px;"></span>
                                                    <div class="vc_pie_chart_group">
                                                        <div class="vc_pie_chart_value"
                                                             style="width: 126px; height: 126px; line-height: 126px;"></div>
                                                        <div class="vc_pie_chart_label">Chemistry</div>
                                                    </div>
                                                    <canvas width="126px" height="126px"></canvas>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="wpb_column vc_column_container vc_col-sm-3 vc_col-lg-2 vc_col-md-2">
                                <div class="vc_column-inner">
                                    <div class="wpb_wrapper">
                                        <div class="vc_pie_chart wpb_content_element vc_ready"
                                             data-pie-value="75" data-pie-label-value="75"
                                             data-pie-units="%" data-pie-color="#ffaf40">
                                            <div class="wpb_wrapper">
                                                <div class="vc_pie_wrapper" style="width: 126px;"><span
                                                        class="vc_pie_chart_back"
                                                        style="border-color: rgb(255, 175, 64); width: 126px; height: 126px;"></span>
                                                    <div class="vc_pie_chart_group">
                                                        <div class="vc_pie_chart_value"
                                                             style="width: 126px; height: 126px; line-height: 126px;"></div>
                                                        <div class="vc_pie_chart_label">Literature</div>
                                                    </div>
                                                    <canvas width="126px" height="126px"></canvas>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="wpb_column vc_column_container vc_col-sm-3 vc_col-lg-2 vc_col-md-2">
                                <div class="vc_column-inner">
                                    <div class="wpb_wrapper">
                                        <div class="vc_pie_chart wpb_content_element vc_ready"
                                             data-pie-value="87" data-pie-label-value="87"
                                             data-pie-units="%" data-pie-color="#ff6666">
                                            <div class="wpb_wrapper">
                                                <div class="vc_pie_wrapper" style="width: 126px;"><span
                                                        class="vc_pie_chart_back"
                                                        style="border-color: rgb(255, 102, 102); width: 126px; height: 126px;"></span>
                                                    <div class="vc_pie_chart_group">
                                                        <div class="vc_pie_chart_value"
                                                             style="width: 126px; height: 126px; line-height: 126px;"></div>
                                                        <div class="vc_pie_chart_label">Geography</div>
                                                    </div>
                                                    <canvas width="126px" height="126px"></canvas>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="wpb_column vc_column_container vc_col-sm-2 vc_hidden-sm vc_hidden-xs">
                                <div class="vc_column-inner">
                                    <div class="wpb_wrapper"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="vc_row wpb_row vc_row-fluid vc_custom_1475125782407">
            <div class="wpb_column vc_column_container vc_col-sm-12">
                <div class="vc_column-inner">
                    <div class="wpb_wrapper"><h3
                            class="stm-title stm-font_color_lilac stm-title_sep_bottom"
                            style="text-align:center"> Progress bar </h3>
                        <div class="vc_row wpb_row vc_inner vc_row-fluid">
                            <div class="wpb_column vc_column_container vc_col-sm-1 vc_col-lg-1 vc_col-md-1 vc_hidden-sm vc_hidden-xs">
                                <div class="vc_column-inner">
                                    <div class="wpb_wrapper"></div>
                                </div>
                            </div>
                            <div class="wpb_column vc_column_container vc_col-sm-6 vc_col-lg-4 vc_col-md-4">
                                <div class="vc_column-inner">
                                    <div class="wpb_wrapper">
                                        <div class="vc_progress_bar wpb_content_element vc_progress-bar-color-white vc_progress-bar_view_large">
                                            <small class="vc_label">Geography</small>
                                            <div class="vc_general vc_single_bar vc_progress-bar-color-bar_blue">
                                                                <span class="vc_bar" data-percentage-value="87"
                                                                      data-value="87"><span
                                                                        class="vc_bar_val">87%</span></span></div>
                                            <small class="vc_label">Literature</small>
                                            <div class="vc_general vc_single_bar vc_progress-bar-color-bar_blue">
                                                                <span class="vc_bar" data-percentage-value="75"
                                                                      data-value="75"><span
                                                                        class="vc_bar_val">75%</span></span></div>
                                            <small class="vc_label">Chemistry</small>
                                            <div class="vc_general vc_single_bar vc_progress-bar-color-bar_blue">
                                                                <span class="vc_bar" data-percentage-value="63"
                                                                      data-value="63"><span
                                                                        class="vc_bar_val">63%</span></span></div>
                                            <small class="vc_label">Algebra</small>
                                            <div class="vc_general vc_single_bar vc_progress-bar-color-bar_blue">
                                                                <span class="vc_bar" data-percentage-value="50"
                                                                      data-value="50"><span
                                                                        class="vc_bar_val">50%</span></span></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="wpb_column vc_column_container vc_col-sm-2 vc_col-lg-2 vc_col-md-2 vc_hidden-sm vc_hidden-xs">
                                <div class="vc_column-inner">
                                    <div class="wpb_wrapper"></div>
                                </div>
                            </div>
                            <div class="wpb_column vc_column_container vc_col-sm-6 vc_col-lg-4 vc_col-md-4">
                                <div class="vc_column-inner">
                                    <div class="wpb_wrapper">
                                        <div class="vc_progress_bar wpb_content_element vc_progress-bar-color-bar_grey vc_progress-bar_view_compact">
                                            <div class="vc_bar_heading"><small class="vc_label">Geography</small><span
                                                    class="vc_bar_val">87%</span></div>
                                            <div class="vc_general vc_single_bar"><span class="vc_bar"
                                                                                        data-percentage-value="87"
                                                                                        data-value="87"
                                                                                        style="background-color: #ff6682;"></span>
                                            </div>
                                            <div class="vc_bar_heading"><small class="vc_label">Literature</small><span
                                                    class="vc_bar_val">75%</span></div>
                                            <div class="vc_general vc_single_bar"><span class="vc_bar"
                                                                                        data-percentage-value="75"
                                                                                        data-value="75"
                                                                                        style="background-color: #ffdd00;"></span>
                                            </div>
                                            <div class="vc_bar_heading"><small class="vc_label">Chemistry</small><span
                                                    class="vc_bar_val">63%</span></div>
                                            <div class="vc_general vc_single_bar"><span class="vc_bar"
                                                                                        data-percentage-value="63"
                                                                                        data-value="63"
                                                                                        style="background-color: #95cc47;"></span>
                                            </div>
                                            <div class="vc_bar_heading"><small
                                                    class="vc_label">Algebra</small><span
                                                    class="vc_bar_val">50%</span></div>
                                            <div class="vc_general vc_single_bar"><span class="vc_bar"
                                                                                        data-percentage-value="50"
                                                                                        data-value="50"
                                                                                        style="background-color: #bd66ff;"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="wpb_column vc_column_container vc_col-sm-1 vc_col-lg-1 vc_col-md-1 vc_hidden-sm vc_hidden-xs">
                                <div class="vc_column-inner">
                                    <div class="wpb_wrapper"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="vc_row wpb_row vc_row-fluid">
            <div class="wpb_column vc_column_container vc_col-sm-12">
                <div class="vc_column-inner">
                    <div class="wpb_wrapper"><h3
                            class="stm-title stm-font_color_lilac stm-title_sep_bottom"
                            style="text-align:center;margin-bottom:66px"> Separators </h3>
                        <div class="stm-separator stm-clearfix stm-separator_has_item"
                             style="width:350px;margin-left: auto;margin-right: auto">
                            <div class="stm-separator__line-holder">
                                <div class="stm-separator__line stm-border-bottom_color_pink"
                                     style="border-bottom-width:1px;border-bottom-style:dotted"></div>
                            </div>
                            <div class="stm-separator__item stm-separator__item_type_text stm-font_color_lilac"
                                 style="font-size:14px">SHORT
                            </div>
                            <div class="stm-separator__line-holder">
                                <div class="stm-separator__line stm-border-bottom_color_pink"
                                     style="border-bottom-width:1px;border-bottom-style:dotted"></div>
                            </div>
                        </div>
                        <div class="stm-separator stm-clearfix"
                             style="width:350px;margin-left: auto;margin-right: auto">
                            <div class="stm-separator__line"
                                 style="border-bottom-width:1px;border-bottom-style:solid;border-bottom-color:#e6e6e6"></div>
                        </div>
                        <div class="stm-separator stm-clearfix stm-separator_has_item"
                             style="width:350px;margin-left: auto;margin-right: auto">
                            <div class="stm-separator__line-holder">
                                <div class="stm-separator__line stm-border-bottom_color_lilac"
                                     style="border-bottom-width:1px;border-bottom-style:solid"></div>
                            </div>
                            <div class="stm-separator__item stm-separator__item_type_icon stm-font_color_pink">
                                <i class="fa fa-graduation-cap"></i></div>
                            <div class="stm-separator__line-holder">
                                <div class="stm-separator__line stm-border-bottom_color_lilac"
                                     style="border-bottom-width:1px;border-bottom-style:solid"></div>
                            </div>
                        </div>
                        <div class="stm-separator stm-clearfix stm-separator_has_item"
                             style="width:100%;margin-left: auto;margin-right: auto">
                            <div class="stm-separator__line-holder">
                                <div class="stm-separator__line stm-border-bottom_color_pink"
                                     style="border-bottom-width:1px;border-bottom-style:dashed"></div>
                            </div>
                            <div class="stm-separator__item stm-separator__item_type_text stm-font_color_lilac"
                                 style="font-size:14px">DEFAULT
                            </div>
                            <div class="stm-separator__line-holder">
                                <div class="stm-separator__line stm-border-bottom_color_pink"
                                     style="border-bottom-width:1px;border-bottom-style:dashed"></div>
                            </div>
                        </div>
                        <div class="stm-separator stm-clearfix"
                             style="width:100%;margin-left: auto;margin-right: auto">
                            <div class="stm-separator__line"
                                 style="border-bottom-width:1px;border-bottom-style:solid;border-bottom-color:#e6e6e6"></div>
                        </div>
                        <div class="stm-separator stm-clearfix stm-separator_has_item"
                             style="width:100%;margin-left: auto;margin-right: auto">
                            <div class="stm-separator__line-holder">
                                <div class="stm-separator__line stm-border-bottom_color_lilac"
                                     style="border-bottom-width:4px;border-bottom-style:double"></div>
                            </div>
                            <div class="stm-separator__item stm-separator__item_type_icon stm-font_color_pink">
                                <i class="fa fa-bell"></i></div>
                            <div class="stm-separator__line-holder">
                                <div class="stm-separator__line stm-border-bottom_color_lilac"
                                     style="border-bottom-width:4px;border-bottom-style:double"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div data-vc-full-width="true" data-vc-full-width-init="true" data-vc-stretch-content="true"
             class="vc_row wpb_row vc_row-fluid vc_custom_1475127820802 vc_row-no-padding"
             style="position: relative; left: -93.5px; box-sizing: border-box; width: 937px;">
            <div class="wpb_column vc_column_container vc_col-sm-12">
                <div class="vc_column-inner">
                    <div class="wpb_wrapper">
                        <div class="stm-separator stm-clearfix stm-separator_has_item"
                             style="width:100%;margin-left: auto;margin-right: auto">
                            <div class="stm-separator__line-holder">
                                <div class="stm-separator__line stm-border-bottom_color_pink"
                                     style="border-bottom-width:1px;border-bottom-style:dashed"></div>
                            </div>
                            <div class="stm-separator__item stm-separator__item_type_text stm-font_color_lilac">
                                FULLWIDTH
                            </div>
                            <div class="stm-separator__line-holder">
                                <div class="stm-separator__line stm-border-bottom_color_pink"
                                     style="border-bottom-width:1px;border-bottom-style:dashed"></div>
                            </div>
                        </div>
                        <div class="stm-separator stm-clearfix"
                             style="width:100%;margin-left: auto;margin-right: auto">
                            <div class="stm-separator__line"
                                 style="border-bottom-width:1px;border-bottom-style:solid;border-bottom-color:#e6e6e6"></div>
                        </div>
                        <div class="stm-separator stm-clearfix stm-separator_has_item"
                             style="width:100%;margin-left: auto;margin-right: auto">
                            <div class="stm-separator__line-holder">
                                <div class="stm-separator__line stm-border-bottom_color_lilac"
                                     style="border-bottom-width:1px;border-bottom-style:solid"></div>
                            </div>
                            <div class="stm-separator__item stm-separator__item_type_icon stm-font_color_pink">
                                <i class="fa fa-bus"></i></div>
                            <div class="stm-separator__line-holder">
                                <div class="stm-separator__line stm-border-bottom_color_lilac"
                                     style="border-bottom-width:1px;border-bottom-style:solid"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="vc_row-full-width vc_clearfix"></div>
        <div class="vc_row wpb_row vc_row-fluid vc_custom_1460088313335">
            <div class="wpb_column vc_column_container vc_col-sm-12">
                <div class="vc_column-inner">
                    <div class="wpb_wrapper"><h3
                            class="stm-title stm-font_color_lilac stm-title_sep_bottom"
                            style="text-align:center;margin-bottom:84px"> Message box </h3>
                        <div class="vc_row wpb_row vc_inner vc_row-fluid">
                            <div class="wpb_column vc_column_container vc_col-sm-6">
                                <div class="vc_column-inner">
                                    <div class="wpb_wrapper">
                                        <div class="vc_message_box vc_message_box-outline vc_message_box-square vc_color-info vc_message_icon_hide">
                                            <p>I am a message box with notification type. You can close
                                                me</p> <a href="#" class="vc_message_hide"><i
                                                    class="stm-icon stm-icon-times-thin"></i></a>
                                        </div>
                                        <div class="vc_message_box vc_message_box-outline vc_message_box-square vc_color-success vc_message_icon_hide">
                                            <p>I am a message box with success type. You can close
                                                me</p> <a href="#" class="vc_message_hide"><i
                                                    class="stm-icon stm-icon-times-thin"></i></a>
                                        </div>
                                        <div class="vc_message_box vc_message_box-outline vc_message_box-square vc_color-warning vc_message_icon_hide">
                                            <p>I am a message box with attention type. You can close
                                                me</p> <a href="#" class="vc_message_hide"><i
                                                    class="stm-icon stm-icon-times-thin"></i></a>
                                        </div>
                                        <div class="vc_message_box vc_message_box-outline vc_message_box-square vc_color-danger vc_message_icon_hide">
                                            <p>I am a message box with error type. You can close me</p>
                                            <a href="#" class="vc_message_hide"><i
                                                    class="stm-icon stm-icon-times-thin"></i></a>
                                        </div>
                                        <div class="stm-icon-box stm-icon-box_ic-pos_left stm-icon-box_style-4 vc_custom_1476092110529"
                                             style="text-align:left">
                                            <div class="stm-icon-box__ic-container"
                                                 style="font-size:30px;margin-top:6px;color:#81ca00">
                                                <span class="fa fa-thumbs-o-up"></span></div>
                                            <div class="stm-icon-box__content">
                                                <div class="stm-icon-box__descr stm-font_color_dark"
                                                     style="font-size:16px">Smarty Kindergarten is a
                                                    private yet affordable elementary school offering a
                                                    stimulating curriculum and a supportive environment
                                                    to ensure our students experience success every day.
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="wpb_column vc_column_container vc_col-sm-6">
                                <div class="vc_column-inner">
                                    <div class="wpb_wrapper">
                                        <div class="vc_message_box vc_message_box-outline vc_message_box-square vc_color-info">
                                            <div class="vc_message_box-icon"><i
                                                    class="fa fa-info-circle"></i></div>
                                            <p>I am a message box with notification type. You can close
                                                me</p>
                                            <a href="#" class="vc_message_hide"><i
                                                    class="stm-icon stm-icon-times-thin"></i></a>
                                        </div>
                                        <div class="vc_message_box vc_message_box-outline vc_message_box-square vc_color-success">
                                            <div class="vc_message_box-icon"><i
                                                    class="fa fa-check-circle"></i></div>
                                            <p>I am a message box with success type. You can close
                                                me</p>
                                            <a href="#" class="vc_message_hide"><i
                                                    class="stm-icon stm-icon-times-thin"></i></a>
                                        </div>
                                        <div class="vc_message_box vc_message_box-outline vc_message_box-square vc_color-warning">
                                            <div class="vc_message_box-icon"><i
                                                    class="fa fa-exclamation-triangle"></i></div>
                                            <p>I am a message box with attention type. You can close
                                                me</p>
                                            <a href="#" class="vc_message_hide"><i
                                                    class="stm-icon stm-icon-times-thin"></i></a>
                                        </div>
                                        <div class="vc_message_box vc_message_box-outline vc_message_box-square vc_color-danger">
                                            <div class="vc_message_box-icon"><i
                                                    class="fa fa-times-circle"></i></div>
                                            <p>I am a message box with error type. You can close me</p>
                                            <a href="#" class="vc_message_hide"><i
                                                    class="stm-icon stm-icon-times-thin"></i></a>
                                        </div>
                                        <div class="stm-icon-box stm-icon-box_ic-pos_left stm-icon-box_style-4 vc_custom_1476092259559"
                                             style="text-align:left">
                                            <div class="stm-icon-box__ic-container"
                                                 style="font-size:30px;margin-top:6px;color:#ffffff">
                                                <span class="fa fa-thumbs-o-up"></span></div>
                                            <div class="stm-icon-box__content">
                                                <div class="stm-icon-box__descr"
                                                     style="font-size:16px;color:#ffffff">Smarty
                                                    Kindergarten is a private yet affordable elementary
                                                    school offering a stimulating curriculum and a
                                                    supportive environment to ensure our students
                                                    experience success every day.
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</article>