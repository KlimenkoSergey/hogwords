<div class="top-bar-mobile">
    <div class="top-bar-mobile__search">
        <form role="search" method="get" id="searchform855" class="stm-search-form"
              action="/search/index"><input
                class="stm-search-form__field" type="search" value="" name="s" placeholder="Поиск..."
                required="">
            <button type="submit" class="stm-search-form__submit"><span class="stm-icon stm-icon-search"></span><span
                    class="stm-search-form__submit-text">Search</span></button>
        </form>
    </div>
</div>


<div class="header-mobile">
    <div class="header-mobile__logo"><a href="/"
                                        class="logo" title="Сервис изучения английского">
            <noscript><img
                    src="/static/img/logo.webp"
                    alt="Сервис изучения английского"></noscript>
            <img class="lazyload" style="margin-bottom: 35px;"
                 src="data:image/svg+xml,%3Csvg%20xmlns=%22http://www.w3.org/2000/svg%22%20viewBox=%220%200%20210%20140%22%3E%3C/svg%3E"
                 data-src="/static/img/logo.webp"
                 alt="Учить англиский онлайн"></a>
        <div class="header-mobile__nav-control"><span class="header-mobile__control-line"></span></div>
    </div>
    <div class="stm-nav stm-nav_type_mobile-header">
        <ul id="header-mobile-nav-menu" class="stm-nav__menu stm-nav__menu_type_mobile-header">
            <?= $this->render('submenu.php') ?>
        </ul>
    </div>
</div>