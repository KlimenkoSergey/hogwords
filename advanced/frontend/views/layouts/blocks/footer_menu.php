<footer id="colophon" class="footer footer_type_default">
    <div class="container">
        <div class="widget-area widget-area_type_footer">
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div id="text-2" class="widget widget_footer widget_text">
                        <div class="textwidget">
                            <p><a href="/kindergarten-two/">
                                    <noscript><img onload="Wpfcll.r(this,true);"
                                                   src="/static/img/logo.webp"
                                                   wpfc-data-original-src="/static/img/logo.webp"
                                                   alt="Smarty Kindergarten"/></noscript>
                                    <img class=" ls-is-cached lazyloaded"
                                         src="/static/img/logo.webp"
                                         data-src="/static/img/logo.webp"
                                         alt="Smarty Kindergarten"></a></p>
                            <p style="font-size: 14px; line-height: 20px;">Изучение английского онлайн</p></div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div id="stm_widget_contact_details-2" class="widget widget_footer widget_contact-details"><h4
                            class="widget_title">Контакты</h4>
                        <ul class="list list_unstyle list_contact-details">
                            <li class="list__item list__item_address">Пулюя2, Киев. Украина.
                            </li>
<!--                            <li class="list__item list__item_telephone"> +3 8098-870-7288</li>-->
<!--                            <li class="list__item list__item_fax">+3 8098-870-7288</li>-->
                            <li class="list__item list__item_email"><a href="mailto:info@hogwords.net">info@hogwords.net</a>
                            </li>
                            <li class="list__item list__item_schedule">Пн — Пт: 8.00 AM — 18.00</li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div id="nav_menu-2" class="widget widget_footer widget_nav_menu"><h4 class="widget_title">Быстрые ссылки</h4>
                        <div class="menu-footer-custom-menu-container">
                            <ul id="menu-footer-custom-menu" class="menu">
                                <li id="menu-item-4964"
                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home menu-item-4964">
                                    <a href="/">Главная</a>
                                </li>
<!--                                <li id="menu-item-4966"-->
<!--                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4966">-->
<!--                                    <a href="#">События</a>-->
<!--                                </li>-->
                                <li id="menu-item-4965"
                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4965">
                                    <a href="/site/about">О сервисе</a></li>


                                <li id="menu-item-4968"
                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4968">
                                    <a href="/site/contact">Контакты</a>
                                </li>


<!--                                <li id="menu-item-4970"-->
<!--                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4970">-->
<!--                                    <a href="#">Новости</a>-->
<!--                                </li>-->

                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
<!--                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">-->
<!--                    <div id="nav_menu-3" class="widget widget_footer widget_nav_menu"><h4 class="widget_title">-->
<!--                            Информация для</h4>-->
<!--                        <div class="menu-top-bar-container">-->
<!--                            <ul id="menu-top-bar" class="menu">-->
<!--                                <li id="menu-item-4973"-->
<!--                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4973">-->
<!--                                    <a href="#">Пожертвования</a>-->
<!--                                </li>-->
<!--                                <li id="menu-item-4974"-->
<!--                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4974">-->
<!--                                    <a href="#">FAQ</a>-->
<!--                                </li>-->
<!--                                <li id="menu-item-4975"-->
<!--                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4975">-->
<!--                                    <a href="#">Галерея</a>-->
<!--                                </li>-->
<!--                                <li id="menu-item-4976"-->
<!--                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4976">-->
<!--                                    <a href="/pages/view?id=1">Цены</a></li>-->
<!--                                <li id="menu-item-4977"-->
<!--                                    class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-4410 current_page_item menu-item-4977">-->
<!--                                    <a href="#"-->
<!--                                       aria-current="page">Program</a></li>-->
<!--                                <li id="menu-item-4978"-->
<!--                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4978">-->
<!--                                    <a href="#">Timetable</a>-->
<!--                                </li>-->
<!--                            </ul>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
            </div>
        </div>
        <div class="copyright">Copyright © HOGWORDS 2021
        </div>
    </div>
</footer>
<!-- BEGIN JIVOSITE CODE {literal} -->
<script type='text/javascript'>
    (function(){ var widget_id = 'b1PiAYy8rl';var d=document;var w=window;function l(){
        var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = '//code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);}if(d.readyState=='complete'){l();}else{if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})();</script>
<!-- {/literal} END JIVOSITE CODE -->
<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
    (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
        m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
    (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

    ym(85619563, "init", {
        clickmap:true,
        trackLinks:true,
        accurateTrackBounce:true,
        webvisor:true
    });
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/85619563" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->