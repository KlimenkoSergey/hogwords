
<div class="header-holder header-holder_view-style_1 header_holder_small">


    <header id="masthead" class="header header_view-style_1">
        <div class="container">
            <div class="header__content"><a href="/"
                                            class="logo" title="Учить англиский онлайн">
                    <noscript><img
                                src="/static/img/logo.webp"
                                alt="Учить англиский онлайн"></noscript>
                    <img class=" ls-is-cached lazyloaded"
                         src="/static/img/logo.png"
                         data-src="/static/img/logo.webp"
                         alt="Сервис изучения английского."></a>

                <div class="top-bar__search">
                    <form role="search" method="get" id="searchform898" class="stm-search-form"
                          action="/search/index"><input
                                class="stm-search-form__field" type="search" value="" name="s"
                                placeholder="Поиск..." required="">
                        <button type="submit" class="stm-search-form__submit"><span
                                    class="stm-icon stm-icon-search"></span><span
                                    class="stm-search-form__submit-text">Поиск</span></button>
                    </form>
                </div>
                <div class="nav_menu_indent"></div>
                <div class="stm-nav stm-nav_type_header">
                    <ul id="header-nav-menu" class="stm-nav__menu stm-nav__menu_type_header">
                        <!--                    <li id="menu-item-3291"-->
                        <!--                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-3291">-->
                        <!--                        <a href="/">Главная</a>-->
                        <!--                        <ul class="sub-menu">-->
                        <!--                            <li id="menu-item-3298"-->
                        <!--                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3298">-->
                        <!--                                <a href="https://smartyschool.stylemixthemes.com/kindergarten-two/meals/">Meals</a>-->
                        <!--                            </li>-->
                        <!--                            <li id="menu-item-3296"-->
                        <!--                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-3296">-->
                        <!--                                <a href="https://smartyschool.stylemixthemes.com/kindergarten-two/our-staff/">Staff</a>-->
                        <!--                                <ul class="sub-menu">-->
                        <!--                                    <li id="menu-item-4231"-->
                        <!--                                        class="menu-item menu-item-type-custom menu-item-object-custom menu-item-4231">-->
                        <!--                                        <a href="/kindergarten-two/teachers/mrs-simone-payne/">Staff – Iinfo</a>-->
                        <!--                                    </li>-->
                        <!--                                </ul>-->
                        <!--                            </li>-->
                        <!--                        </ul>-->
                        <!--                    </li>-->
                        <!--                    <li id="menu-item-4409"-->
                        <!--                        class="menu-item menu-item-type-post_type menu-item-object-page current-menu-ancestor current-menu-parent current_page_parent current_page_ancestor menu-item-has-children menu-item-4409">-->
                        <!--                        <a href="/site/about">О сервисе</a>-->
                        <!--                        <ul class="sub-menu">-->
                        <!--                            <li id="menu-item-4475"-->
                        <!--                                class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-4410 current_page_item menu-item-4475">-->
                        <!--                                <a href="https://smartyschool.stylemixthemes.com/kindergarten-two/program/"-->
                        <!--                                   aria-current="page">Programs – Single</a></li>-->
                        <!--                        </ul>-->
                        <!--                        -->
                        <!--                    </li>-->
                        <?= $this->render('submenu.php') ?>
                    </ul>
                </div>
            </div>
        </div>
    </header>

    <div class="top-bar-mobile">
        <div class="top-bar-mobile__search">
            <form role="search" method="get" id="searchform855" class="stm-search-form"
                  action="/search/index"><input
                        class="stm-search-form__field" type="search" value="" name="s" placeholder="Поиск..."
                        required="">
                <button type="submit" class="stm-search-form__submit"><span class="stm-icon stm-icon-search"></span><span
                            class="stm-search-form__submit-text">Search</span></button>
            </form>
        </div>
    </div>


    <div class="header-mobile">
        <div class="header-mobile__logo"><a href="/"
                                            class="logo" title="Сервис изучения английского">
                <noscript><img
                            src="/static/img/logo.webp"
                            alt="Сервис изучения английского"></noscript>
                <img class="lazyload" style="margin-bottom: 35px;"
                     src="data:image/svg+xml,%3Csvg%20xmlns=%22http://www.w3.org/2000/svg%22%20viewBox=%220%200%20210%20140%22%3E%3C/svg%3E"
                     data-src="/static/img/logo.webp"
                     alt="Учить англиский онлайн"></a>
            <div class="header-mobile__nav-control"><span class="header-mobile__control-line"></span></div>
        </div>
        <div class="stm-nav stm-nav_type_mobile-header">
            <ul id="header-mobile-nav-menu" class="stm-nav__menu stm-nav__menu_type_mobile-header">
                <?= $this->render('submenu.php') ?>
            </ul>
        </div>
    </div>

</div>