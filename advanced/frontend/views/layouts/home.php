<?php

/* @var $this \yii\web\View */

/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" id="stm-site-preloader" class="no-js">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=0">
	<link rel="preconnect" href="https://fonts.googleapis.com">
	<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
	<link href="https://fonts.googleapis.com/css2?family=Just+Another+Hand&display=swap" rel="stylesheet">
    
    
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
	 <?php $this->head() ?>
	  <link href="https://fonts.gstatic.com" crossorigin="anonymous" rel="preconnect">
    
    <noscript><style>.woocommerce-product-gallery{opacity:1 !important;}</style></noscript>
    <link rel="icon"
          href="/static/img/cropped-favicon-150x150.png"
          sizes="32x32">
    <link rel="icon"
          href="/static/img/cropped-favicon-300x300.png"
          sizes="192x192">
    <link rel="apple-touch-icon"
          href="/static/img/cropped-favicon-180x180.png">
    <meta name="msapplication-TileImage"
          content="/static/img/cropped-favicon-300x300.png">
    <style data-type="vc_shortcodes-custom-css">.vc_custom_1570165198308{background:#fea633 url(https://smartyschool.stylemixthemes.com/kindergarten-two/wp-content/uploads/2019/10/main_bg.png?id=5133) !important;background-position:center !important;background-repeat:no-repeat !important;background-size:cover !important;}
        .vc_custom_1570421691129{margin-top:-115px !important;margin-bottom:0px !important;padding-bottom:0px !important;background-image:url(https://smartyschool.stylemixthemes.com/kindergarten-two/wp-content/uploads/2019/10/wave_bottom.png?id=5070) !important;background-position:0 0 !important;background-repeat:no-repeat !important;}
        .vc_custom_1570269272221{background-color:#ffffff !important;}
        .vc_custom_1570015394508{background-color:#ffffff !important;background-position:0 0 !important;background-repeat:no-repeat !important;}
        .vc_custom_1570429434926{margin-bottom:0px !important;padding-bottom:0px !important;background-image:url(https://smartyschool.stylemixthemes.com/kindergarten-two/wp-content/uploads/2019/10/wave_top.png?id=5093) !important;background-position:0 0 !important;background-repeat:no-repeat !important;}
        .vc_custom_1570179319698{margin-top:-115px !important;background:#007eff url(https://smartyschool.stylemixthemes.com/kindergarten-two/wp-content/uploads/2019/10/main_bg.png?id=5133) !important;background-position:0 0 !important;background-repeat:repeat !important;border-radius:20px !important;}
        .vc_custom_1570178716725{margin-top:-115px !important;margin-bottom:0px !important;padding-bottom:0px !important;background-image:url(https://smartyschool.stylemixthemes.com/kindergarten-two/wp-content/uploads/2019/10/wave_bottom.png?id=5070) !important;background-position:0 0 !important;background-repeat:no-repeat !important;}
        .vc_custom_1570103838348{margin-bottom:0px !important;padding-bottom:0px !important;background-image:url(https://smartyschool.stylemixthemes.com/kindergarten-two/wp-content/uploads/2019/10/wave_top.png?id=5093) !important;background-position:0 0 !important;background-repeat:no-repeat !important;}
        .vc_custom_1570165106385{margin-top:-115px !important;background:#fea633 url(https://smartyschool.stylemixthemes.com/kindergarten-two/wp-content/uploads/2019/10/main_bg.png?id=5133) !important;background-position:0 0 !important;background-repeat:no-repeat !important;}
        .vc_custom_1570684770623{margin-top:-114px !important;margin-bottom:0px !important;padding-bottom:0px !important;background-image:url(https://smartyschool.stylemixthemes.com/kindergarten-two/wp-content/uploads/2019/10/wave_bottom.png?id=5070) !important;background-position:0 0 !important;background-repeat:no-repeat !important;}
        .vc_custom_1570168473530{background-position:center !important;background-repeat:no-repeat !important;background-size:cover !important;}
        .vc_custom_1570179806247{background:#007eff url(https://smartyschool.stylemixthemes.com/kindergarten-two/wp-content/uploads/2019/10/main_bg.png?id=5133) !important;background-position:center !important;background-repeat:no-repeat !important;background-size:cover !important;border-radius:20px !important;}
        .vc_custom_1570015088817{padding-top:0px !important;}
        .vc_custom_1570015088817{padding-top:0px !important;}
        .vc_custom_1570015088817{padding-top:0px !important;}
        .vc_custom_1570015088817{padding-top:0px !important;}
        .vc_custom_1570015088817{padding-top:0px !important;}</style><noscript><style>.wpb_animate_when_almost_visible{opacity:1;}</style></noscript>


    <link rel="stylesheet" href="/static/css/style3.css">
</head>

<body class="home page-template-default page page-id-19 theme-smarty woocommerce-no-js wpb-js-composer js-comp-ver-6.5.0 vc_responsive ovf_hidden">
<div id="preloader">
  <div id="loader"></div>
</div>


<?php $this->beginBody() ?>
<div id="wrapper">
    <?= $this->render("blocks/menu.php") ?>
    
    
    <div class="content content_type_vc"> <div class="container"> <main class="main"> 
	<?= Alert::widget() ?>
                <?= $content ?>
				
	</main></div></div>
			<?= $this->render("blocks/footer_menu.php") ?>

    <div class="container">
	
	  <table>
            <tr>
                <td width="130"><a href="https://freekassa.ru" target="_blank"  rel="noopener noreferrer nofollow">
            <img src="https://cdn.freekassa.ru/banners/small-white-2.png" title="Прием платежей на сайте для физических лиц и т.д.">
        </a></td>
                <td width="130" >



                </td>
                <td></td>
                
            </tr>
            
        </table>


        
		
		 
		
    </div>
			
			</div><noscript><style>.lazyload{display:none;}</style></noscript>
<link rel="stylesheet" media="print" onload="this.onload=null;this.media='all';" id="ao_optimized_gfonts" wpfc-href="https://fonts.googleapis.com/css?family=Lato%3A400%2C700%7CQuicksand%3A300%2C300i%2C400%2C400i%2C500%2C500i%2C600%2C600i%2C700%2C700i%2C800%2C800i%7CPatrick+Hand%3A400%2C400i%2C700%7CQuicksand%3Aregular%2C700%2C300%7CPatrick+Hand%3Aregular%7CPatrick+Hand:400%7CQuicksand:500%2C400%7CRoboto:400&#038;subset=latin%2Clatin-ext&amp;display=swap"/>



<script type='text/javascript' src='/static/js/jquery.min.js?ver=3.5.1' id='jquery-core-js'></script>
<script type='text/javascript' src='/static/js/jquery-migrate.min.js?ver=3.3.2' id='jquery-migrate-js'></script>
<script type='text/javascript' src='/static/js/rbtools.min.js?ver=6.0.2' id='tp-tools-js'></script>
<script type='text/javascript' src="/static/js/jquery-3.6.0.min.js"></script>
<script type='text/javascript' src="/static/js/main.js"></script>
<script type='text/javascript' src='/static/js/jquery.spincrement.min.js'></script>
<script type='text/javascript' src='/static/js/jquery.spincrement.js'></script>
<script type='text/javascript' src='/static/js/jquery.animateNumber.min.js'></script>
<script type='text/javascript' src='/static/js/jquery.animateNumber.js'></script>
<script type='text/javascript' src='/static/js/rs6.min.js?ver=6.2.6' id='revmin-js'></script>
<script type='text/javascript' src='/static/js/jquery.blockUI.min.js?ver=2.70' id='jquery-blockui-js'></script>
<!--<script type='text/javascript' src='/static/js/add-to-cart.min.js?ver=4.9.0' id='wc-add-to-cart-js'></script>-->



<script>

$(window).scroll(function() {
    if ($(window).scrollTop() > $(".numb_section").offset().top - 450) {
		$(".nm_one span").spincrement({
			thousandSeparator: " ",
			duration: 2000,
			to: 10000,
			from: 0
		});
		$(".nm_two span").spincrement({
			thousandSeparator: "",
			duration: 2000,
			to: 150,
			from: 0
		});
		$(".nm_three span").spincrement({
			thousandSeparator: "",
			duration: 2000,
			to: 55,
			from: 0
		});
		$(".nm_four span").spincrement({
			thousandSeparator: "",
			duration: 2000,
			to: 500,
			from: 0
		});
    }
});

$(".stm-pricing__filter a").on("click", function() {
	var per_fil = $(this).data("period-filter");
	
	if(per_fil == "monthly") {
	
		$(this).parent(".stm_month").addClass("stm-pricing__filter_active");
		$(".stm-pricing__filter_fluid-hover").css("left", "0");
		
		$(this).parent(".stm_year").removeClass("stm-pricing__filter_active");
		
		$(".spt_month").addClass("stm-pricing-table__periods-item_active");
		$(".spt_year").removeClass("stm-pricing-table__periods-item_active");
	} else if(per_fil == "yearly") {
	
		$(this).parent(".stm_year").addClass("stm-pricing__filter_active");
		$(".stm-pricing__filter_fluid-hover").css("left", "126.587px");
		
		$(this).parent(".stm_month").removeClass("stm-pricing__filter_active");
		$(".spt_month").removeClass("stm-pricing-table__periods-item_active");
		$(".spt_year").addClass("stm-pricing-table__periods-item_active");
	}
	
});


</script>



<script>function setREVStartSize(e){
//window.requestAnimationFrame(function(){
        window.RSIW=window.RSIW===undefined ? window.innerWidth:window.RSIW;
        window.RSIH=window.RSIH===undefined ? window.innerHeight:window.RSIH;
        try {
            var pw=document.getElementById(e.c).parentNode.offsetWidth,
                newh;
            pw=pw===0||isNaN(pw) ? window.RSIW:pw;
            e.tabw=e.tabw===undefined ? 0:parseInt(e.tabw);
            e.thumbw=e.thumbw===undefined ? 0:parseInt(e.thumbw);
            e.tabh=e.tabh===undefined ? 0:parseInt(e.tabh);
            e.thumbh=e.thumbh===undefined ? 0:parseInt(e.thumbh);
            e.tabhide=e.tabhide===undefined ? 0:parseInt(e.tabhide);
            e.thumbhide=e.thumbhide===undefined ? 0:parseInt(e.thumbhide);
            e.mh=e.mh===undefined||e.mh==""||e.mh==="auto" ? 0:parseInt(e.mh,0);
            if(e.layout==="fullscreen"||e.l==="fullscreen")
                newh=Math.max(e.mh,window.RSIH);
            else{
                e.gw=Array.isArray(e.gw) ? e.gw:[e.gw];
                for (var i in e.rl) if(e.gw[i]===undefined||e.gw[i]===0) e.gw[i]=e.gw[i-1];
                e.gh=e.el===undefined||e.el===""||(Array.isArray(e.el)&&e.el.length==0)? e.gh:e.el;
                e.gh=Array.isArray(e.gh) ? e.gh:[e.gh];
                for (var i in e.rl) if(e.gh[i]===undefined||e.gh[i]===0) e.gh[i]=e.gh[i-1];
                var nl=new Array(e.rl.length),
                    ix=0,
                    sl;
                e.tabw=e.tabhide>=pw ? 0:e.tabw;
                e.thumbw=e.thumbhide>=pw ? 0:e.thumbw;
                e.tabh=e.tabhide>=pw ? 0:e.tabh;
                e.thumbh=e.thumbhide>=pw ? 0:e.thumbh;
                for (var i in e.rl) nl[i]=e.rl[i]<window.RSIW ? 0:e.rl[i];
                sl=nl[0];
                for (var i in nl) if(sl>nl[i]&&nl[i]>0){ sl=nl[i]; ix=i;}
                var m=pw>(e.gw[ix]+e.tabw+e.thumbw) ? 1:(pw-(e.tabw+e.thumbw)) / (e.gw[ix]);
                newh=(e.gh[ix] * m) + (e.tabh + e.thumbh);
            }
            if(window.rs_init_css===undefined) window.rs_init_css=document.head.appendChild(document.createElement("style"));
            document.getElementById(e.c).height=newh+"px";
            window.rs_init_css.innerHTML +="#"+e.c+"_wrapper { height: "+newh+"px }";
        } catch(e){
            console.log("Failure at Presize of Slider:" + e)
        }};</script>
<script>setREVStartSize({c: 'rev_slider_2_1',rl:[1240,1024,778,480],el:[800,768,768,720],gw:[1170,720,680,480],gh:[800,768,768,720],type:'standard',justify:'',layout:'fullwidth',mh:"0"});
    var	revapi2,
        tpj;
    jQuery(function(){
        tpj=jQuery;
        revapi2=tpj("#rev_slider_2_1")
        if(revapi2==undefined||revapi2.revolution==undefined){
            revslider_showDoubleJqueryError("rev_slider_2_1");
        }else{
            revapi2.revolution({
                sliderLayout:"fullwidth",
                visibilityLevels:"1240,1024,778,480",
                gridwidth:"1170,720,680,480",
                gridheight:"800,768,768,720",
                perspectiveType:"local",
                editorheight:"800,768,768,720",
                responsiveLevels:"1240,1024,778,480",
                disableProgressBar:"on",
                navigation: {
                    onHoverStop:false
                },
                fallbacks: {
                    allowHTML5AutoPlayOnAndroid:true
                },
            });
        }});</script>
<script>(function($){
        "use strict";
        var spacingID='stm-spacing-60dad28944d57',
            lgSpacing='30',
            mdSpacing='30',
            smSpacing='150',
            xsSpacing='170';
        function smartySpacing(){
            if(window.matchMedia("(min-width: 1200px)").matches&&lgSpacing){
                $('#' + spacingID).css("height", lgSpacing);
            }else if(window.matchMedia("(max-width: 1199px) and (min-width: 992px)").matches&&mdSpacing){
                $('#' + spacingID).css("height", mdSpacing);
            }else if(window.matchMedia("(max-width: 991px) and (min-width: 768px)").matches&&smSpacing){
                $('#' + spacingID).css("height", smSpacing);
            }else if(window.matchMedia("(max-width: 767px)").matches&&xsSpacing){
                $('#' + spacingID).css("height", xsSpacing);
            }else{
                $('#' + spacingID).css("height", "");
            }}
        $(document).ready(function(){
            smartySpacing();
        });
        $(window).resize(function(){
            smartySpacing();
        });
    })(jQuery);</script>
<script>(function($){
        "use strict";
        var spacingID='stm-spacing-60dad2894630f',
            lgSpacing='115',
            mdSpacing='115',
            smSpacing='115',
            xsSpacing='115';
        function smartySpacing(){
            if(window.matchMedia("(min-width: 1200px)").matches&&lgSpacing){
                $('#' + spacingID).css("height", lgSpacing);
            }else if(window.matchMedia("(max-width: 1199px) and (min-width: 992px)").matches&&mdSpacing){
                $('#' + spacingID).css("height", mdSpacing);
            }else if(window.matchMedia("(max-width: 991px) and (min-width: 768px)").matches&&smSpacing){
                $('#' + spacingID).css("height", smSpacing);
            }else if(window.matchMedia("(max-width: 767px)").matches&&xsSpacing){
                $('#' + spacingID).css("height", xsSpacing);
            }else{
                $('#' + spacingID).css("height", "");
            }}
        $(document).ready(function(){
            smartySpacing();
        });
        $(window).resize(function(){
            smartySpacing();
        });
    })(jQuery);</script>
<script>(function($){
        "use strict";
        var spacingID='stm-spacing-60dad28946a0b',
            lgSpacing='50',
            mdSpacing='50',
            smSpacing='50',
            xsSpacing='50';
        function smartySpacing(){
            if(window.matchMedia("(min-width: 1200px)").matches&&lgSpacing){
                $('#' + spacingID).css("height", lgSpacing);
            }else if(window.matchMedia("(max-width: 1199px) and (min-width: 992px)").matches&&mdSpacing){
                $('#' + spacingID).css("height", mdSpacing);
            }else if(window.matchMedia("(max-width: 991px) and (min-width: 768px)").matches&&smSpacing){
                $('#' + spacingID).css("height", smSpacing);
            }else if(window.matchMedia("(max-width: 767px)").matches&&xsSpacing){
                $('#' + spacingID).css("height", xsSpacing);
            }else{
                $('#' + spacingID).css("height", "");
            }}
        $(document).ready(function(){
            smartySpacing();
        });
        $(window).resize(function(){
            smartySpacing();
        });
    })(jQuery);</script>
<script>(function($){
        "use strict";
        var spacingID='stm-spacing-60dad28948253',
            lgSpacing='24',
            mdSpacing='24',
            smSpacing='24',
            xsSpacing='24';
        function smartySpacing(){
            if(window.matchMedia("(min-width: 1200px)").matches&&lgSpacing){
                $('#' + spacingID).css("height", lgSpacing);
            }else if(window.matchMedia("(max-width: 1199px) and (min-width: 992px)").matches&&mdSpacing){
                $('#' + spacingID).css("height", mdSpacing);
            }else if(window.matchMedia("(max-width: 991px) and (min-width: 768px)").matches&&smSpacing){
                $('#' + spacingID).css("height", smSpacing);
            }else if(window.matchMedia("(max-width: 767px)").matches&&xsSpacing){
                $('#' + spacingID).css("height", xsSpacing);
            }else{
                $('#' + spacingID).css("height", "");
            }}
        $(document).ready(function(){
            smartySpacing();
        });
        $(window).resize(function(){
            smartySpacing();
        });
    })(jQuery);</script>
<script>(function($){
        "use strict";
        var spacingID='stm-spacing-60dad2894a583',
            lgSpacing='40',
            mdSpacing='40',
            smSpacing='40',
            xsSpacing='40';
        function smartySpacing(){
            if(window.matchMedia("(min-width: 1200px)").matches&&lgSpacing){
                $('#' + spacingID).css("height", lgSpacing);
            }else if(window.matchMedia("(max-width: 1199px) and (min-width: 992px)").matches&&mdSpacing){
                $('#' + spacingID).css("height", mdSpacing);
            }else if(window.matchMedia("(max-width: 991px) and (min-width: 768px)").matches&&smSpacing){
                $('#' + spacingID).css("height", smSpacing);
            }else if(window.matchMedia("(max-width: 767px)").matches&&xsSpacing){
                $('#' + spacingID).css("height", xsSpacing);
            }else{
                $('#' + spacingID).css("height", "");
            }}
        $(document).ready(function(){
            smartySpacing();
        });
        $(window).resize(function(){
            smartySpacing();
        });
    })(jQuery);</script>
<script>(function($){
        "use strict";
        var spacingID='stm-spacing-60dad2894b25d',
            lgSpacing='40',
            mdSpacing='40',
            smSpacing='40',
            xsSpacing='40';
        function smartySpacing(){
            if(window.matchMedia("(min-width: 1200px)").matches&&lgSpacing){
                $('#' + spacingID).css("height", lgSpacing);
            }else if(window.matchMedia("(max-width: 1199px) and (min-width: 992px)").matches&&mdSpacing){
                $('#' + spacingID).css("height", mdSpacing);
            }else if(window.matchMedia("(max-width: 991px) and (min-width: 768px)").matches&&smSpacing){
                $('#' + spacingID).css("height", smSpacing);
            }else if(window.matchMedia("(max-width: 767px)").matches&&xsSpacing){
                $('#' + spacingID).css("height", xsSpacing);
            }else{
                $('#' + spacingID).css("height", "");
            }}
        $(document).ready(function(){
            smartySpacing();
        });
        $(window).resize(function(){
            smartySpacing();
        });
    })(jQuery);</script>
<script>(function($){
        "use strict";
        var spacingID='stm-spacing-60dad2894c8af',
            lgSpacing='40',
            mdSpacing='40',
            smSpacing='40',
            xsSpacing='40';
        function smartySpacing(){
            if(window.matchMedia("(min-width: 1200px)").matches&&lgSpacing){
                $('#' + spacingID).css("height", lgSpacing);
            }else if(window.matchMedia("(max-width: 1199px) and (min-width: 992px)").matches&&mdSpacing){
                $('#' + spacingID).css("height", mdSpacing);
            }else if(window.matchMedia("(max-width: 991px) and (min-width: 768px)").matches&&smSpacing){
                $('#' + spacingID).css("height", smSpacing);
            }else if(window.matchMedia("(max-width: 767px)").matches&&xsSpacing){
                $('#' + spacingID).css("height", xsSpacing);
            }else{
                $('#' + spacingID).css("height", "");
            }}
        $(document).ready(function(){
            smartySpacing();
        });
        $(window).resize(function(){
            smartySpacing();
        });
    })(jQuery);</script>
<script>(function($){
        "use strict";
        var spacingID='stm-spacing-60dad2894e3fc',
            lgSpacing='40',
            mdSpacing='40',
            smSpacing='40',
            xsSpacing='40';
        function smartySpacing(){
            if(window.matchMedia("(min-width: 1200px)").matches&&lgSpacing){
                $('#' + spacingID).css("height", lgSpacing);
            }else if(window.matchMedia("(max-width: 1199px) and (min-width: 992px)").matches&&mdSpacing){
                $('#' + spacingID).css("height", mdSpacing);
            }else if(window.matchMedia("(max-width: 991px) and (min-width: 768px)").matches&&smSpacing){
                $('#' + spacingID).css("height", smSpacing);
            }else if(window.matchMedia("(max-width: 767px)").matches&&xsSpacing){
                $('#' + spacingID).css("height", xsSpacing);
            }else{
                $('#' + spacingID).css("height", "");
            }}
        $(document).ready(function(){
            smartySpacing();
        });
        $(window).resize(function(){
            smartySpacing();
        });
    })(jQuery);</script>
<script>(function($){
        "use strict";
        var spacingID='stm-spacing-60dad2894f075',
            lgSpacing='24',
            mdSpacing='24',
            smSpacing='24',
            xsSpacing='24';
        function smartySpacing(){
            if(window.matchMedia("(min-width: 1200px)").matches&&lgSpacing){
                $('#' + spacingID).css("height", lgSpacing);
            }else if(window.matchMedia("(max-width: 1199px) and (min-width: 992px)").matches&&mdSpacing){
                $('#' + spacingID).css("height", mdSpacing);
            }else if(window.matchMedia("(max-width: 991px) and (min-width: 768px)").matches&&smSpacing){
                $('#' + spacingID).css("height", smSpacing);
            }else if(window.matchMedia("(max-width: 767px)").matches&&xsSpacing){
                $('#' + spacingID).css("height", xsSpacing);
            }else{
                $('#' + spacingID).css("height", "");
            }}
        $(document).ready(function(){
            smartySpacing();
        });
        $(window).resize(function(){
            smartySpacing();
        });
    })(jQuery);</script>
<script>(function($){
        "use strict";
        var spacingID='stm-spacing-60dad289516c1',
            lgSpacing='50',
            mdSpacing='50',
            smSpacing='30',
            xsSpacing='30';
        function smartySpacing(){
            if(window.matchMedia("(min-width: 1200px)").matches&&lgSpacing){
                $('#' + spacingID).css("height", lgSpacing);
            }else if(window.matchMedia("(max-width: 1199px) and (min-width: 992px)").matches&&mdSpacing){
                $('#' + spacingID).css("height", mdSpacing);
            }else if(window.matchMedia("(max-width: 991px) and (min-width: 768px)").matches&&smSpacing){
                $('#' + spacingID).css("height", smSpacing);
            }else if(window.matchMedia("(max-width: 767px)").matches&&xsSpacing){
                $('#' + spacingID).css("height", xsSpacing);
            }else{
                $('#' + spacingID).css("height", "");
            }}
        $(document).ready(function(){
            smartySpacing();
        });
        $(window).resize(function(){
            smartySpacing();
        });
    })(jQuery);</script>
<script>(function($){
        "use strict";
        var spacingID='stm-spacing-60dad2895e1e1',
            lgSpacing='50',
            mdSpacing='50',
            smSpacing='30',
            xsSpacing='30';
        function smartySpacing(){
            if(window.matchMedia("(min-width: 1200px)").matches&&lgSpacing){
                $('#' + spacingID).css("height", lgSpacing);
            }else if(window.matchMedia("(max-width: 1199px) and (min-width: 992px)").matches&&mdSpacing){
                $('#' + spacingID).css("height", mdSpacing);
            }else if(window.matchMedia("(max-width: 991px) and (min-width: 768px)").matches&&smSpacing){
                $('#' + spacingID).css("height", smSpacing);
            }else if(window.matchMedia("(max-width: 767px)").matches&&xsSpacing){
                $('#' + spacingID).css("height", xsSpacing);
            }else{
                $('#' + spacingID).css("height", "");
            }}
        $(document).ready(function(){
            smartySpacing();
        });
        $(window).resize(function(){
            smartySpacing();
        });
    })(jQuery);</script>
<script>(function($){
        "use strict";
        var spacingID='stm-spacing-60dad2895edf6',
            lgSpacing='50',
            mdSpacing='50',
            smSpacing='30',
            xsSpacing='30';
        function smartySpacing(){
            if(window.matchMedia("(min-width: 1200px)").matches&&lgSpacing){
                $('#' + spacingID).css("height", lgSpacing);
            }else if(window.matchMedia("(max-width: 1199px) and (min-width: 992px)").matches&&mdSpacing){
                $('#' + spacingID).css("height", mdSpacing);
            }else if(window.matchMedia("(max-width: 991px) and (min-width: 768px)").matches&&smSpacing){
                $('#' + spacingID).css("height", smSpacing);
            }else if(window.matchMedia("(max-width: 767px)").matches&&xsSpacing){
                $('#' + spacingID).css("height", xsSpacing);
            }else{
                $('#' + spacingID).css("height", "");
            }}
        $(document).ready(function(){
            smartySpacing();
        });
        $(window).resize(function(){
            smartySpacing();
        });
    })(jQuery);</script>
<script>(function($){
        "use strict";
        var spacingID='stm-spacing-60dad289609eb',
            lgSpacing='50',
            mdSpacing='50',
            smSpacing='30',
            xsSpacing='30';
        function smartySpacing(){
            if(window.matchMedia("(min-width: 1200px)").matches&&lgSpacing){
                $('#' + spacingID).css("height", lgSpacing);
            }else if(window.matchMedia("(max-width: 1199px) and (min-width: 992px)").matches&&mdSpacing){
                $('#' + spacingID).css("height", mdSpacing);
            }else if(window.matchMedia("(max-width: 991px) and (min-width: 768px)").matches&&smSpacing){
                $('#' + spacingID).css("height", smSpacing);
            }else if(window.matchMedia("(max-width: 767px)").matches&&xsSpacing){
                $('#' + spacingID).css("height", xsSpacing);
            }else{
                $('#' + spacingID).css("height", "");
            }}
        $(document).ready(function(){
            smartySpacing();
        });
        $(window).resize(function(){
            smartySpacing();
        });
    })(jQuery);</script>
<script>(function($){
        "use strict";
        var spacingID='stm-spacing-60dad28961d72',
            lgSpacing='60',
            mdSpacing='60',
            smSpacing='40',
            xsSpacing='0';
        function smartySpacing(){
            if(window.matchMedia("(min-width: 1200px)").matches&&lgSpacing){
                $('#' + spacingID).css("height", lgSpacing);
            }else if(window.matchMedia("(max-width: 1199px) and (min-width: 992px)").matches&&mdSpacing){
                $('#' + spacingID).css("height", mdSpacing);
            }else if(window.matchMedia("(max-width: 991px) and (min-width: 768px)").matches&&smSpacing){
                $('#' + spacingID).css("height", smSpacing);
            }else if(window.matchMedia("(max-width: 767px)").matches&&xsSpacing){
                $('#' + spacingID).css("height", xsSpacing);
            }else{
                $('#' + spacingID).css("height", "");
            }}
        $(document).ready(function(){
            smartySpacing();
        });
        $(window).resize(function(){
            smartySpacing();
        });
    })(jQuery);</script>
<script>(function($){
        "use strict";
        var spacingID='stm-spacing-60dad28962544',
            lgSpacing='115',
            mdSpacing='115',
            smSpacing='115',
            xsSpacing='115';
        function smartySpacing(){
            if(window.matchMedia("(min-width: 1200px)").matches&&lgSpacing){
                $('#' + spacingID).css("height", lgSpacing);
            }else if(window.matchMedia("(max-width: 1199px) and (min-width: 992px)").matches&&mdSpacing){
                $('#' + spacingID).css("height", mdSpacing);
            }else if(window.matchMedia("(max-width: 991px) and (min-width: 768px)").matches&&smSpacing){
                $('#' + spacingID).css("height", smSpacing);
            }else if(window.matchMedia("(max-width: 767px)").matches&&xsSpacing){
                $('#' + spacingID).css("height", xsSpacing);
            }else{
                $('#' + spacingID).css("height", "");
            }}
        $(document).ready(function(){
            smartySpacing();
        });
        $(window).resize(function(){
            smartySpacing();
        });
    })(jQuery);</script>
<script>(function($){
        "use strict";
        var spacingID='stm-spacing-60dad28962cf8',
            lgSpacing='115',
            mdSpacing='115',
            smSpacing='115',
            xsSpacing='115';
        function smartySpacing(){
            if(window.matchMedia("(min-width: 1200px)").matches&&lgSpacing){
                $('#' + spacingID).css("height", lgSpacing);
            }else if(window.matchMedia("(max-width: 1199px) and (min-width: 992px)").matches&&mdSpacing){
                $('#' + spacingID).css("height", mdSpacing);
            }else if(window.matchMedia("(max-width: 991px) and (min-width: 768px)").matches&&smSpacing){
                $('#' + spacingID).css("height", smSpacing);
            }else if(window.matchMedia("(max-width: 767px)").matches&&xsSpacing){
                $('#' + spacingID).css("height", xsSpacing);
            }else{
                $('#' + spacingID).css("height", "");
            }}
        $(document).ready(function(){
            smartySpacing();
        });
        $(window).resize(function(){
            smartySpacing();
        });
    })(jQuery);</script>
<script>(function($){
        "use strict";
        var spacingID='stm-spacing-60dad2896409b',
            lgSpacing='20',
            mdSpacing='20',
            smSpacing='20',
            xsSpacing='20';
        function smartySpacing(){
            if(window.matchMedia("(min-width: 1200px)").matches&&lgSpacing){
                $('#' + spacingID).css("height", lgSpacing);
            }else if(window.matchMedia("(max-width: 1199px) and (min-width: 992px)").matches&&mdSpacing){
                $('#' + spacingID).css("height", mdSpacing);
            }else if(window.matchMedia("(max-width: 991px) and (min-width: 768px)").matches&&smSpacing){
                $('#' + spacingID).css("height", smSpacing);
            }else if(window.matchMedia("(max-width: 767px)").matches&&xsSpacing){
                $('#' + spacingID).css("height", xsSpacing);
            }else{
                $('#' + spacingID).css("height", "");
            }}
        $(document).ready(function(){
            smartySpacing();
        });
        $(window).resize(function(){
            smartySpacing();
        });
    })(jQuery);</script>
<script>(function($){
        "use strict";
        var spacingID='stm-spacing-60dad2896a75a',
            lgSpacing='140',
            mdSpacing='140',
            smSpacing='100',
            xsSpacing='140';
        function smartySpacing(){
            if(window.matchMedia("(min-width: 1200px)").matches&&lgSpacing){
                $('#' + spacingID).css("height", lgSpacing);
            }else if(window.matchMedia("(max-width: 1199px) and (min-width: 992px)").matches&&mdSpacing){
                $('#' + spacingID).css("height", mdSpacing);
            }else if(window.matchMedia("(max-width: 991px) and (min-width: 768px)").matches&&smSpacing){
                $('#' + spacingID).css("height", smSpacing);
            }else if(window.matchMedia("(max-width: 767px)").matches&&xsSpacing){
                $('#' + spacingID).css("height", xsSpacing);
            }else{
                $('#' + spacingID).css("height", "");
            }}
        $(document).ready(function(){
            smartySpacing();
        });
        $(window).resize(function(){
            smartySpacing();
        });
    })(jQuery);</script>
<script>(function($){
        "use strict";
        var spacingID='stm-spacing-60dad2896bda4',
            lgSpacing='115',
            mdSpacing='115',
            smSpacing='115',
            xsSpacing='115';
        function smartySpacing(){
            if(window.matchMedia("(min-width: 1200px)").matches&&lgSpacing){
                $('#' + spacingID).css("height", lgSpacing);
            }else if(window.matchMedia("(max-width: 1199px) and (min-width: 992px)").matches&&mdSpacing){
                $('#' + spacingID).css("height", mdSpacing);
            }else if(window.matchMedia("(max-width: 991px) and (min-width: 768px)").matches&&smSpacing){
                $('#' + spacingID).css("height", smSpacing);
            }else if(window.matchMedia("(max-width: 767px)").matches&&xsSpacing){
                $('#' + spacingID).css("height", xsSpacing);
            }else{
                $('#' + spacingID).css("height", "");
            }}
        $(document).ready(function(){
            smartySpacing();
        });
        $(window).resize(function(){
            smartySpacing();
        });
    })(jQuery);</script>
<script>(function($){
        "use strict";
        var spacingID='stm-spacing-60dad2896c504',
            lgSpacing='30',
            mdSpacing='30',
            smSpacing='0',
            xsSpacing='0';
        function smartySpacing(){
            if(window.matchMedia("(min-width: 1200px)").matches&&lgSpacing){
                $('#' + spacingID).css("height", lgSpacing);
            }else if(window.matchMedia("(max-width: 1199px) and (min-width: 992px)").matches&&mdSpacing){
                $('#' + spacingID).css("height", mdSpacing);
            }else if(window.matchMedia("(max-width: 991px) and (min-width: 768px)").matches&&smSpacing){
                $('#' + spacingID).css("height", smSpacing);
            }else if(window.matchMedia("(max-width: 767px)").matches&&xsSpacing){
                $('#' + spacingID).css("height", xsSpacing);
            }else{
                $('#' + spacingID).css("height", "");
            }}
        $(document).ready(function(){
            smartySpacing();
        });
        $(window).resize(function(){
            smartySpacing();
        });
    })(jQuery);</script>
<script>(function($){
        "use strict";
        var spacingID='stm-spacing-60dad2896c844',
            lgSpacing='20',
            mdSpacing='20',
            smSpacing='20',
            xsSpacing='20';
        function smartySpacing(){
            if(window.matchMedia("(min-width: 1200px)").matches&&lgSpacing){
                $('#' + spacingID).css("height", lgSpacing);
            }else if(window.matchMedia("(max-width: 1199px) and (min-width: 992px)").matches&&mdSpacing){
                $('#' + spacingID).css("height", mdSpacing);
            }else if(window.matchMedia("(max-width: 991px) and (min-width: 768px)").matches&&smSpacing){
                $('#' + spacingID).css("height", smSpacing);
            }else if(window.matchMedia("(max-width: 767px)").matches&&xsSpacing){
                $('#' + spacingID).css("height", xsSpacing);
            }else{
                $('#' + spacingID).css("height", "");
            }}
        $(document).ready(function(){
            smartySpacing();
        });
        $(window).resize(function(){
            smartySpacing();
        });
    })(jQuery);</script>
<script>(function($){
        "use strict";
        $(document).ready(function(){
            var carouselId='stm-posts_carousel-60dad2896ca78';
            $('#'+carouselId).owlCarousel({
                loop:true,
                margin:30,
                dots: true,
                nav: false,
                lazyLoad:true,
                responsive:{
                    0:{
                        items:1
                    },
                    640:{
                        items:2
                    },
                    992:{
                        items:3
                    }}
            });
        });
    })(jQuery);</script>
<script>(function($){
        "use strict";
        var spacingID='stm-spacing-60dad28975cce',
            lgSpacing='100',
            mdSpacing='100',
            smSpacing='100',
            xsSpacing='100';
        function smartySpacing(){
            if(window.matchMedia("(min-width: 1200px)").matches&&lgSpacing){
                $('#' + spacingID).css("height", lgSpacing);
            }else if(window.matchMedia("(max-width: 1199px) and (min-width: 992px)").matches&&mdSpacing){
                $('#' + spacingID).css("height", mdSpacing);
            }else if(window.matchMedia("(max-width: 991px) and (min-width: 768px)").matches&&smSpacing){
                $('#' + spacingID).css("height", smSpacing);
            }else if(window.matchMedia("(max-width: 767px)").matches&&xsSpacing){
                $('#' + spacingID).css("height", xsSpacing);
            }else{
                $('#' + spacingID).css("height", "");
            }}
        $(document).ready(function(){
            smartySpacing();
        });
        $(window).resize(function(){
            smartySpacing();
        });
    })(jQuery);</script>
<script>(function($){
        "use strict";
        var spacingID='stm-spacing-60dad289787ca',
            lgSpacing='0',
            mdSpacing='0',
            smSpacing='80',
            xsSpacing='70';
        function smartySpacing(){
            if(window.matchMedia("(min-width: 1200px)").matches&&lgSpacing){
                $('#' + spacingID).css("height", lgSpacing);
            }else if(window.matchMedia("(max-width: 1199px) and (min-width: 992px)").matches&&mdSpacing){
                $('#' + spacingID).css("height", mdSpacing);
            }else if(window.matchMedia("(max-width: 991px) and (min-width: 768px)").matches&&smSpacing){
                $('#' + spacingID).css("height", smSpacing);
            }else if(window.matchMedia("(max-width: 767px)").matches&&xsSpacing){
                $('#' + spacingID).css("height", xsSpacing);
            }else{
                $('#' + spacingID).css("height", "");
            }}
        $(document).ready(function(){
            smartySpacing();
        });
        $(window).resize(function(){
            smartySpacing();
        });
    })(jQuery);</script>
<script>(function($){
        "use strict";
        var spacingID='stm-spacing-60dad2897a700',
            lgSpacing='30',
            mdSpacing='30',
            smSpacing='30',
            xsSpacing='30';
        function smartySpacing(){
            if(window.matchMedia("(min-width: 1200px)").matches&&lgSpacing){
                $('#' + spacingID).css("height", lgSpacing);
            }else if(window.matchMedia("(max-width: 1199px) and (min-width: 992px)").matches&&mdSpacing){
                $('#' + spacingID).css("height", mdSpacing);
            }else if(window.matchMedia("(max-width: 991px) and (min-width: 768px)").matches&&smSpacing){
                $('#' + spacingID).css("height", smSpacing);
            }else if(window.matchMedia("(max-width: 767px)").matches&&xsSpacing){
                $('#' + spacingID).css("height", xsSpacing);
            }else{
                $('#' + spacingID).css("height", "");
            }}
        $(document).ready(function(){
            smartySpacing();
        });
        $(window).resize(function(){
            smartySpacing();
        });
    })(jQuery);</script>
<script>(function($){
        "use strict";
        var spacingID='stm-spacing-60dad2897af57',
            lgSpacing='30',
            mdSpacing='30',
            smSpacing='30',
            xsSpacing='30';
        function smartySpacing(){
            if(window.matchMedia("(min-width: 1200px)").matches&&lgSpacing){
                $('#' + spacingID).css("height", lgSpacing);
            }else if(window.matchMedia("(max-width: 1199px) and (min-width: 992px)").matches&&mdSpacing){
                $('#' + spacingID).css("height", mdSpacing);
            }else if(window.matchMedia("(max-width: 991px) and (min-width: 768px)").matches&&smSpacing){
                $('#' + spacingID).css("height", smSpacing);
            }else if(window.matchMedia("(max-width: 767px)").matches&&xsSpacing){
                $('#' + spacingID).css("height", xsSpacing);
            }else{
                $('#' + spacingID).css("height", "");
            }}
        $(document).ready(function(){
            smartySpacing();
        });
        $(window).resize(function(){
            smartySpacing();
        });
    })(jQuery);</script>
<script>(function($){
        "use strict";
        var spacingID='stm-spacing-60dad2897ce18',
            lgSpacing='0',
            mdSpacing='0',
            smSpacing='30',
            xsSpacing='30';
        function smartySpacing(){
            if(window.matchMedia("(min-width: 1200px)").matches&&lgSpacing){
                $('#' + spacingID).css("height", lgSpacing);
            }else if(window.matchMedia("(max-width: 1199px) and (min-width: 992px)").matches&&mdSpacing){
                $('#' + spacingID).css("height", mdSpacing);
            }else if(window.matchMedia("(max-width: 991px) and (min-width: 768px)").matches&&smSpacing){
                $('#' + spacingID).css("height", smSpacing);
            }else if(window.matchMedia("(max-width: 767px)").matches&&xsSpacing){
                $('#' + spacingID).css("height", xsSpacing);
            }else{
                $('#' + spacingID).css("height", "");
            }}
        $(document).ready(function(){
            smartySpacing();
        });
        $(window).resize(function(){
            smartySpacing();
        });
    })(jQuery);</script>
<script>(function($){
        "use strict";
        var spacingID='stm-spacing-60dad2897d79f',
            lgSpacing='0',
            mdSpacing='0',
            smSpacing='30',
            xsSpacing='30';
        function smartySpacing(){
            if(window.matchMedia("(min-width: 1200px)").matches&&lgSpacing){
                $('#' + spacingID).css("height", lgSpacing);
            }else if(window.matchMedia("(max-width: 1199px) and (min-width: 992px)").matches&&mdSpacing){
                $('#' + spacingID).css("height", mdSpacing);
            }else if(window.matchMedia("(max-width: 991px) and (min-width: 768px)").matches&&smSpacing){
                $('#' + spacingID).css("height", smSpacing);
            }else if(window.matchMedia("(max-width: 767px)").matches&&xsSpacing){
                $('#' + spacingID).css("height", xsSpacing);
            }else{
                $('#' + spacingID).css("height", "");
            }}
        $(document).ready(function(){
            smartySpacing();
        });
        $(window).resize(function(){
            smartySpacing();
        });
    })(jQuery);</script>
<script>(function($){
        "use strict";
        var spacingID='stm-spacing-60dad2897ed58',
            lgSpacing='100',
            mdSpacing='100',
            smSpacing='80',
            xsSpacing='60';
        function smartySpacing(){
            if(window.matchMedia("(min-width: 1200px)").matches&&lgSpacing){
                $('#' + spacingID).css("height", lgSpacing);
            }else if(window.matchMedia("(max-width: 1199px) and (min-width: 992px)").matches&&mdSpacing){
                $('#' + spacingID).css("height", mdSpacing);
            }else if(window.matchMedia("(max-width: 991px) and (min-width: 768px)").matches&&smSpacing){
                $('#' + spacingID).css("height", smSpacing);
            }else if(window.matchMedia("(max-width: 767px)").matches&&xsSpacing){
                $('#' + spacingID).css("height", xsSpacing);
            }else{
                $('#' + spacingID).css("height", "");
            }}
        $(document).ready(function(){
            smartySpacing();
        });
        $(window).resize(function(){
            smartySpacing();
        });
    })(jQuery);</script>
<script>(function($){
        "use strict";
        var spacingID='stm-spacing-60dad2897f66c',
            lgSpacing='115',
            mdSpacing='115',
            smSpacing='115',
            xsSpacing='115';
        function smartySpacing(){
            if(window.matchMedia("(min-width: 1200px)").matches&&lgSpacing){
                $('#' + spacingID).css("height", lgSpacing);
            }else if(window.matchMedia("(max-width: 1199px) and (min-width: 992px)").matches&&mdSpacing){
                $('#' + spacingID).css("height", mdSpacing);
            }else if(window.matchMedia("(max-width: 991px) and (min-width: 768px)").matches&&smSpacing){
                $('#' + spacingID).css("height", smSpacing);
            }else if(window.matchMedia("(max-width: 767px)").matches&&xsSpacing){
                $('#' + spacingID).css("height", xsSpacing);
            }else{
                $('#' + spacingID).css("height", "");
            }}
        $(document).ready(function(){
            smartySpacing();
        });
        $(window).resize(function(){
            smartySpacing();
        });
    })(jQuery);</script>
<script>(function($){
        "use strict";
        var spacingID='stm-spacing-60dad28980c92',
            lgSpacing='115',
            mdSpacing='115',
            smSpacing='115',
            xsSpacing='135';
        function smartySpacing(){
            if(window.matchMedia("(min-width: 1200px)").matches&&lgSpacing){
                $('#' + spacingID).css("height", lgSpacing);
            }else if(window.matchMedia("(max-width: 1199px) and (min-width: 992px)").matches&&mdSpacing){
                $('#' + spacingID).css("height", mdSpacing);
            }else if(window.matchMedia("(max-width: 991px) and (min-width: 768px)").matches&&smSpacing){
                $('#' + spacingID).css("height", smSpacing);
            }else if(window.matchMedia("(max-width: 767px)").matches&&xsSpacing){
                $('#' + spacingID).css("height", xsSpacing);
            }else{
                $('#' + spacingID).css("height", "");
            }}
        $(document).ready(function(){
            smartySpacing();
        });
        $(window).resize(function(){
            smartySpacing();
        });
    })(jQuery);</script>
<script>(function($){
        "use strict";
        var spacingID='stm-spacing-60dad28980fc3',
            lgSpacing='20',
            mdSpacing='20',
            smSpacing='20',
            xsSpacing='20';
        function smartySpacing(){
            if(window.matchMedia("(min-width: 1200px)").matches&&lgSpacing){
                $('#' + spacingID).css("height", lgSpacing);
            }else if(window.matchMedia("(max-width: 1199px) and (min-width: 992px)").matches&&mdSpacing){
                $('#' + spacingID).css("height", mdSpacing);
            }else if(window.matchMedia("(max-width: 991px) and (min-width: 768px)").matches&&smSpacing){
                $('#' + spacingID).css("height", smSpacing);
            }else if(window.matchMedia("(max-width: 767px)").matches&&xsSpacing){
                $('#' + spacingID).css("height", xsSpacing);
            }else{
                $('#' + spacingID).css("height", "");
            }}
        $(document).ready(function(){
            smartySpacing();
        });
        $(window).resize(function(){
            smartySpacing();
        });
    })(jQuery);</script>
<script>(function($){
        "use strict";
        var $container;
        $(document).ready(function(){
            if($(".stm-posts__items-row_view_masonry_box").length){
                $container=$('.stm-posts__items-row_view_masonry_box').imagesLoaded(function(){
                    $container.isotope({
                        itemSelector: '.stm-posts__items-row_view_masonry',
                        layoutMode: 'masonry',
                        masonry: {
                            columnWidth: '.stm-posts__items-row_view_masonry'
                        }});
                });
            }});
    })(jQuery);</script>
<script>(function($){
        "use strict";
        var spacingID='stm-spacing-60dad2898b0a3',
            lgSpacing='100',
            mdSpacing='100',
            smSpacing='80',
            xsSpacing='115';
        function smartySpacing(){
            if(window.matchMedia("(min-width: 1200px)").matches&&lgSpacing){
                $('#' + spacingID).css("height", lgSpacing);
            }else if(window.matchMedia("(max-width: 1199px) and (min-width: 992px)").matches&&mdSpacing){
                $('#' + spacingID).css("height", mdSpacing);
            }else if(window.matchMedia("(max-width: 991px) and (min-width: 768px)").matches&&smSpacing){
                $('#' + spacingID).css("height", smSpacing);
            }else if(window.matchMedia("(max-width: 767px)").matches&&xsSpacing){
                $('#' + spacingID).css("height", xsSpacing);
            }else{
                $('#' + spacingID).css("height", "");
            }}
        $(document).ready(function(){
            smartySpacing();
        });
        $(window).resize(function(){
            smartySpacing();
        });
    })(jQuery);</script>
<script>(function($){
        "use strict";
        var spacingID='stm-spacing-60dad2898bfc7',
            lgSpacing='115',
            mdSpacing='115',
            smSpacing='115',
            xsSpacing='115';
        function smartySpacing(){
            if(window.matchMedia("(min-width: 1200px)").matches&&lgSpacing){
                $('#' + spacingID).css("height", lgSpacing);
            }else if(window.matchMedia("(max-width: 1199px) and (min-width: 992px)").matches&&mdSpacing){
                $('#' + spacingID).css("height", mdSpacing);
            }else if(window.matchMedia("(max-width: 991px) and (min-width: 768px)").matches&&smSpacing){
                $('#' + spacingID).css("height", smSpacing);
            }else if(window.matchMedia("(max-width: 767px)").matches&&xsSpacing){
                $('#' + spacingID).css("height", xsSpacing);
            }else{
                $('#' + spacingID).css("height", "");
            }}
        $(document).ready(function(){
            smartySpacing();
        });
        $(window).resize(function(){
            smartySpacing();
        });
    })(jQuery);</script>
<script>(function($){
        "use strict";
        var spacingID='stm-spacing-60dad2898db9c',
            lgSpacing='50',
            mdSpacing='50',
            smSpacing='50',
            xsSpacing='50';
        function smartySpacing(){
            if(window.matchMedia("(min-width: 1200px)").matches&&lgSpacing){
                $('#' + spacingID).css("height", lgSpacing);
            }else if(window.matchMedia("(max-width: 1199px) and (min-width: 992px)").matches&&mdSpacing){
                $('#' + spacingID).css("height", mdSpacing);
            }else if(window.matchMedia("(max-width: 991px) and (min-width: 768px)").matches&&smSpacing){
                $('#' + spacingID).css("height", smSpacing);
            }else if(window.matchMedia("(max-width: 767px)").matches&&xsSpacing){
                $('#' + spacingID).css("height", xsSpacing);
            }else{
                $('#' + spacingID).css("height", "");
            }}
        $(document).ready(function(){
            smartySpacing();
        });
        $(window).resize(function(){
            smartySpacing();
        });
    })(jQuery);</script>
<script>(function($){
        "use strict";
        var spacingID='stm-spacing-60dad2898df82',
            lgSpacing='30',
            mdSpacing='30',
            smSpacing='30',
            xsSpacing='30';
        function smartySpacing(){
            if(window.matchMedia("(min-width: 1200px)").matches&&lgSpacing){
                $('#' + spacingID).css("height", lgSpacing);
            }else if(window.matchMedia("(max-width: 1199px) and (min-width: 992px)").matches&&mdSpacing){
                $('#' + spacingID).css("height", mdSpacing);
            }else if(window.matchMedia("(max-width: 991px) and (min-width: 768px)").matches&&smSpacing){
                $('#' + spacingID).css("height", smSpacing);
            }else if(window.matchMedia("(max-width: 767px)").matches&&xsSpacing){
                $('#' + spacingID).css("height", xsSpacing);
            }else{
                $('#' + spacingID).css("height", "");
            }}
        $(document).ready(function(){
            smartySpacing();
        });
        $(window).resize(function(){
            smartySpacing();
        });
    })(jQuery);</script>
<script>(function($){
        "use strict";
        var carouselAutoPlay=true,
            carouselLoop=true,
            carouselOptions={
                lazyLoad:true,
                dots: true,
                nav: false,
                smartSpeed: 700,
                responsive: {
                    0: {
                        items: 1
                    },
                    767: {
                        items: 2                        },
                },
                navText:["<span class='stm-icon stm-icon-arrow-l'></span>","<span class='stm-icon stm-icon-arrow-r'></span>"]
            };
        if(carouselAutoPlay){
            carouselOptions.autoplay=carouselAutoPlay;
        }
        if(carouselLoop){
            carouselOptions.loop=carouselLoop;
        }
        $(document).ready(function(){
            $('.stm-carousel_type_testimonials')
                .owlCarousel(carouselOptions);
        });
    })(jQuery);</script>
<script>(function($){
        "use strict";
        var spacingID='stm-spacing-60dad28992da8',
            lgSpacing='40',
            mdSpacing='40',
            smSpacing='40',
            xsSpacing='40';
        function smartySpacing(){
            if(window.matchMedia("(min-width: 1200px)").matches&&lgSpacing){
                $('#' + spacingID).css("height", lgSpacing);
            }else if(window.matchMedia("(max-width: 1199px) and (min-width: 992px)").matches&&mdSpacing){
                $('#' + spacingID).css("height", mdSpacing);
            }else if(window.matchMedia("(max-width: 991px) and (min-width: 768px)").matches&&smSpacing){
                $('#' + spacingID).css("height", smSpacing);
            }else if(window.matchMedia("(max-width: 767px)").matches&&xsSpacing){
                $('#' + spacingID).css("height", xsSpacing);
            }else{
                $('#' + spacingID).css("height", "");
            }}
        $(document).ready(function(){
            smartySpacing();
        });
        $(window).resize(function(){
            smartySpacing();
        });
    })(jQuery);</script>
<script>(function($){
        "use strict";
        var spacingID='stm-spacing-60dad28994ce2',
            lgSpacing='20',
            mdSpacing='20',
            smSpacing='20',
            xsSpacing='20';
        function smartySpacing(){
            if(window.matchMedia("(min-width: 1200px)").matches&&lgSpacing){
                $('#' + spacingID).css("height", lgSpacing);
            }else if(window.matchMedia("(max-width: 1199px) and (min-width: 992px)").matches&&mdSpacing){
                $('#' + spacingID).css("height", mdSpacing);
            }else if(window.matchMedia("(max-width: 991px) and (min-width: 768px)").matches&&smSpacing){
                $('#' + spacingID).css("height", smSpacing);
            }else if(window.matchMedia("(max-width: 767px)").matches&&xsSpacing){
                $('#' + spacingID).css("height", xsSpacing);
            }else{
                $('#' + spacingID).css("height", "");
            }}
        $(document).ready(function(){
            smartySpacing();
        });
        $(window).resize(function(){
            smartySpacing();
        });
    })(jQuery);</script>
<script>(function($){
        "use strict";
        var spacingID='stm-spacing-60dad2899530f',
            lgSpacing='26',
            mdSpacing='26',
            smSpacing='26',
            xsSpacing='26';
        function smartySpacing(){
            if(window.matchMedia("(min-width: 1200px)").matches&&lgSpacing){
                $('#' + spacingID).css("height", lgSpacing);
            }else if(window.matchMedia("(max-width: 1199px) and (min-width: 992px)").matches&&mdSpacing){
                $('#' + spacingID).css("height", mdSpacing);
            }else if(window.matchMedia("(max-width: 991px) and (min-width: 768px)").matches&&smSpacing){
                $('#' + spacingID).css("height", smSpacing);
            }else if(window.matchMedia("(max-width: 767px)").matches&&xsSpacing){
                $('#' + spacingID).css("height", xsSpacing);
            }else{
                $('#' + spacingID).css("height", "");
            }}
        $(document).ready(function(){
            smartySpacing();
        });
        $(window).resize(function(){
            smartySpacing();
        });
    })(jQuery);</script>
<script>(function(){
        window.mc4wp=window.mc4wp||{
            listeners: [],
            forms: {
                on: function(evt, cb){
                    window.mc4wp.listeners.push({
                            event:evt,
                            callback: cb
                        }
                    );
                }}
        }})();</script>
<script>(function($){
        "use strict";
        var spacingID='stm-spacing-60dad2899700e',
            lgSpacing='20',
            mdSpacing='20',
            smSpacing='20',
            xsSpacing='20';
        function smartySpacing(){
            if(window.matchMedia("(min-width: 1200px)").matches&&lgSpacing){
                $('#' + spacingID).css("height", lgSpacing);
            }else if(window.matchMedia("(max-width: 1199px) and (min-width: 992px)").matches&&mdSpacing){
                $('#' + spacingID).css("height", mdSpacing);
            }else if(window.matchMedia("(max-width: 991px) and (min-width: 768px)").matches&&smSpacing){
                $('#' + spacingID).css("height", smSpacing);
            }else if(window.matchMedia("(max-width: 767px)").matches&&xsSpacing){
                $('#' + spacingID).css("height", xsSpacing);
            }else{
                $('#' + spacingID).css("height", "");
            }}
        $(document).ready(function(){
            smartySpacing();
        });
        $(window).resize(function(){
            smartySpacing();
        });
    })(jQuery);</script>
<script>(function($){
        "use strict";
        var spacingID='stm-spacing-60dad28998f55',
            lgSpacing='20',
            mdSpacing='20',
            smSpacing='20',
            xsSpacing='20';
        function smartySpacing(){
            if(window.matchMedia("(min-width: 1200px)").matches&&lgSpacing){
                $('#' + spacingID).css("height", lgSpacing);
            }else if(window.matchMedia("(max-width: 1199px) and (min-width: 992px)").matches&&mdSpacing){
                $('#' + spacingID).css("height", mdSpacing);
            }else if(window.matchMedia("(max-width: 991px) and (min-width: 768px)").matches&&smSpacing){
                $('#' + spacingID).css("height", smSpacing);
            }else if(window.matchMedia("(max-width: 767px)").matches&&xsSpacing){
                $('#' + spacingID).css("height", xsSpacing);
            }else{
                $('#' + spacingID).css("height", "");
            }}
        $(document).ready(function(){
            smartySpacing();
        });
        $(window).resize(function(){
            smartySpacing();
        });
    })(jQuery);</script>
<script>(function(){function maybePrefixUrlField(){
        if(this.value.trim()!==''&&this.value.indexOf('http')!==0){
            this.value="http://" + this.value;
        }}
        var urlFields=document.querySelectorAll('.mc4wp-form input[type="url"]');
        if(urlFields){
            for (var j=0; j < urlFields.length; j++){
                urlFields[j].addEventListener('blur', maybePrefixUrlField);
            }}
    })();</script>
<script type="text/html" id="wpb-modifications"></script>
<script data-noptimize="1">window.lazySizesConfig=window.lazySizesConfig||{};window.lazySizesConfig.loadMode=1;</script>
<script async data-noptimize="1" src='/static/js/lazysizes.min.js?ao_version=2.8.1'></script>
<script>if(typeof revslider_showDoubleJqueryError==="undefined"){
        function revslider_showDoubleJqueryError(sliderID){
            var err="<div class='rs_error_message_box'>"; err +="<div class='rs_error_message_oops'>Oops...</div>";
            err +="<div class='rs_error_message_content'>"; err +="You have some jquery.js library include that comes after the Slider Revolution files js inclusion.<br>"; err +="To fix this, you can:<br>&nbsp;&nbsp;&nbsp; 1. Set 'Module General Options' -> 'Advanced' -> 'jQuery & OutPut Filters' -> 'Put JS to Body' to on"; err +="<br>&nbsp;&nbsp;&nbsp; 2. Find the double jQuery.js inclusion and remove it"; err +="</div>";
            err +="</div>";
            var slider=document.getElementById(sliderID); slider.innerHTML=err; slider.style.display="block";
        }}</script>
<script defer type='text/javascript' src='/static/js/autoptimize_single_f89263c0c2f24398a1df52eead69f5f8.js?ver=5.3.2' id='contact-form-7-js'></script>
<script defer type='text/javascript' src='/static/js/core.min.js?ver=1.12.1' id='jquery-ui-core-js'></script>
<!--<script defer type='text/javascript' src='/static/js/tabs.min.js?ver=1.12.1' id='jquery-ui-tabs-js'></script>-->
<!--<script defer type='text/javascript' src='/static/js/jquery.ba-bbq.min.js?ver=5.6.1' id='jquery-ba-bqq-js'></script>-->
<!--<script defer type='text/javascript' src='/static/js/autoptimize_single_bfe456dc33fd691cb0b1a8e769a7bb76.js?ver=5.6.1' id='jquery-carouFredSel-js'></script> -->
<!--<script defer type='text/javascript' src='/static/js/autoptimize_single_9c7154d8e447fba64866bd179fdd670d.js?ver=5.6.1' id='timetable_main-js'></script>-->
<!--<script defer type='text/javascript' src='/static/js/js.cookie.min.js?ver=2.1.4' id='js-cookie-js'></script>-->
<!--<script defer type='text/javascript' src='/static/js/woocommerce.min.js?ver=4.9.0' id='woocommerce-js'></script>-->
<!--<script defer type='text/javascript' src='/static/js/cart-fragments.min.js?ver=4.9.0' id='wc-cart-fragments-js'></script>-->


<script defer type='text/javascript' src='/static/js/bootstrap.min.js?ver=3.2' id='bootstrap-js'></script>
<!--<script defer type='text/javascript' src='/static/js/select2.min.js?ver=3.2' id='stm-select2-js'></script>-->
<script defer type='text/javascript' src='/static/js/autoptimize_single_cff3d9f2bf5722ea915aa4dd5d90d4e4.js?ver=3.2' id='stm-custom-js'></script>
<script defer type='text/javascript' src='/static/js/autoptimize_single_49a6b4d019a934bcf83f0c397eba82d8.js?ver=3.2' id='fancybox-js'></script>
<!--<script defer type='text/javascript' src='/static/js/wp-embed.min.js?ver=5.6.1' id='wp-embed-js'></script>-->
<script defer type='text/javascript' src='/static/js/js_composer_front.min.js?ver=6.5.0' id='wpb_composer_front_js-js'></script>
<script defer type='text/javascript' src='/static/js/owl.carousel.min.js?ver=3.2' id='owl-carousel-js'></script>


<!--<script defer type='text/javascript' src='/static/js/isotope.pkgd.min.js?ver=3.2' id='isotope-js'></script>-->
<!--<script defer type='text/javascript' src='/static/js/imagesloaded.min.js?ver=4.1.4' id='imagesloaded-js'></script>-->
<!--<script defer type='text/javascript' src='/static/js/forms.min.js?ver=4.8.2' id='mc4wp-forms-api-js'></script>-->


<link href="https://fonts.googleapis.com/css2?family=Nunito:wght@300&display=swap" rel="stylesheet">

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>