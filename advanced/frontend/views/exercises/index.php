<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;


$this->title = 'Учить слова';

/* @var $this yii\web\View */
?>



    <div class="vc_row wpb_row vc_inner vc_row-fluid">

        <div class="">
            <div class="vc_column-inner">
                <div class="wpb_wrapper">
                    <div class="vc_progress_bar wpb_content_element vc_progress-bar-color-white vc_progress-bar_view_large">
                        <small class="vc_label">Прогресс</small>
                        <div class="vc_general vc_single_bar vc_progress-bar-color-bar_blue">
                            <span class="vc_bar"
                                                                                                   data-percentage-value="87"
                                                                                                   data-value="87"
                                                                                                   style="width: 87%; min-width: 10px;"><span
                                        class="vc_bar_val">87%</span></span></div>

                    </div>
                </div>
            </div>
        </div>



    </div>



<!--<div class="iprogress">-->
<!--    <div class="vc_column-inner">-->
<!--        <div class="wpb_wrapper">-->
<!--            <div class="vc_progress_bar wpb_content_element vc_progress-bar-color-white vc_progress-bar_view_large">-->
<!--                <small class="vc_label">Прогресс</small>-->
<!--                <div class="iprogress__bar">-->
<!--                    <div style="min-width: 4%;" class="iprogress__bar-inner"></div>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--</div>-->

<div class="itheory__wrap" style="display: none">
    <div class="itheory">
        <div class="itheory__text">
            <ul style='font-family: "Quicksand", sans-serif;'>
                <li>АНГЛИЙСКИЙ</li>
                <li class="itheory__wrap-listen">
                    <p class="itheory__li-p">test</p>
                    <img src="/static/img/gromko.png" class="itheory-audio" alt="listen_photo">
                </li>
                <li class="itheory-transcription-title">ТРАНСКРИПЦИЯ</li>
                <li class="itheory-transcription">[test]</li>
                <li>РУССКИЙ</li>
                <li class="itheory-translate"></li>
            </ul>
        </div>
        <div class="itheory__video">
            <video controlsList="nodownload" src="/uploads/audio/628.mp4" class="itheory__video-item" controls></video>
        </div>
    </div>
    <div class="itheory__btns">
        <button class="itheory__btns-prev">Назад</button>
        <button class="itheory__btns-next right-btn">Вперед</button>

        <button class="itheory__btns-mix">Перемешать список и начать сначала</button>
        <button class="itheory__btns-start">В начало</button>
        <br><br>
        <button class="itheory__btns-delete">Удалить слово из списка</button>
    </div>
</div>

<div class="hourglass__wrap">
    <div class="hourglass">
        <div class="top"></div>
        <div class="bottom"></div>
    </div>
</div>

<div class="iengToRus" style="display: none">
    <h2 class="iengToRus-question"></h2>
    <p class="iengToRus-word">Виберите правильный вариант ответа</p>
    <div class="iengToRus__wrap"></div>
    <button class="iengToRus-showAnswer">Посмотреть ответ</button>
</div>

<div class="irusToEng" style="display: none">
    <h2 class="irusToEng-question">Возможность</h2>
    <p class="irusToEng-word">Виберите правильный вариант ответа</p>
    <div class="irusToEng__wrap"></div>
    <button class="irusToEng-showAnswer">Посмотреть ответ</button>
</div>

<div class="iaudio" style="display: none">
    <div class="iaudio__question">
        <img src="/static/img/gromko.png" class="iaudio-listen" alt="listen_photo">
        Listen
    </div>
    <p class="iaudio-word">Виберите правильный вариант ответа</p>
    <div class="iaudio__wrap"></div>
    <button class="iaudio-showAnswer">Посмотреть ответ</button>
</div>

<div class="iwrite" style="display: none">
    <h2 class="iwrite-question">Возможность</h2>
    <p>Вы можете выбирать буквы мышью или набирать с клавиатуры</p>
    <div class="iwrite__wrap"></div>
    <div class="iwrite__btns"></div>
    <div class="iwrite__answerBtns">
        <button class="iwrite-showAnswer">Посмотреть ответ</button>
    </div>
</div>

<table class="itable donation-info"></table>

<div class="idone">
    <h2>Спасибо вы прошли весь список</h2>
    <button class="idone-start">Начать сначала</button>
</div>

<div>
    <div class="settings_wrap">
        <div>
            <img src="/static/img/settings_icon.jpg" alt="settings_icon">
            <h3>Настройки</h3>
        </div>
    </div>

    <div class="settings__content">
        <? $form = ActiveForm::begin([
            'id' => 'options-form',
            //    'options' => ['class' => 'form-horizontal'],
        ]) ?>
        <div>
            <p>Упражнения</p>
            <?= $form->field($model, 'type_exercises_1')->checkbox() ?>
            <?= $form->field($model, 'type_exercises_2')->checkbox() ?>
            <?= $form->field($model, 'type_exercises_3')->checkbox() ?>
            <?= $form->field($model, 'type_exercises_4')->checkbox() ?>
            <?= $form->field($model, 'type_exercises_5')->checkbox() ?>


        </div>
        <!--        --><? //= $form->field($model, 'autoPlayWords')->checkbox() ?>
        <!--        --><? //= $form->field($model, 'pauseBetweenWords')->textInput() ?>

        <!--        --><? //= $form->field($model, 'autoPlayVideo')->checkbox() ?>
        <?= $form->field($model, 'autoPlayAudio')->checkbox() ?>

        <?= $form->field($model, 'portionsOfWords')->textInput() ?>


        <div class="form-group">
            <div class="">
                <?= Html::submitButton('Сохранить', ['class' => 'icustome-btn']) ?>
            </div>
        </div>
        <?php ActiveForm::end() ?>
    </div>

</div>

<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>


<!-- <script src='/static/js/tests.js'></script> -->