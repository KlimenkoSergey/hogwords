<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\PlayList */

$this->title = 'Update Play List: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Play Lists', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="play-list-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
