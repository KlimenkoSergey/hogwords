<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\PlayList */

$this->title = 'Create Play List';
$this->params['breadcrumbs'][] = ['label' => 'Play Lists', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="play-list-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
