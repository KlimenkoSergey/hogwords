<?php
$this->title = 'Пополнение баланса';
$this->params['breadcrumbs'][] = $this->title;

?>



<div class="container">

    <h1><?php echo $this->title; ?></h1>

    <?php
    //$_SERVER['HTTP_HOST'] = 'soclike.kiev.ua';
    $current_shop = \common\models\PaymentgatewayShops::find()->where( ['id' => 7] )->one();
    //print_r($current_shop->id);

    ?>

    <div >

        <h3>Пополнение через фри-кассу (VISA, MasterCard, Тинькофф, Сбербанк ...)</h3>
        <p>При оплате вернитесь на сайт и дождитесь подтверждения платежа (это может занять 1-5 минут).</p>
        <form method="POST" action="https://soctarget.org/index.php?r=paymentgateway/payform"  target="_blank">
            <table width="100%" border="0">

                <tbody>
                <tr>
                    <td colspan="3" >Введите сумму в российских рублях</td>

                </tr>
                <tr>
                    <td width="115">


                        <input type="hidden" name="o" value="<?php echo Yii::$app->user->identity->id; ?>">
                        <input type="text" size="5" name="oa" value="100" placeholder="RUR" class="form-control"  style="display:inline; width:110px;" ></td>
                    <td width="58">руб.
                    </td>
                    <td><input type="submit" value="Пополнить баланс" class="btn btn-primary" >
                        <input type="hidden" name="lang" value="ru">

                        <!--                        передаем свои поля -->

                        <input type="hidden" name="us_name" value="shop_id">
                        <input type="hidden" name="shop_id" value="<?php echo $current_shop->id; ?>">

                    </td>
                </tr>
                </tbody>
            </table>



        </form>

    </div>
    <div style="height:50px;"></div


    ><div  >
        <h3>Пополнение через интеркассу (Электронные деньги ...)</h3>
        <form id="payment" name="payment" method="post" action="https://sci.interkassa.com/" enctype="utf-8">
            <input type="hidden" name="ik_co_id" value="593fdb4a3b1eafba618b4567" />
            <input type="hidden" name="ik_pm_no" value="ID_4233" />

            <table width="400" border="0">
                <tbody>
                <tr>
                    <td colspan="3" >Введите сумму в российских рублях</td>

                </tr>
                <tr>
                    <td width="115"><input type="text" name="ik_am" value="100" class="form-control" style="display:inline; width:110px;" /></td>
                    <td width="58">руб. <input type="hidden" name="ik_cur" value="RUB" />
                        <input type="hidden" name="ik_desc" value="<?php echo Yii::$app->user->identity->id; ?>" /></td>
                    <td><input type="submit" value="Пополнить баланс" class="btn btn-primary" ></td>
                </tr>
                </tbody>
            </table>

        </form>
    </div>









    <div style="height:25px;"></div>






</div>
