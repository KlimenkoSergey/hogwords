<?php

/* @var $this yii\web\View */

$this->title = 'Учить английские слова онлайн тренажер. Эффективно, быстро на каждый день.';
?>

<div class="video-background">
	<i class="fas fa-times"></i>
	<div class="video_block">
		
	</div>
</div>

<article id="post-19" class="post-19 page type-page status-publish hentry">
    <div class="entry-content">
        <div data-vc-full-width="true" data-vc-full-width-init="false"
             class="vc_row wpb_row vc_row-fluid vc_custom_1570165198308 vc_row-has-fill">
            <div class="wpb_column vc_column_container vc_col-sm-12">
                <div class="vc_column-inner">
                    <div class="wpb_wrapper"><p class="rs-p-wp-fix"></p>
                        <rs-module-wrap id="rev_slider_2_1_wrapper" data-source="gallery"
                                        style="background:transparent;padding:0;margin:0px auto;margin-top:0;margin-bottom:0;">
                            <rs-module id="rev_slider_2_1" style="" data-version="6.2.6">
                                <rs-slides>
                                    <rs-slide data-key="rs-12" data-title="Slide"
                                              data-anim="ei:d;eo:d;s:1000;r:0;t:fade;sl:0;"><img
                                            src=/static/img/3/transparent.png"
                                            title="Home" class="rev-slidebg" data-no-retina>
                                        <rs-layer id="slider-2-slide-12-layer-0" data-type="text" data-rsp_ch="on"
                                                  data-xy="x:l,c,c,c;xo:32px,0,0,0;y:m;yo:-143px,-190px,-210px,-140px;"
                                                  data-text="w:normal;s:48,38,35,32;l:58,40,37,34;a:left,center,center,center;"
                                                  data-dim="w:452px,580px,320px,320px;" data-frame_0="y:50,30,28,19;"
                                                  data-frame_1="sp:1000;"
                                                  data-frame_999="y:-50,-30,-28,-19;o:0;st:w;sp:1000;sR:8000;"
                                                  style="z-index:10;">Добро пожаловать на сервис для изучения английских слов!
                                        </rs-layer>
                                        <rs-layer id="slider-2-slide-12-layer-1" data-type="text" data-rsp_ch="on"
                                                  data-xy="x:l,c,c,c;xo:32px,0,0,0;y:m;yo:-23px,-110px,-120px,-53px;"
                                                  data-text="w:normal;s:18,16,16,16;l:28,22,22,22;fw:500;a:left,center,center,center;"
                                                  data-dim="w:450px,520px,320px,320px;" data-frame_0="y:50,30,28,19;"
                                                  data-frame_1="st:500;sp:1000;sR:500;"
                                                  data-frame_999="y:50,30,28,19;o:0;st:w;sp:1000;sR:7500;"
                                                  style="z-index:9;">Онлайн тренажер для изучения английских слов.
                                        </rs-layer>
                                        <a id="slider-2-slide-12-layer-2" class="rs-layer"
                                           href="/site/signup" target="_blank" rel="nofollow noopener"
                                           data-type="button" data-rsp_ch="on"
                                           data-xy="x:l,c,c,c;xo:30px,0,0,0;y:m;yo:85px,-30px,-30px,30px;"
                                           data-text="w:normal;s:16,14,16,14;l:24,14,24,18;"
                                           data-padding="t:15,14,12,14;r:40,30,30,30;b:15,14,12,14;l:40,30,30,30;"
                                           data-border="bor:12px,30px,12px,30px;" data-frame_0="x:-50,-30,-28,-19;"
                                           data-frame_1="st:1000;sp:1000;sR:1000;"
                                           data-frame_999="x:-50,-30,-28,-19;o:0;st:w;sp:500;sR:7000;"
                                           data-frame_hover="c:#007eff;bgc:#fff;bor:12px,30px,12px,30px;"
                                           style="z-index:8;background-color:#007eff;cursor:pointer;">Зарегистрируйтесь  <i class="stm-icon stm-icon-arrow-r"
                                                                                                                            style="margin-left: 5px; font-size:10px"></i> </a>
																															
																															
                                        <rs-layer  data-type="image" data-rsp_ch="on"data-xy="x:r,c,c,c;xo:-70px,0,0,0;y:m;yo:-10px,200px,200px,220px;"data-dim="w:693px,426px,402px,283px;h:531px,326px,307px,216px;" data-frame_999="o:0;st:w;" style="z-index:11; position: relative;">
                                            <noscript>
												<img wpfc-lazyload-disable="true" src="/static/img/1/main.png" width="1386" height="1062" data-no-retina>
											</noscript>
											
                                            <img class="lazyload" wpfc-lazyload-disable="true" src='data:image/svg+xml,%3Csvg%20xmlns=%22http://www.w3.org/2000/svg%22%20viewBox=%220%200%201386%201062%22%3E%3C/svg%3E' data-src="/static/img/1/main.png" width="1386" height="1062" data-no-retina>
											
											<div class="play_video-but" data-url="https://www.youtube.com/embed/ygdgbG-p9HE">
												<div class="pv-b"><i class="fas fa-play"></i></div>
											</div>
										</rs-layer>
										
										
										
										
										
                                    </rs-slide>
                                </rs-slides>
                                <rs-progress class="rs-bottom" style="visibility: hidden !important;"></rs-progress>
                            </rs-module>
                        </rs-module-wrap>
                        <div class="stm-spacing" id="stm-spacing-60dad28944d57"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="vc_row-full-width vc_clearfix"></div>
        <div data-vc-full-width="true" data-vc-full-width-init="false"
             class="vc_row wpb_row vc_row-fluid superiority_section vc_custom_1570421691129 vc_row-has-fill">
            <div class="wpb_column vc_column_container vc_col-sm-12">
                <div class="vc_column-inner vc_custom_1570015088817">
                    <div class="wpb_wrapper">
                        <div class="stm-spacing" id="stm-spacing-60dad2894630f"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="vc_row-full-width vc_clearfix"></div>






        <?= $this->render('blocks/workout', [
            'play_list' => $play_list,
        ]) ?>















        <div data-vc-full-width="true" data-vc-full-width-init="false"
             class="vc_row wpb_row vc_row-fluid vc_custom_1570269272221 vc_row-has-fill">
            <div class="wpb_column vc_column_container vc_col-sm-12">
                <div class="vc_column-inner">
                    <div class="wpb_wrapper">
                        <div class="stm-spacing" id="stm-spacing-60dad28946a0b"></div>
                        <h2 style="text-align: center; font-weight: 400;" class="vc_custom_heading">Наши преимущества</h2>
                        <div class="stm-spacing" id="stm-spacing-60dad28948253"></div>
                        <div class="vc_row wpb_row vc_inner vc_row-fluid">
                            <div class="wpb_column vc_column_container vc_col-sm-6 vc_col-lg-3">
                                <div class="vc_column-inner">
                                    <div class="wpb_wrapper">
                                        <div class="stm-icon-box stm-icon-box_ic-pos_left stm-icon-box_style-5"
                                             style="text-align:left">
                                            <div class="stm-icon-box__content">
                                                <div style="justify-content: center;" class="stm-icon-box__ic-container" style="font-size:40px"><span
                                                        class="stm-icon stm-icon-toy-kinder stm-font_color_blue"
                                                        style="font-size:40px"><img src="static/img/el1.png" alt="el1"></span></div>
                                                <div style="text-align: center; font-size: 20px; font-weight: 400;" class="stm-icon-box__title stm-font_color_dark">Обучение на видео материалах</div>
                                                <div style="text-align: center; font-size: 15px; font-weight: 400;" class="stm-icon-box__descr stm-font_color_dark">Вы сможете просматривать интересные и полезные видео при этом изучать новые слова
                                                </div>
                                               </div>
                                        </div>
                                        <div class="stm-spacing" id="stm-spacing-60dad2894a583"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="wpb_column vc_column_container vc_col-sm-6 vc_col-lg-3">
                                <div class="vc_column-inner">
                                    <div class="wpb_wrapper">
                                        <div class="stm-icon-box stm-icon-box_ic-pos_left stm-icon-box_style-5"
                                             style="text-align:left">
                                            <div class="stm-icon-box__content">
                                                <div class="stm-icon-box__ic-container" style="font-size:40px"><span
                                                        class="stm-icon stm-icon-abc-block-kinder stm-font_color_blue"
                                                        style="font-size:40px"><img src="static/img/el2.png" alt="el1"></span></div>
                                                <div style="text-align: center; font-size: 20px; font-weight: 400;" class="stm-icon-box__title stm-font_color_dark">Правильное произношение слов</div>
                                                <div style="text-align: center; font-size: 15px; font-weight: 400;" class="stm-icon-box__descr stm-font_color_dark">Вы сможете услышать правильное звучание слов и повторять, для идеального произношения
                                                </div>
                                                </div>
                                        </div>
                                        <div class="stm-spacing" id="stm-spacing-60dad2894b25d"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="wpb_column vc_column_container vc_col-sm-6 vc_col-lg-3">
                                <div class="vc_column-inner">
                                    <div class="wpb_wrapper">
                                        <div class="stm-icon-box stm-icon-box_ic-pos_left stm-icon-box_style-5"
                                             style="text-align:left">
                                            <div class="stm-icon-box__content">
                                                <div class="stm-icon-box__ic-container" style="font-size:40px"><span
                                                        class="stm-icon stm-icon-car-kinder stm-font_color_blue"
                                                        style="font-size:40px"><img src="static/img/el3.png" alt="el1"></span></div>
                                                <div style="text-align: center; font-size: 20px; font-weight: 400;" class="stm-icon-box__title stm-font_color_dark">Персональный словарь</div>
                                                <div style="text-align: center; font-size: 15px; font-weight: 400;" class="stm-icon-box__descr stm-font_color_dark">У Вас будет составлен персональный словарь с новыми незнакомыми словами
                                                </div>

                                            </div>
                                        </div>
                                        <div class="stm-spacing" id="stm-spacing-60dad2894c8af"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="wpb_column vc_column_container vc_col-sm-6 vc_col-lg-3">
                                <div class="vc_column-inner">
                                    <div class="wpb_wrapper">
                                        <div class="stm-icon-box stm-icon-box_ic-pos_left stm-icon-box_style-5"
                                             style="text-align:left">
                                            <div class="stm-icon-box__content">
                                                <div class="stm-icon-box__ic-container" style="font-size:40px"><span
                                                        class="stm-icon stm-icon-pacifier-kinder stm-font_color_blue"
                                                        style="font-size:40px"><img src="static/img/el4.png" alt="el1"></span></div>
                                                <div style="text-align: center; font-size: 20px; font-weight: 400;" class="stm-icon-box__title stm-font_color_dark">Тренировка и проверка слов</div>
                                                <div style="text-align: center; font-size: 15px; font-weight: 400;" class="stm-icon-box__descr stm-font_color_dark">Ваши новые слова будут не только накапливаться , но и проверятся рандомно, для лучшего запоминания
                                                </div>
                                               </div>
                                        </div>
                                        <div class="stm-spacing" id="stm-spacing-60dad2894e3fc"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="vc_row-full-width vc_clearfix"></div>
        <div data-vc-full-width="true" data-vc-full-width-init="false"
             class="vc_row wpb_row vc_row-fluid vc_custom_1570015394508 vc_row-has-fill">
            <div class="wpb_column vc_column_container vc_col-sm-12">
                <div class="vc_column-inner">
                    <div class="wpb_wrapper"><h2 style="text-align: center;" class="vc_custom_heading">Как это работает?</h2>
                        <div class="stm-spacing" id="stm-spacing-60dad2894f075"></div>
                        <div class="vc_row wpb_row vc_inner vc_row-fluid">
                            <div class="wpb_column vc_column_container vc_col-sm-6">
                                <div class="vc_column-inner">
                                    <div class="wpb_wrapper">
                                        <div class="stm-action-box" style="text-align:left">
                                            <figure class="stm-action-box__figure_box"><span
                                                    class="stm-action-box__thumbnail"> <noscript><img
                                                            src="/static/img/kid1.png"
                                                            width="390" height="390" alt="gallery9"
                                                            title="gallery9"/></noscript><img
                                                        wpfc-lazyload-disable="true" class="lazyload"
                                                        src='/static/img/kid1.png'
                                                        data-src="static/img/kid1.png"
                                                        width="390" height="390" alt="gallery9" title="gallery9"/> </span>
                                                <figcaption class="stm-action-box__figcaption_box"><span
                                                        class="stm-action-box__figcaption-title"> <span
                                                            class="stm-action-box__title-text">Обучайся от простого к сложному</span> </span>
                                                    <div class="stm-action-box__content-text"> Начни учить простые короткие слова до длинных и сложных
                                                    </div>
                                                </figcaption>
                                            </figure>
                                        </div>
                                        <div class="stm-spacing" id="stm-spacing-60dad289516c1"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="wpb_column vc_column_container vc_col-sm-6">
                                <div class="vc_column-inner">
                                    <div class="wpb_wrapper">
                                        <div class="stm-action-box" style="text-align:left">
                                            <figure class="stm-action-box__figure_box"><span
                                                    class="stm-action-box__thumbnail"> <noscript><img
                                                            src="/static/img/kid2.png"
                                                            width="390" height="380" alt="news9"
                                                            title="news9"/></noscript><img
                                                        wpfc-lazyload-disable="true" class="lazyload"
                                                        src='/static/img/kid2.png'
                                                        data-src="static/img/kid2.png"
                                                        width="390" height="380" alt="news9" title="news9"/> </span>
                                                <figcaption class="stm-action-box__figcaption_box"><span
                                                        class="stm-action-box__figcaption-title"> <span
                                                            class="stm-action-box__title-text">Возрастные особенности</span> </span>
                                                    <div class="stm-action-box__content-text"> Обучение для людей разного возраста от маленьких до взрослых
                                                    </div>
                                                </figcaption>
                                            </figure>
                                        </div>
                                        <div class="stm-spacing" id="stm-spacing-60dad2895e1e1"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="vc_row wpb_row vc_inner vc_row-fluid">
                            <div class="wpb_column vc_column_container vc_col-sm-6">
                                <div class="vc_column-inner">
                                    <div class="wpb_wrapper">
                                        <div class="stm-action-box" style="text-align:left">
                                            <figure class="stm-action-box__figure_box"><span
                                                    class="stm-action-box__thumbnail"> <noscript><img
                                                            src="/static/img/kid3.png"
                                                            width="240" height="240" alt="img3"
                                                            title="img3"/></noscript><img
                                                        wpfc-lazyload-disable="true" class="lazyload"
                                                        src='/static/img/kid3.png'
                                                        data-src="static/img/kid3.png"
                                                        width="240" height="240" alt="img3" title="img3"/> </span>
                                                <figcaption class="stm-action-box__figcaption_box"><span
                                                        class="stm-action-box__figcaption-title"> <span
                                                            class="stm-action-box__title-text">Обучайся дома</span> </span>
                                                    <div class="stm-action-box__content-text"> Обучайся в привычной обстановке экономя свое время на поездках
                                                    </div>
                                                </figcaption>
                                            </figure>
                                        </div>
                                        <div class="stm-spacing" id="stm-spacing-60dad2895edf6"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="wpb_column vc_column_container vc_col-sm-6">
                                <div class="vc_column-inner">
                                    <div class="wpb_wrapper">
                                        <div class="stm-action-box" style="text-align:left">
                                            <figure class="stm-action-box__figure_box"><span
                                                    class="stm-action-box__thumbnail"> <noscript><img
                                                            src="/static/img/kid4.png"
                                                            width="240" height="240" alt="img4"
                                                            title="img4"/></noscript><img
                                                        wpfc-lazyload-disable="true" class="lazyload"
                                                        src='/static/img/kid4.png'
                                                        data-src="static/img/kid4.png"
                                                        width="240" height="240" alt="img4" title="img4"/> </span>
                                                <figcaption class="stm-action-box__figcaption_box"><span
                                                        class="stm-action-box__figcaption-title"> <span
                                                            class="stm-action-box__title-text">Европейское произношение</span> </span>
                                                    <div class="stm-action-box__content-text"> Европейское произношение
                                                    </div>
                                                </figcaption>
                                            </figure>
                                        </div>
                                        <div class="stm-spacing" id="stm-spacing-60dad289609eb"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="stm-spacing" id="stm-spacing-60dad28961d72"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="vc_row-full-width vc_clearfix"></div>
        <div data-vc-full-width="true" data-vc-full-width-init="false"
             class="vc_row wpb_row vc_row-fluid superiority_section vc_custom_1570429434926 vc_row-has-fill">
            <div class="wpb_column vc_column_container vc_col-sm-12">
                <div class="vc_column-inner vc_custom_1570015088817">
                    <div class="wpb_wrapper">
                        <div class="stm-spacing" id="stm-spacing-60dad28962544"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="vc_row-full-width vc_clearfix"></div>
        <div data-vc-full-width="true" data-vc-full-width-init="false"
             class="vc_row wpb_row vc_row-fluid vc_custom_1570179319698 vc_row-has-fill">
            <div class="wpb_column vc_column_container vc_col-sm-12">
                <div class="vc_column-inner">
                    <div class="wpb_wrapper">
                        <div class="stm-spacing" id="stm-spacing-60dad28962cf8"></div>
                        <h2 style="color: #ffffff;text-align: center;" class="vc_custom_heading">Слова для любой цели</h2>
                        <div class="stm-spacing" id="stm-spacing-60dad2896409b"></div>
                        <div class="all_events_blocks">
                            <div class="event-block">
                                <div class="circle"></div>
                                <div class="ev-img">
                                    <img src="static/img/ev1.png" alt="ev1">
                                </div>
                                <div class="ev-text cev1">Быстрый старт</div>
                            </div>
                            <div class="event-block">
                                <div style="top: -35px;" class="circle"></div>
                                <div class="ev-img">
                                    <img src="static/img/ev2.png" alt="ev2">
                                </div>
                                <div class="ev-text cev2">Свободное общение</div>
                            </div>
                            <div class="event-block">
                                <div class="circle"></div>
                                <div class="ev-img">
                                    <img src="static/img/ev3.png" alt="ev3"></div>
                                <div class="ev-text cev3">Путешествие</div>
                            </div>
                            <div class="event-block">
                                <div style="top: -30px;" class="circle"></div>
                                <div class="ev-img">
                                    <img src="static/img/ev4.png" alt="ev4">
                                </div>
                                <div class="ev-text cev4">Понимать кино</div>
                            </div>
                            <div class="event-block">
                                <div style="top: -20px;" class="circle"></div>
                                <div class="ev-img">
                                    <img src="static/img/ev5.png" alt="ev5">
                                </div>
                                <div class="ev-text cev5">Школа</div>
                            </div>
                            <div class="event-block">
                                <div class="circle"></div>
                                <div class="ev-img">
                                    <img src="static/img/ev6.png" alt="ev6">
                                </div>
                                <div class="ev-text cev6">Посещать экскурсии</div>
                            </div>
                        </div>
                        <div class="stm-spacing" id="stm-spacing-60dad2896a75a"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="vc_row-full-width vc_clearfix"></div>
        <div data-vc-full-width="true" data-vc-full-width-init="false"
             class="vc_row wpb_row vc_row-fluid superiority_section vc_custom_1570178716725 vc_row-has-fill">
            <div class="wpb_column vc_column_container vc_col-sm-12">
                <div class="vc_column-inner vc_custom_1570015088817">
                    <div class="wpb_wrapper">
                        <div class="stm-spacing" id="stm-spacing-60dad2896bda4"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="vc_row-full-width vc_clearfix"></div>
        <div class="vc_row wpb_row vc_row-fluid">
            <div class="wpb_column vc_column_container vc_col-sm-12">
                <div class="vc_column-inner">
                    <div class="wpb_wrapper">
                        <div class="stm-spacing" id="stm-spacing-60dad2896c504"></div>
                        <h2 style="text-align: center;" class="vc_custom_heading">Наши материалы для обучения</h2>
                        <div class="stm-spacing" id="stm-spacing-60dad2896c844"></div>
                        <div class="stm-carousel stm-carousel_view_courses" id="stm-posts_carousel-60dad2896ca78">
                            <div class="courses_post__box courses_post__has_thumbnail">
                                <div class="courses_post__thumbnail">
                                        <noscript><img
                                                src="/static/img/ch1.png"
                                                width="350" height="190" alt="classes1" title="classes1"/>
                                        </noscript>
                                        <img wpfc-lazyload-disable="true" class="lazyload"
                                             src='/static/img/ch1.png'
                                             data-src="/static/img/ch1.png"
                                             width="350" height="190" alt="classes1" title="classes1"/></div>
                                <div class="courses_post__body_wrap">
                                    <div class="courses_post__body">
                                        <div class="courses_post__title">Лингвистический интерактивный словарь</div>
                                        <div class=""><p>Правильное произношение слов, которое можно повторять ежедневно для тренировки.</p></div>
                                        <!--<div class="courses_post__meta">
                                            <ul>
                                                <li>
                                                    <div>Age</div>
                                                    1—2 years
                                                </li>
                                                <li>
                                                    <div>Size</div>
                                                    9 seats
                                                </li>
                                                <li>
                                                    <div>Price</div>
                                                    $25/hour
                                                </li>
                                            </ul>
                                        </div>-->
                                    </div>
                                    <!--<a href="https://smartyschool.stylemixthemes.com/kindergarten-two/courses/color-matching-class/"
                                       class="courses_post__link_more"><span>Читать далее</span></a>--></div>
                            </div>
                            <div class="courses_post__box courses_post__has_thumbnail">
                                <div class="courses_post__thumbnail">
                                        <noscript><img
                                                src="/static/img/ch2.png"
                                                width="350" height="190" alt="classes2" title="classes2"/>
                                        </noscript>
                                        <img wpfc-lazyload-disable="true" class="lazyload"
                                             src='/static/img/ch2.png'
                                             data-src="/static/img/ch2.png"
                                             width="350" height="190" alt="classes2" title="classes2"/></div>
                                <div class="courses_post__body_wrap">
                                    <div class="courses_post__body">
                                        <div class="courses_post__title">Интересные и увлекательные материалы</div>
                                        <div class=""><p>Материалы которые позволяют не только учить английские слова но и приобрести чтото интересное.</p></div>
                                        <!--<div class="courses_post__meta">
                                            <ul>
                                                <li>
                                                    <div>Age</div>
                                                    2—3 years
                                                </li>
                                                <li>
                                                    <div>Size</div>
                                                    11 seats
                                                </li>
                                                <li>
                                                    <div>Price</div>
                                                    $25/hour
                                                </li>
                                            </ul>
                                        </div>-->
                                    </div>
                                    <!--<a href="https://smartyschool.stylemixthemes.com/kindergarten-two/courses/astronomy/"
                                       class="courses_post__link_more"><span>Читать далее</span></a>--></div>
                            </div>



                            <div class="courses_post__box courses_post__has_thumbnail">
                                <div class="courses_post__thumbnail">
                                        <noscript><img
                                                src="/static/img/ch3.png"
                                                width="350" height="190" alt="classes3" title="classes3"/>
                                        </noscript>
                                        <img wpfc-lazyload-disable="true" class="lazyload"
                                             src='/static/img/ch3.png'
                                             data-src="/static/img/ch3.png"
                                             width="350" height="190" alt="classes3" title="classes3"/></div>
                                <div class="courses_post__body_wrap">
                                    <div class="courses_post__body">
                                        <div class="courses_post__title">Обучающие и развивающие игры</div>
                                        <div class=""><p>Можно учится в виде игры при этом изучать новые слова, что более легче в восприятии.</p></div>
                                        <!--<div class="courses_post__meta">
                                            <ul>
                                                <li>
                                                    <div>Age</div>
                                                    3—4 years
                                                </li>
                                                <li>
                                                    <div>Size</div>
                                                    15 seats
                                                </li>
                                                <li>
                                                    <div>Price</div>
                                                    $25/hour
                                                </li>
                                            </ul>
                                        </div>-->
                                    </div>
                                    <!--<a href="https://smartyschool.stylemixthemes.com/kindergarten-two/courses/letter-match-class/"
                                       class="courses_post__link_more"><span>Читать далее</span></a>--></div>
                            </div>
                        </div>
                        <div class="stm-spacing" id="stm-spacing-60dad28975cce"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="vc_row wpb_row vc_row-fluid vc_row-o-equal-height vc_row-o-content-middle vc_row-flex">
            <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-6">
                <div class="vc_column-inner">
                    <div class="wpb_wrapper"><h2 style="text-align: left; font-size: 30px;" class="vc_custom_heading">ЧТО ВЫ ПОЛУЧАЕТЕ?</h2>
                        <div class="wpb_text_column wpb_content_element">
                            <div class="wpb_wrapper">
                                <p style="border-bottom: 1px solid #E5E5E5;">Удобный аккаунт</p>
                                <p style="border-bottom: 1px solid #E5E5E5;">Персональный словарь</p>
                                <p style="border-bottom: 1px solid #E5E5E5;">Более 10 000 иностранных слов</p>
                                <p style="border-bottom: 1px solid #E5E5E5;">Игры, видео, мультфильмы в свобоном доступе на оригинальном языке</p>
                                <p style="border-bottom: 1px solid #E5E5E5;">Контроль прогресса изучения</p>
                            </div>
                        </div>
                        <div class="stm-btn-container stm-btn-container_left"><a href="/site/signup"
                                                                                 target="_self"
                                                                                 class="stm-btn stm-btn_flat stm-btn_orange stm-btn_md stm-btn_icon-right">Приобрести аккаунт</a></div>
                        <div class="stm-spacing" id="stm-spacing-60dad289787ca"></div>
                    </div>
                </div>
            </div>
            <div class="wpb_column vc_column_container vc_col-sm-1 vc_hidden-sm vc_hidden-xs">
                <div class="vc_column-inner">
                    <div class="wpb_wrapper"></div>
                </div>
            </div>
            <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-5 vc_col-md-5">
                <div class="vc_column-inner">
                    <div class="wpb_wrapper">
                        <div class="vc_row wpb_row vc_inner vc_row-fluid">
                            <div class="wpb_column vc_column_container vc_col-sm-6 vc_col-xs-6">
                                <img style="width: 50%;" src="static/img/vec1.png" alt="vec1">
                            </div>
                            <div class="wpb_column vc_column_container vc_col-sm-6 vc_col-xs-6">
                                <img style="width: 50%;" src="static/img/vec2.png" alt="vec2">
                            </div>
                        </div>
                        <div class="vc_row wpb_row vc_inner vc_row-fluid">
                            <div class="wpb_column vc_column_container vc_col-sm-6 vc_col-xs-6">
                                <img style="width: 50%;" src="static/img/vec3.png" alt="vec3">
                            </div>
                            <div class="wpb_column vc_column_container vc_col-sm-6 vc_col-xs-6">
                                <img style="width: 50%;" src="static/img/vec4.png" alt="vec4">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="wpb_column vc_column_container vc_col-sm-12">
                <div class="vc_column-inner">
                    <div class="wpb_wrapper">
                        <div class="stm-spacing" id="stm-spacing-60dad2897ed58"></div>
                    </div>
                </div>
            </div>
        </div>
        <div data-vc-full-width="true" data-vc-full-width-init="false"
             class="vc_row wpb_row vc_row-fluid superiority_section vc_custom_1570103838348 vc_row-has-fill">
            <div class="wpb_column vc_column_container vc_col-sm-12">
                <div class="vc_column-inner vc_custom_1570015088817">
                    <div class="wpb_wrapper">
                        <div class="stm-spacing" id="stm-spacing-60dad2897f66c"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="vc_row-full-width vc_clearfix"></div>
        <div data-vc-full-width="true" data-vc-full-width-init="false"
             class="vc_row wpb_row vc_row-fluid vc_custom_1570165106385 vc_row-has-fill">
            <div class="wpb_column vc_column_container vc_col-sm-12">
                <div class="vc_column-inner">
                    <div class="wpb_wrapper">
                        <div class="stm-spacing" id="stm-spacing-60dad28980c92"></div>
                        <h2 style="color: #ffffff;text-align: center;" class="vc_custom_heading">Интересные статьи</h2>
                        <div class="stm-spacing" id="stm-spacing-60dad28980fc3"></div>
                        <div class="vc_row wpb_row vc_inner vc_row-fluid post_color_white">
                            <div class="wpb_column vc_column_container vc_col-sm-12">
                                <div class="vc_column-inner">
                                    <div class="wpb_wrapper">
                                        <div class="stm-posts">
                                            <div class="row">
                                                <div class="col-sm-4 col-xs-12 stm-posts__items-row_view_masonry">
                                                    <div id="post-107"
                                                         class="post-107 post type-post status-publish format-standard has-post-thumbnail hentry category-olympiad category-athletics tag-school tag-teachers tag-parents sticky post_view_grid posts_has__thumb">
                                                        <div class="entry-body">
                                                            <div class="entry-thumbnail-container">
                                                                <div class="entry-thumbnail"><a
                                                                        href="#">
                                                                        <noscript><img
                                                                                src="/static/img/ch4.png"
                                                                                width="350" height="220" alt="news1"
                                                                                title="news1"/></noscript>
                                                                        <img wpfc-lazyload-disable="true"
                                                                             class="lazyload"
                                                                             src='/static/img/ch4.png'
                                                                             data-src="static/img/ch4.png"
                                                                             width="350" height="220" alt="news1"
                                                                             title="news1"/></a>
                                                                    <div class="posts_post__date"><a
                                                                            href="#">
                                                                            <span>3</span> Apr </a></div>
                                                                </div>
                                                            </div>
                                                            <div class="entry-details-container">
                                                                <div class="entry-details">
                                                                    <div class="posts_post__date"><a
                                                                            href="#">
                                                                            <span>3</span> Apr </a></div>
                                                                    <h5 class="entry-title">
                                                                        <a href="#">Нескучный английский язык для детей: учим алфавит</a>
                                                                    </h5>
                                                                    <div class="entry-summary">
                                                                        <p>Алфавит в английском называется alphabet или просто ABC. В нем 26 букв, из них 20 	согласных (consonants) и всего 6 гласных (vowels).</p>
                                                                        <p>Практически все буквы алфавита американцы и британцы произносят одинаково, кроме последней. В американском английском Z будет звучать как «зи» [ziː].</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4 col-xs-12 stm-posts__items-row_view_masonry">
                                                    <div id="post-104"
                                                         class="post-104 post type-post status-publish format-standard has-post-thumbnail hentry category-sport category-athletics post_view_grid posts_has__thumb">
                                                        <div class="entry-body">
                                                            <div class="entry-thumbnail-container">
                                                                <div class="entry-thumbnail"><a
                                                                        href="#">
                                                                        <noscript><img
                                                                                src="/static/img/ch5.png"
                                                                                width="350" height="220" alt="news2"
                                                                                title="news2"/></noscript>
                                                                        <img wpfc-lazyload-disable="true"
                                                                             class="lazyload"
                                                                             src='/static/img/ch5.png'
                                                                             data-src="static/img/ch5.png"
                                                                             width="350" height="220" alt="news2"
                                                                             title="news2"/></a>
                                                                    <div class="posts_post__date"><a
                                                                            href="#">
                                                                            <span>27</span> Mar </a></div>
                                                                </div>
                                                            </div>
                                                            <div class="entry-details-container">
                                                                <div class="entry-details">
                                                                    <div class="posts_post__date"><a
                                                                            href="#">
                                                                            <span>27</span> Mar </a></div>
                                                                    <h5 class="entry-title">
                                                                        <a href="#">One, two, three, елочка — гори! Стихи для детей на английском </a>
                                                                    </h5>
                                                                    <div class="entry-summary">
                                                                        <p>В этой статье вы найдете не только простые стишки на английском для малышей, но и интересные детские стихи с переводом для ребят постарше.</p>
                                                                        <p>Вне зависимости от уровня знаний малыша, рекомендуем всегда начинать учить стихи на английском языке сразу с переводом, чтобы малыш не просто запоминал звуки и слова, а осмысливал стих целиком.</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4 col-xs-12 stm-posts__items-row_view_masonry">
                                                    <div id="post-101"
                                                         class="post-101 post type-post status-publish format-standard has-post-thumbnail hentry category-school-life category-athletics tag-school tag-education tag-wordpress tag-themeforest post_view_grid posts_has__thumb">
                                                        <div class="entry-body">
                                                            <div class="entry-thumbnail-container">
                                                                <div class="entry-thumbnail"><a
                                                                        href="#">
                                                                        <noscript><img
                                                                                src="/static/img/ch6.png"
                                                                                width="350" height="220" alt="news3"
                                                                                title="news3"/></noscript>
                                                                        <img wpfc-lazyload-disable="true"
                                                                             class="lazyload"
                                                                             src='/static/img/ch6.png'
                                                                             data-src="static/img/ch6.png"
                                                                             width="350" height="220" alt="news3"
                                                                             title="news3"/></a>
                                                                    <div class="posts_post__date"><a
                                                                            href="#">
                                                                            <span>19</span> Mar </a></div>
                                                                </div>
                                                            </div>
                                                            <div class="entry-details-container">
                                                                <div class="entry-details">
                                                                    <div class="posts_post__date"><a
                                                                            href="#">
                                                                            <span>19</span> Mar </a></div>
                                                                    <h5 class="entry-title">
                                                                        <a href="#">Разомнем мозги: загадки на английском</a>
                                                                    </h5>
                                                                    <div class="entry-summary">
                                                                        <p>Загадки на английском полезны как детям, так и начинающим изучать язык взрослым. Они помогут выучить не только новые слова, но и много синонимов и простых описаний предметов и явлений. </p>
                                                                        <p>В этой статье вы найдете интересные загадки для детей на английском языке, а также стихотворные загадки и головоломки для взрослых. </p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="text-left"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="stm-spacing" id="stm-spacing-60dad2898b0a3"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="vc_row-full-width vc_clearfix"></div>
        <div data-vc-full-width="true" data-vc-full-width-init="false"
             class="vc_row wpb_row vc_row-fluid superiority_section vc_custom_1570684770623 vc_row-has-fill">
            <div class="wpb_column vc_column_container vc_col-sm-12">
                <div class="vc_column-inner vc_custom_1570015088817">
                    <div class="wpb_wrapper">
                        <div class="stm-spacing" id="stm-spacing-60dad2898bfc7"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="vc_row-full-width vc_clearfix"></div>
        <div class="vc_row wpb_row vc_row-fluid vc_custom_1570168473530 vc_row-has-fill">
            <div class="wpb_column vc_column_container vc_col-sm-12">
                <div class="vc_column-inner">
                    <div class="wpb_wrapper">
                        <div class="stm-spacing" id="stm-spacing-60dad2898db9c"></div>
                        <h2 style="text-align: center" class="vc_custom_heading">Отзывы пользователей о сервисе</h2>
                        <div class="stm-spacing" id="stm-spacing-60dad2898df82"></div>
                        <div class="stm-testimonials">
                            <div class="stm-carousel stm-carousel_type_testimonials stm-carousel_dots_blue">
                                <div class="stm-testimonial stm-testimonial_style_1">
                                    <div class="stm-testimonial__avatar_box">
                                        <div class="stm-testimonial__avatar">
                                            <noscript><img onload="Wpfcll.r(this,true);"
                                                           src="/static/img/fam1.png"
                                                           wpfc-data-original-src="static/img/fam1.png"
                                                           width="100" height="100" alt="testimonial_1"
                                                           title="testimonial_1"/></noscript>
                                            <img class="lazyload"
                                                 src='/static/img/fam1.png'
                                                 data-src="static/img/fam1.png"
                                                 width="100" height="100" alt="testimonial_1" title="testimonial_1"/>
                                        </div>
                                    </div>
                                    <div class="stm-testimonial__content">
                                        <div class="stm-testimonial__author"> Мария</div>
                                        <div class="stm-testimonial__text"><p>Очень важно что бы с детства ребенок взаимодействовал с языком - весело в игровой форме!  Все очень современно, интересно, в доступной форме. Я вижу как Катя хватает новые слова на лету и безмерно радуюсь
                                            </p></div>
                                    </div>
                                </div>
                                <div class="stm-testimonial stm-testimonial_style_1">
                                    <div class="stm-testimonial__avatar_box">
                                        <div class="stm-testimonial__avatar">
                                            <noscript><img onload="Wpfcll.r(this,true);"
                                                           src="/static/img/fam2.png"
                                                           wpfc-data-original-src="static/img/fam2.png"
                                                           width="100" height="100" alt="testimonial_2"
                                                           title="testimonial_2"/></noscript>
                                            <img class="lazyload"
                                                 src='/static/img/fam2.png'
                                                 data-src="/static/img/fam2.png"
                                                 width="100" height="100" alt="testimonial_2" title="testimonial_2"/>
                                        </div>
                                    </div>
                                    <div class="stm-testimonial__content">
                                        <div class="stm-testimonial__author"> Николай
                                        </div>
                                        <div class="stm-testimonial__text"><p>Каждый раз слушая как мой ребенок пытается самостоятельно повторять фразы и слова, я радуюсь этому и ликую. Настя сама учится следить за временем и выделяет пару часиков для занятий. Результат радует нас так как со школы начала приносить одни пятерки.</p></div>
                                    </div>
                                </div>
                                <div class="stm-testimonial stm-testimonial_style_1">
                                    <div class="stm-testimonial__avatar_box">
                                        <div class="stm-testimonial__avatar">
                                            <noscript><img onload="Wpfcll.r(this,true);"
                                                           src="/static/img/fam3.png"
                                                           wpfc-data-original-src="static/img/fam3.png"
                                                           width="100" height="100" alt="testimonial_3"
                                                           title="testimonial_3"/></noscript>
                                            <img class="lazyload"
                                                 src='/static/img/fam3.png'
                                                 data-src="/static/img/fam3.png"
                                                 width="100" height="100" alt="testimonial_3" title="testimonial_3"/>
                                        </div>
                                    </div>
                                    <div class="stm-testimonial__content">
                                        <div class="stm-testimonial__author"> Светлана</div>
                                        <div class="stm-testimonial__text"><p>Нравится то что ребенок вовлечен в изучение как в игру. Очень все доступно и понятно, я как взрослая только радуюсь успехам Максима. Красочно и креативно! Сайт удобен в использовании.</p></div>
                                    </div>
                                </div>
                                <div class="stm-testimonial stm-testimonial_style_1">
                                    <div class="stm-testimonial__avatar_box">
                                        <div class="stm-testimonial__avatar">
                                            <noscript><img onload="Wpfcll.r(this,true);"
                                                           src="/static/img/fam4.png"
                                                           wpfc-data-original-src="static/img/fam4.png"
                                                           width="100" height="100" alt="testimonial_3"
                                                           title="testimonial_3"/></noscript>
                                            <img class="lazyload"
                                                 src='/static/img/fam4.png'
                                                 data-src="static/img/fam4.png"
                                                 width="100" height="100" alt="testimonial_3" title="testimonial_3"/>
                                        </div>
                                    </div>
                                    <div class="stm-testimonial__content">
                                        <div class="stm-testimonial__author"> Эдгар</div>
                                        <div class="stm-testimonial__text"><p>Сам изучаю английский и сына направляю в это. Долго искал, что может быть ему интересным. В этом сервисе мне нравится все!! Отличный подход, классная подача материала в игровой форме, качественные видеоуроки. Нравится разнообразие упражнений, за счёт этого не приедается. </p></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="stm-spacing" id="stm-spacing-60dad28992da8"></div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    </div>
    </div>

    <section class="numb_section">
        <div class="container">
            <div class="all_numb_blocks">
                <div class="numb_block">
                    <div class="num_svg"><img src="static/img/svg/sv1.svg"></div>
                    <div class="num_txt">
                        <div class="num_num nm_one">> <span style="font-family: 'Just Another Hand', cursive;">10 000</span> </div>
                        <div class="num_text">Английских слов</div>
                    </div>
                </div>
                <div class="numb_block">
                    <div class="num_svg"><img src="static/img/svg/sv2.svg"></div>
                    <div class="num_txt">
                        <div class="num_num nm_two">> <span style="font-family: 'Just Another Hand', cursive;">150</span> </div>
                        <div class="num_text">Видео  материалов</div>
                    </div>
                </div>
                <div class="numb_block">
                    <div class="num_svg"><img src="static/img/svg/sv3.svg"></div>
                    <div class="num_txt">
                        <div class="num_num nm_three">> <span style="font-family: 'Just Another Hand', cursive;">55</span> </div>
                        <div class="num_text">Проверочных тестов</div>
                    </div>
                </div>
                <div class="numb_block">
                    <div class="num_svg"><img src="static/img/svg/sv4.svg"></div>
                    <div class="num_txt">
                        <div class="num_num nm_four">> <span style="font-family: 'Just Another Hand', cursive;">500</span> </div>
                        <div class="num_text">Интересного контента</div>
                    </div>
                </div>
            </div>
        </div>
    </section>



    <div class="content content_type_vc">
        <div class="container">
            <main class="main">
                <?= $this->render('blocks/price_table') ?>








                <div class="vc_row wpb_row vc_row-fluid vc_custom_1570179806247 vc_row-has-fill">
                    <div class="wpb_column vc_column_container vc_col-sm-2">
                        <div class="vc_column-inner">
                            <div class="wpb_wrapper"></div>
                        </div>
                    </div>
                    <div class="wpb_column vc_column_container vc_col-sm-8">
                        <div class="vc_column-inner">
                            <div class="wpb_wrapper">
                                <div class="stm-spacing" id="stm-spacing-60dad28994ce2"></div>
                                <h2 style="font-size: 60px;color: #ffffff;text-align: center" class="vc_custom_heading">Подписаться на рассылку</h2>
                                <div class="wpb_text_column wpb_content_element">
                                    <div class="wpb_wrapper"><p style="text-align: center; border: 0;"><span style="color: #ffffff;">Узнавай первым о новых возможностях сервиса, конкурсах и приятных бонусах.</span>
                                        </p></div>
                                </div>
                                <div class="stm-spacing" id="stm-spacing-60dad2899530f"></div>
                                <div class="wpb_text_column wpb_content_element modal_subscribe">
                                    <div class="wpb_wrapper"><p style="text-align: center; border: 0;">
                                        <form id="mc4wp-form-1" class="mc4wp-form mc4wp-form-2874" method="post" data-id="2874"
                                              data-name="Popup form" action="/subscribes/add">
                                            <div class="mc4wp-form-fields"><input type="email" name="EMAIL"
                                                                                  placeholder="Ваш E-mail" required/>
                                                <button type="submit">Подписаться <span class='stm-icon stm-icon-arrow-r'></button>
                                            </div>
                                            <label style="display: none !important;">Leave this field empty if you're human:
                                                <input type="text" name="_mc4wp_honeypot" value="" tabindex="-1"
                                                       autocomplete="off"/></label><input type="hidden" name="_mc4wp_timestamp"
                                                                                          value="1624953481"/><input
                                                type="hidden" name="_mc4wp_form_id" value="2874"/><input type="hidden"
                                                                                                         name="_mc4wp_form_element_id"
                                                                                                         value="mc4wp-form-1"/>
                                            <div class="mc4wp-response"></div>
                                        </form>



                                        </p></div>
                                </div>
                                <div class="stm-spacing" id="stm-spacing-60dad2899700e"></div>
                            </div>
                        </div>
                    </div>
                    <div class="wpb_column vc_column_container vc_col-sm-2">
                        <div class="vc_column-inner">
                            <div class="wpb_wrapper"></div>
                        </div>
                    </div>
                </div>
                <div class="vc_row wpb_row vc_row-fluid">
                    <div class="wpb_column vc_column_container vc_col-sm-12">
                        <div class="vc_column-inner">
                            <div class="wpb_wrapper">
                                <div class="stm-spacing" id="stm-spacing-60dad28998f55"></div>
                            </div>
                        </div>
                    </div>
                </div>
</article>


<div class="site-index">

    <p>Учить английские слова онлайн очень удобно, ведь всё, что для этого нужно - любой гаджет и доступ к интернету.
        Сайт hogwords.net предлагает огромный выбор тренажёров для отработки лексики на разные темы: транспорт, мебель,
        еда и приёмы пищи. На сайте представлены следующие варианты упражнений:</p>

    <ul>
        <li>flash-карты со звуком;</li>
        <li>интерактивные игры;</li>
        <li>аудио и видео с использованием новых слов;</li>
        <li>подстановочные упражнения на выбор правильного слова;</li>
    </ul>

    <p>Сайт позволяет учить слова на английском языке онлайн бесплатно. Вы просто заходите на сайт, выбираете
        интересующую вас тему, вид интерактивного упражнения и начинаете тренировку. Здесь вы можете отработать
        произношение, запомнить перевод слова и контекст, в котором оно употребляется. Как только вы отработаете новые
        слова, вы можете пройти онлайн-тест, чтобы проверить свои знания:</p>

    <ul>
        <li>сопоставить слово и перевод на русском языке;</li>
        <li>сопоставить слово и картинку;</li>
        <li>сопоставить слово и звуковой файл с произношением;</li>
    </ul>

    <p>Благодаря сайту можно выучить до 25000 новых слов за один курс. Сайт предлагает самые проверенные, работающие
        мнемонические техники, позволяющие запомнить слово и активно его применить в повседневном общении. Если вас
        заинтересовал любой из тренажёров, вы можете выбрать тему, упражнение, нажать на кнопку &quot;Start&quot; и
        начать тренироваться. Все выученные слова автоматически попадают в папку, и вы сможете самостоятельно
        отслеживать свой прогресс и пополнение словарного запаса. Страница Progress Page позволяет отслеживать, сколько
        слов вы выучили в день и в течение месяца, насколько успели отработать новый лексический материал.</p>

    <p>Сайт будет полезен школьникам и взрослым, изучающим английский для путешествий, работы и общения. Благодаря
        большому выбору упражнений, в том числе игровых, можно поддержать свой интерес и мотивацию к учёбе. Учить слова
        на английском языке онлайн на тренажере можно с любого гаджета, будь то компьютер, планшет или смартфон.
        Выученные слова можно попрактиковать в общении с носителем языка или с преподавателем, совмещая занятия в онлайн
        и оффлайн формате.</p>



</div>

</main>
</div>
</div>


