<?php ?>
<br><br><br>


<div class="vc_tta-panels-container">
    <div class="vc_tta-panels">
        <div class="vc_tta-panel vc_active" id="1475496679279-88fdd69c-4c6b" data-vc-content=".vc_tta-panel-body">
            <div class="vc_tta-panel-heading text-center"><h4 class="vc_tta-panel-title"><span
                                class="vc_tta-title-text text-center">Учить английские слова по темам на каждый день.</span>
                </h4></div>
            <div class="vc_tta-panel-body">
                <div class="row">

                    <?php
                    foreach ($play_list as  $list) { ?>
                    <div class="col-sm-4 col-xs-12">
                        <div class="courses_post__box courses_post__has_thumbnail">
                            <div class="courses_post__thumbnail"><a
                                        href="/work-out/index?id=<?php echo $list->id; ?>"><img
                                            src="<?php  if ($list->image_list == '') { echo "/static/img/3/noimage.png";  }
                                            else {  echo "/uploads/playlists/".$list->image_list."";  } ?>"
                                            width="350" height="190" alt="classes1" title="<?php echo $list->name; ?>"></a></div>
                            <div class="courses_post__body_wrap">
                                <div class="courses_post__body">
                                    <div class="courses_post__title"><a
                                                href="/work-out/index?id=<?php echo $list->id; ?>"><?php echo $list->name; ?></a></div>
                                    <div class="courses_post__short_description"><p></p></div>
                                    <div class="courses_post__meta">
                                        <ul>
                                            <li>
                                                <div>Уровень</div>
                                                Легкий
                                            </li>
                                            <li>
                                                <div>Слов</div>
                                                <?php echo $list->count_words; ?>
                                            </li>
                                            <li>
                                                <div>Цена</div>
                                                Бесплатно
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <a href="/work-out/index?id=<?php echo $list->id; ?>"
                                   class="courses_post__link_more"><span>Изучить</span></a></div>
                        </div>
                    </div>

                    <? }  ?>





                </div>
            </div>
        </div>





    </div>
</div>

