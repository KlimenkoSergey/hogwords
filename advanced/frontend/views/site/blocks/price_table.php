<div class="wpb_wrapper" style="margin-bottom: 100px;" id="price_table">
    <h3 class="vc_custom_heading" style="text-align:left;margin-bottom:44px; font-size: 30px;"> Хотите попробовать сейчаc? </h3>
    <div class="stm-pricing stm-pricing_three" id="stm-pricing-60f020b0ed3bc">
        <div class="stm-pricing__content">
            <div class="stm-pricing__side-panel">
                <div class="stm-pricing__filters">

                </div>
                <ul class="stm-pricing__features">
                    <li class="stm-pricing__feature">Изучение аниглийских слов, чтение повторение, персональный словарь</li>
                    <li class="stm-pricing__feature">Возможность проходить тесты для контроля памяти</li>
                    <li class="stm-pricing__feature">Возможность читать статьи и играть в игры</li>
                    <li class="stm-pricing__feature">Возможность смотреть видео материал</li>
                </ul>
            </div>
            <div class="stm-pricing__tables">
                <div class="stm-pricing__tables-row">
                    <div class="stm-pricing__tables-col">
                        <div class="stm-pricing-table" style="border-color:#ffaf40">
                            <div class="stm-pricing-table__title">Бесплатный период 30 дней</div>
                            <ul class="stm-pricing-table__periods">
                                <li class="stm-pricing-table__periods-item spt_month" data-period="monthly">
                                    <div class="stm-pricing-table__price" style="color:#ffaf40; font-size: 25px; display: flex; align-items: center; justify-content: center; margin-left: 10px;">100 <img src="static/img/svg/uah1.svg"></div>
                                    <div class="stm-pricing-table__period"> <br></div>
                                </li>
                                <li class="stm-pricing-table__periods-item spt_year stm-pricing-table__periods-item_active" data-period="yearly">
                                    <div>
                                        <div class="stm-pricing-table__price" style="color:#ffaf40; font-size: 25px; display: flex; align-items: center; justify-content: center; margin-left: 10px;">БЕСПЛАТНО</div>
                                        <span style="color: #ffaf40;"> </span>
                                    </div>
                                    <div class="stm-pricing-table__period"><br></div>
                                </li>
                            </ul>
                            <ul class="stm-pricing-table__features">
                                <li class="stm-pricing-table__feature">
                                    <div class="stm-pricing-table__feature-label">Изучение аниглийских слов, чьтение повторение, персональный словарь</div>
                                    <div class="stm-pricing-table__feature-value"> <i class="stm-icon stm-icon-check"></i></div>
                                </li>
                                <li class="stm-pricing-table__feature">
                                    <div class="stm-pricing-table__feature-label">Возможность проходить тесты для контроля памяти</div>
                                    <div class="stm-pricing-table__feature-value"> <i class="stm-icon stm-icon-check"></i></div>
                                </li>
                                <li class="stm-pricing-table__feature">
                                    <div class="stm-pricing-table__feature-label">Возможность читать статьи и играть в игры</div>
                                    <div class="stm-pricing-table__feature-value"> <i class="stm-icon stm-icon-check"></i></div>
                                </li>
                                <li class="stm-pricing-table__feature">
                                    <div class="stm-pricing-table__feature-label">Возможность смотреть видео материал</div>
                                    <div class="stm-pricing-table__feature-value"> <i class="stm-icon stm-icon-check"></i></div>
                                </li>
                            </ul>
                            <div class="stm-pricing-table__action"> <a href="/site/signup" style="border-color:#ffaf40" target="_self" class="stm-btn stm-btn_outline stm-btn_md stm-btn_icon-left"> Подписаться </a></div>
                        </div>
                    </div>
                    <div class="stm-pricing__tables-col">
                        <div class="stm-pricing-table" style="border-color:#ff6666">
                            <div class="stm-pricing-table__title">Премиум на месяц<br></div>
                            <ul class="stm-pricing-table__periods">
                                <li class="stm-pricing-table__periods-item spt_month" data-period="monthly">
                                    <div class="stm-pricing-table__price" style="color:#ff6666; font-size: 25px; display: flex; align-items: center; justify-content: center; margin-left: 10px;">9.99 $</div>
                                    <div class="stm-pricing-table__period"> за месяц</div>
                                </li>
                                <li class="stm-pricing-table__periods-item spt_year stm-pricing-table__periods-item_active" data-period="yearly">
                                    <div>
                                        <div class="stm-pricing-table__price" style="color:#ff6666; font-size: 25px; display: flex; align-items: center; justify-content: center; margin-left: 10px;"> 9.99$   </div>
                                        <span style="color: #ff6666;"><br></span>
                                    </div>
                                    <div class="stm-pricing-table__period">за месяц</div>
                                </li>
                            </ul>
                            <ul class="stm-pricing-table__features">
                                <li class="stm-pricing-table__feature">
                                    <div class="stm-pricing-table__feature-label">Изучение аниглийских слов, чьтение повторение, персональный словарь</div>
                                    <div class="stm-pricing-table__feature-value"> <i class="stm-icon stm-icon-check"></i></div>
                                </li>
                                <li class="stm-pricing-table__feature">
                                    <div class="stm-pricing-table__feature-label">Возможность проходить тесты для контроля памяти</div>
                                    <div class="stm-pricing-table__feature-value"> <i class="stm-icon stm-icon-check"></i></div>
                                </li>
                                <li class="stm-pricing-table__feature">
                                    <div class="stm-pricing-table__feature-label">Возможность читать статьи и играть в игры</div>
                                    <div class="stm-pricing-table__feature-value"> <i class="stm-icon stm-icon-check"></i></div>
                                </li>
                                <li class="stm-pricing-table__feature">
                                    <div class="stm-pricing-table__feature-label">Возможность смотреть видео материал</div>
                                    <div class="stm-pricing-table__feature-value"> <i class="stm-icon stm-icon-check"></i></div>
                                </li>
                            </ul>
                            <div class="stm-pricing-table__action"> <a href="/site/signup" style="border-color:#ff6666" target="_self" class="stm-btn stm-btn_outline stm-btn_md stm-btn_icon-left"> Подписаться </a></div>
                        </div>
                    </div>
                    <div class="stm-pricing__tables-col">
                        <div class="stm-pricing-table" style="border-color:#bd66ff">
                            <div class="stm-pricing-table__title">Премиум на год<br></div>
                            <ul class="stm-pricing-table__periods">
                                <li class="stm-pricing-table__periods-item spt_month" data-period="monthly">
                                    <div class="stm-pricing-table__price" style="color:#bd66ff; font-size: 25px; display: flex; align-items: center; justify-content: center; margin-left: 10px;"> 300 <img src="static/img/svg/uah3.svg"></div>
                                    <div class="stm-pricing-table__period"> за месяц</div>
                                </li>
                                <li class="stm-pricing-table__periods-item spt_year stm-pricing-table__periods-item_active" data-period="yearly">
                                    <div>
                                        <div class="stm-pricing-table__price" style="color:#bd66ff; font-size: 25px; display: flex; align-items: center; justify-content: center; margin-left: 10px;"> 72$</div>
                                        <span style="color: #bd66ff;">(Скидка 40%)</span>
                                    </div>
                                    <div class="stm-pricing-table__period"> за год</div>
                                </li>
                            </ul>
                            <ul class="stm-pricing-table__features">
                                <li class="stm-pricing-table__feature">
                                    <div class="stm-pricing-table__feature-label">Изучение аниглийских слов, чьтение повторение, персональный словарь</div>
                                    <div class="stm-pricing-table__feature-value"> <i class="stm-icon stm-icon-check"></i></div>
                                </li>
                                <li class="stm-pricing-table__feature">
                                    <div class="stm-pricing-table__feature-label">Возможность проходить тесты для контроля памяти</div>
                                    <div class="stm-pricing-table__feature-value"> <i class="stm-icon stm-icon-check"></i></div>
                                </li>
                                <li class="stm-pricing-table__feature">
                                    <div class="stm-pricing-table__feature-label">Возможность читать статьи и играть в игры</div>
                                    <div class="stm-pricing-table__feature-value"> <i class="stm-icon stm-icon-check"></i></div>
                                </li>
                                <li class="stm-pricing-table__feature">
                                    <div class="stm-pricing-table__feature-label">Возможность смотреть видео материал</div>
                                    <div class="stm-pricing-table__feature-value"> <i class="stm-icon stm-icon-check"></i></div>
                                </li>
                            </ul>
                            <div class="stm-pricing-table__action"> <a href="/site/signup" style="border-color:#bd66ff" target="_self" class="stm-btn stm-btn_outline stm-btn_md stm-btn_icon-left">Подписаться </a></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>