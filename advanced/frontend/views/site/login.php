<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Вход';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login">
    <h1><?= Html::encode($this->title) ?></h1>

    <div style="background-color: #fff9f2; padding: 20px;" ><h3> Через социальные сети</h3>
        <script src="//ulogin.ru/js/ulogin.js"></script>
        <div id="uLogin" data-ulogin="display=panel;theme=flat;fields=first_name,last_name;providers=google,facebook,vkontakte,odnoklassniki,yandex,mailru;hidden=other;redirect_uri=https%3A%2F%2Fhogwords.net%2Fulogin;mobilebuttons=0;"></div>


    </div>

    <div style="height: 100px;"></div>
    <div  style="background-color: #fff9f2; padding: 20px;">
        <h3>С помощью логин / пароль</h3>

    <p>Пожалуйста, заполните следующие поля для входа:</p>

    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>

                <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>

                <?= $form->field($model, 'password')->passwordInput() ?>

                <?= $form->field($model, 'rememberMe')->checkbox() ?>

                <div style="color:#999;margin:1em 0">
                    Если вы забыли пароль, вы можете его <?= Html::a('Восстановить', ['site/request-password-reset']) ?>.
                    <br>
                    Требуется новое письмо с подтверждением? <?= Html::a('Отправить', ['site/resend-verification-email']) ?>
                </div>

                <div class="form-group">
                    <?= Html::submitButton('Вход', ['class' => 'stm-btn stm-btn_outline stm-btn_orange stm-btn_md', 'name' => 'login-button']) ?>
                </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div></div>
</div>
