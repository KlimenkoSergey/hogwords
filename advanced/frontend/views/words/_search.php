<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\search\WordsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="words-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'sort') ?>

    <?= $form->field($model, 'word') ?>

    <?= $form->field($model, 'transcription') ?>

    <?= $form->field($model, 'video') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'translate') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
