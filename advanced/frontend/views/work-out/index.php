<?php
/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Учить слова';

//$this->params['breadcrumbs'][] = array(
//    'label' => 'Мои листы',
//    'url' => '/my-play-list/index'
//);

$this->params['breadcrumbs'][] = $get_list_name->name;

?>
<div class="progress_bar">
    <div class="vc_column-inner">
        <div class="wpb_wrapper">
            <div class="vc_progress_bar wpb_content_element vc_progress-bar-color-white vc_progress-bar_view_large">
                <small class="vc_label">Прогресс</small>
                <div class="vc_general vc_single_bar vc_progress-bar-color-bar_blue">
					<span class="vc_bar" data-percentage-value="1" data-value="1" style="width: 1%;">
						<span class="vc_bar_val">1%</span>
					</span>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="training-screen"></div>

<script type="text/javascript">
    const renderTimer = (seconds) => `
		<div class="training-timer-container">
			<div class="training-timer-image"></div>
			<div class="training-timer">
				<svg width="35px" viewBox="0 0 14 18">
					<defs>
					</defs>
					<g id="sandclock" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" sketch:type="MSPage">
						<path d="M13.0986667,16.465 L12.7226667,16.465 C12.6796667,16.031 12.5996667,15.6073333 12.4886667,15.1963333 C12.084,13.6953333 11.269,12.3646667 10.352,11.3396667 C9.52833333,10.4183333 8.623,9.74333333 7.859,9.41433333 C7.859,9.41433333 7.56766667,9.20133333 7.56766667,8.97866667 C7.56766667,8.75633333 7.859,8.54533333 7.859,8.54533333 C9.687,7.74033333 12.3786667,4.93333333 12.7223333,1.50133333 L13.0986667,1.50133333 C13.5006667,1.50133333 13.8266667,1.17533333 13.8266667,0.773666667 C13.8266667,0.371666667 13.5006667,0.0456666667 13.0986667,0.0456666667 L0.776,0.0456666667 C0.374,0.0456666667 0.048,0.371666667 0.048,0.773666667 C0.048,1.17533333 0.374,1.50133333 0.776,1.50133333 L1.152,1.50133333 C1.49233333,4.93666667 4.15866667,7.745 6.00533333,8.54733333 C6.00533333,8.54733333 6.31133333,8.737 6.31133333,8.97866667 C6.31133333,9.22033333 6.01566667,9.421 6.01566667,9.421 C5.26233333,9.75266667 4.36333333,10.4246667 3.54166667,11.3396667 C2.62066667,12.3646667 1.79833333,13.6953333 1.389,15.1963333 C1.277,15.6073333 1.196,16.031 1.15266667,16.465 L0.776,16.465 C0.374,16.465 0.048,16.791 0.048,17.1926667 C0.048,17.5946667 0.374,17.9206667 0.776,17.9206667 L13.0986667,17.9206667 C13.5006667,17.9206667 13.8266667,17.5946667 13.8266667,17.1926667 C13.8263333,16.791 13.5006667,16.465 13.0986667,16.465 L13.0986667,16.465 Z M2.47033333,16.4923333 L1.892,16.4923333 C1.92166667,16.023 1.97666667,15.5933333 2.053,15.1963333 C2.273,14.054 2.67366667,13.1896667 3.18666667,12.4753333 C3.47733333,12.0703333 3.80133333,11.6873333 4.14033333,11.3396667 C4.80733333,10.6553333 5.88879069,10.1021427 6.19133333,9.82066667 C6.49387598,9.53919067 6.65833333,9.39266667 6.65833333,8.997 C6.65833333,8.60133333 6.45611593,8.47363293 6.03570577,8.2112428 C5.61529562,7.94885266 4.034,6.69966667 3.17433333,5.49566667 C2.488,4.53433333 2.00533333,3.328 1.891,1.50166667 L11.982,1.50166667 C11.8663333,3.322 11.378,4.52866667 10.687,5.49133333 C9.82466667,6.69266667 8.52740499,7.75976575 7.89733907,8.12268078 C7.26727316,8.48559582 7.22133333,8.61 7.22133333,8.98333333 C7.22133333,9.35666667 7.41784912,9.52330154 7.89733907,9.82066691 C8.37682903,10.1180323 9.08133333,10.6486667 9.75266667,11.3393333 C10.0873333,11.6833333 10.4066667,12.0626667 10.6933333,12.4633333 C11.2053333,13.179 11.6043333,14.0463333 11.823,15.196 C11.8983333,15.5926667 11.9523333,16.0223333 11.9816667,16.492 L11.4053333,16.492 C4.14033338,16.4920002 5.86059567,16.4920002 2.47033333,16.4923333 Z" id="Shape" fill="transparent"></path>

						<g id="sand">
							<path d="M7.00695799,10.3484497 C7.00695799,10.3484497 6.27532958,10.4129639 7.00695799,10.3484497 C7.73858641,10.2839355 7.00695799,10.3484497 7.00695799,10.3484497 C7.00695799,10.3484497 7.78173827,9.99768063 7.09265135,10.548584 C6.40356444,11.0994873 7.09265137,10.548584 7.09265137,10.548584 L7.09265137,10.5493774 L11.4924319,16.4705197 L2.52148436,16.4705197 L6.87243652,10.5493774 L6.80957031,10.548584 C6.80957031,10.548584 7.1925659,10.737854 6.87243651,10.548584 C6.37234656,10.2529159 7.00695799,10.3484497 7.00695799,10.3484497 Z;" id="Path-26" fill="#C62626" sketch:type="MSShapeGroup">
								<animate
									attributeName="d"
									dur="${ seconds }s"
									repeatCount="once"
									keyTimes="0; .01; .2; .4; .7; .8; .99; 1"

								 	values="M2.33630371,3.07006836 C2.33630371,3.07006836 5.43261719,3.33813477 6.80957031,3.33813477 C8.18652344,3.33813477 11.3754883,3.07006836 11.3754883,3.07006836 C11.3754883,3.07006836 10.8122559,4.96514893 9.58630371,6.16516113 C8.36035156,7.36517334 7.09265137,8.2623291 7.09265137,8.2623291 L7.09265137,8.35028076 L7.09265137,8.46459961 L6.87243652,8.46459961 L6.87243652,8.35028076 L6.80957031,8.2623291 C6.80957031,8.2623291 4.9704053,7.27703707 3.96130371,5.96057129 C2.5045166,4.06005859 2.33630371,3.07006836 2.33630371,3.07006836 Z;


									M2.375,3.11462402 C2.375,3.11462402 5.71569824,3.44421387 7.09265137,3.44421387 C8.46960449,3.44421387 11.4150391,3.31262207 11.4150391,3.31262207 C11.4150391,3.31262207 10.8122559,4.96514893 9.58630371,6.16516113 C8.36035156,7.36517334 7.09265137,8.2623291 7.09265137,8.2623291 L7.09265137,15.5496216 L7.09265137,16.47052 L6.87243652,16.47052 L6.87243652,15.5496216 L6.80957031,8.2623291 C6.80957031,8.2623291 4.9704053,7.27703707 3.96130371,5.96057129 C2.5045166,4.06005859 2.375,3.11462402 2.375,3.11462402 Z;

									M2.49230957,3.31262207 C2.49230957,3.31262207 5.71569824,3.66851807 7.09265137,3.66851807 C8.46960449,3.66851807 11.3153076,3.53222656 11.3153076,3.53222656 C11.3153076,3.53222656 10.8122559,4.96514893 9.58630371,6.16516113 C8.36035156,7.36517334 7.09265137,8.2623291 7.09265137,8.2623291 L7.09265137,15.149231 L7.9152832,16.47052 L6.10144043,16.47052 L6.87243652,15.149231 L6.80957031,8.2623291 C6.80957031,8.2623291 4.9704053,7.27703707 3.96130371,5.96057129 C2.5045166,4.06005859 2.49230957,3.31262207 2.49230957,3.31262207 Z;

									M2.98474121,4.37164307 C2.98474121,4.37164307 5.49548338,4.7074585 6.87243651,4.7074585 C8.24938963,4.7074585 10.8119509,4.64428711 10.8119509,4.64428711 C10.8119509,4.64428711 10.8122559,4.96514893 9.58630371,6.16516113 C8.36035156,7.36517334 7.09265137,8.2623291 7.09265137,8.2623291 L7.09265137,12.5493774 L9.36248779,16.47052 L4.5581665,16.47052 L6.87243652,12.5493774 L6.80957031,8.2623291 C6.80957031,8.2623291 4.9704053,7.27703707 3.96130371,5.96057129 C2.5045166,4.06005859 2.98474121,4.37164307 2.98474121,4.37164307 Z;

								 	M4.49743651,6.36560059 C4.49743651,6.36560059 5.63000487,6.72412109 7.00695799,6.72412109 C8.38391112,6.72412109 9.56188963,6.36560059 9.56188963,6.36560059 C9.56188963,6.36560059 9.48870848,6.54571533 8.79962157,7.09661865 C8.11053465,7.64752197 7.09265137,8.2623291 7.09265137,8.2623291 L7.09265137,10.5493774 L11.4924319,16.4705197 L2.52148436,16.4705197 L6.87243652,10.5493774 L6.80957031,8.2623291 C6.80957031,8.2623291 6.01727463,8.16043491 4.82800292,6.81622307 C4.42932128,6.36560059 4.49743651,6.36560059 4.49743651,6.36560059 Z;

								 	M5.87017821,7.51904297 C5.87017821,7.51904297 6.14080809,7.70904542 6.87243651,7.64453126 C7.60406493,7.5800171 7.47180174,7.51904297 7.47180174,7.51904297 C7.47180174,7.51904297 8.51336669,7.23876953 7.82427977,7.78967285 C7.13519286,8.34057617 7.09265137,8.2623291 7.09265137,8.2623291 L7.09265137,10.5493774 L11.4924319,16.4705197 L2.52148436,16.4705197 L6.87243652,10.5493774 L6.80957031,8.2623291 C6.80957031,8.2623291 6.66632079,8.14239502 6.34619139,7.953125 C5.84610144,7.65745695 5.87017821,7.51904297 5.87017821,7.51904297 Z;

								 	M7.00695799,8.06219482 C7.00695799,8.06219482 6.27532958,8.12670898 7.00695799,8.06219482 C7.73858641,7.99768066 7.00695799,8.06219482 7.00695799,8.06219482 C7.00695799,8.06219482 7.78173827,7.71142576 7.09265135,8.26232908 C6.40356444,8.8132324 7.09265137,8.2623291 7.09265137,8.2623291 L7.09265137,10.5493774 L11.4924319,16.4705197 L2.52148436,16.4705197 L6.87243652,10.5493774 L6.80957031,8.2623291 C6.80957031,8.2623291 7.1925659,8.45159912 6.87243651,8.2623291 C6.37234656,7.96666105 7.00695799,8.06219482 7.00695799,8.06219482 Z;

								 	M7.00695799,10.3484497 C7.00695799,10.3484497 6.27532958,10.4129639 7.00695799,10.3484497 C7.73858641,10.2839355 7.00695799,10.3484497 7.00695799,10.3484497 C7.00695799,10.3484497 7.78173827,9.99768063 7.09265135,10.548584 C6.40356444,11.0994873 7.09265137,10.548584 7.09265137,10.548584 L7.09265137,10.5493774 L11.4924319,16.4705197 L2.52148436,16.4705197 L6.87243652,10.5493774 L6.80957031,10.548584 C6.80957031,10.548584 7.1925659,10.737854 6.87243651,10.548584 C6.37234656,10.2529159 7.00695799,10.3484497 7.00695799,10.3484497 Z;" />

									<animate
										attributeName="fill"
										dur="${ seconds }s"
										repeatCount="once"
										keyTimes="0; .5; 1"
										values="#fea633; #fea633; #C62626;"
									/>
							</path>
						</g>
					</g>
				</svg>
			</div>
			<span class="training-timer-seconds">${ seconds }</span>
		</div>
	`

    const renderTrainingType1 = ({ word, transcription, audio = null, irregularVerbs = null, translate = null, translate_many = null, video = null}) => `
		<div class="training-container training-type-1">
			<h3 class="training-title">
				<span class="training-word">${ word }</span>
				${
        transcription ?
            `<span class="training-transcription">[${ transcription }]</span>`
            : ''
    }
				${
        audio ?
            `<span class="fa fa-volume-up training-btn-play-audio" data-src="${ audio }" title="Проиграть"></span>`
            : ''
    }
				${ irregularVerbs ? `<p class="training-sub-title">Неправильный глагол</p>` : '' }
			</h3>
			${
        irregularVerbs ?
            `<div class="training-irregular-verbs">
						<div class="training-flex">
							<div class="training-col">
								<p class="training-col-title">Infinitive</p>
							</div>
							<div class="training-col">
								<p class="training-col-title">Past Simple</p>
							</div>
							<div class="training-col">
								<p class="training-col-title">Past participle</p>
							</div>
							<div class="training-col">
								<p class="training-col-title">Перевод</p>
							</div>
						</div>
						<div class="training-flex">
							<div class="training-col">
								<p class="training-irregular-verbs-item training-irregular-verbs-infinitive">${ irregularVerbs.infinitive }</p>
							</div>
							<div class="training-col">
								<p class="training-irregular-verbs-item training-irregular-verbs-past-simple">${ irregularVerbs.past_simple_active }</p>
							</div>
							<div class="training-col">
								<p class="training-irregular-verbs-item training-irregular-verbs-past-participle">${ irregularVerbs.past_participle }</p>
							</div>
							<div class="training-col">
								<p class="training-irregular-verbs-item training-irregular-verbs-translate">${ irregularVerbs.translation }</p>
							</div>
						</div>
					</div>`
            : ''
    }
			${
        video ?
            `<div class="training-video-container">
						<div id="training-video" class="training-video">
						</div>
					</div>`
            : ''
    }
			<div class="training-translates-and-examples training-flex">
				<div class="training-col">
					${
        translate_many ?
            `<ul class="training-translates">
								${ translate_many.map((e) => `<li><div class="training-translates-item" data-word_id="${ e.id }">${ e.translate }</div><span class="training-parts-of-speech">${ e.partsOfSpeech }</span></li>`).join('') }
							</ul>`
            : translate ?
            `<ul class="training-translates">
										<li><div class="training-translates-item">${ translate }</div></li>
									</ul>`
            : ''
    }
				</div>
				<div class="training-col">
					${
        translate_many ?
            `<div class="training-examples-table ${ translate_many.length > 1 ? 'training-examples-table_more' : '' }">
								<div class="training-flex">
									<div class="training-col">
										<p class="training-examples-title training-col-title">Английский</p>
									</div>
									<div class="training-col">
										<p class="training-examples-title training-col-title">Русский</p>
									</div>
								</div>
							${
                translate_many.map((e, i) => `
									<div class="training-examples" data-for="${e.id}">
										${
                    e.examples ?
                        e.examples.map(example => `
													<div class="training-examples-item">
														<div class="training-flex">
															<div class="training-col">
																<div class="training-examples-en">
																	${ example.en_text }
																</div>
															</div>
															<div class="training-col">
																<div class="training-examples-ru">
																	${ example.ru_text }
																</div>
															</div>
														</div>
													</div>
												`).join('')
                        : ''
                }
									</div>
								`).join('')
            }
							${
                translate_many.length > 1 ?
                    `<div class="training-examples-controls">
										<div class="fa fa-chevron-up training-examples-controls-item training-examples-hide"></div>
										<span class="fa fa-chevron-down training-examples-controls-item training-examples-show"></span>
									</div>`
                    : ''
            }
							</div>`
            : ''
    }
				</div>
			</div>
		</div>
	`;

    const renderTrainingType = ({ type, params, audio, right_answer_id, timer = null }) => `
		<div class="training-container training-type-2 training-type-3">
			${ timer ? timer : '' }
			<h3 class="training-title">
				${
        type !== 4 ?
            `<span>${ params.question }</span>`
            : ''
    }
				${
        audio ?
            `<span class="fa fa-volume-up training-btn-play-audio" ${ type === 3 ? 'style="display: none;"' : '' } data-src="${ audio }" title="Проиграть"></span>`
            : ''
    }
			</h3>
			<p>Выбери правильный вариант</p>
			<ol class="training-choices training-flex">
				${
        params.variants.map((variant, i) => `
						<li class="training-choices-item" data-variant="${ variant }" data-right_answer_id="${ right_answer_id }" data-answer="${ params.answer }">
							<span class="training-choices-index">${ i + 1 }.</span>
							<p class="training-choices-value">${ variant }</p>
						</li>
					`).join('')
    }
			</ol>
		</div>
	`;

    const renderLetter = (letter) => `<span class="training-letters-item" data-letter="${ letter }">${ letter }</span>`;

    const renderTrainingType5 = ({ params, audio, right_answer_id, timer = null }) => `
		<div class="training-container training-type-5">
			${ timer ? timer : '' }
			<h3 class="training-title">
				<span>${ params.question }</span>
				${
        audio ?
            `<span class="fa fa-volume-up training-btn-play-audio" style="display: none;" data-src="${ audio }" title="Проиграть"></span>`
            : ''
    }
			</h3>
			<p>Расставь буквы в правильном порядке</p>
			<div class="training-letters-area" data-right_answer_id="${ right_answer_id }" data-answer="${ params.answer }"></div>
			<div class="training-letters">
				${
        params.answer.split('').sort(() => { return 0.5 - Math.random() }).map(letter => { return renderLetter(letter) }).join('')
    }
			</div>
			<div class="training-letters-controls">
				<a href="#" class="training-letters-controls-item training-letters-hide">Скрыть буквы</a>
				<a href="#" class="training-letters-controls-item training-letters-show">Показать буквы</a>
			</div>
		</div>
	`;

    const renderTrainingType6 = ({ message }) => `
		<div class="training-container training-type-6">
			<h3 class="training-finish">${ message }</h3>
		</div>
	`
</script>
<!--
	Минималистичный таймер
	<div class="training-timer">
		<svg width="50px" viewBox="0 0 73 88" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
			<g id="hourglass">
				<path d="M63.8761664,86 C63.9491436,84.74063 64,83.4707791 64,82.1818182 C64,65.2090455 57.5148507,50.6237818 48.20041,44 C57.5148507,37.3762182 64,22.7909545 64,5.81818182 C64,4.52922091 63.9491436,3.25937 63.8761664,2 L10.1238336,2 C10.0508564,3.25937 10,4.52922091 10,5.81818182 C10,22.7909545 16.4851493,37.3762182 25.79959,44 C16.4851493,50.6237818 10,65.2090455 10,82.1818182 C10,83.4707791 10.0508564,84.74063 10.1238336,86 L63.8761664,86 Z" id="glass" fill="#ECF1F6"></path>
				<rect id="top-plate" fill="#4D4544" x="0" y="0" width="74" height="8" rx="2"></rect>
				<rect id="bottom-plate" fill="#4D4544" x="0" y="80" width="74" height="8" rx="2"></rect>

				<g id="top-sand" transform="translate(18, 21)">
					<clipPath id="top-clip-path" fill="white">
						<rect x="0" y="0" width="38" height="21"></rect>
					</clipPath>

					<path fill="#FEA633" clip-path="url(#top-clip-path)" d="M38,0 C36.218769,7.51704545 24.818769,21 19,21 C13.418769,21 1.9,7.63636364 0,0 L38,0 Z"></path>
				</g>

				<g id="bottom-sand" transform="translate(18, 55)">
					<clipPath id="bottom-clip-path" fill="white">
						<rect x="0" y="0" width="38" height="21"></rect>
					</clipPath>

					<g clip-path="url(#bottom-clip-path)">
						<path fill="#FEA633" d="M0,21 L38,21 C36.1,13.3636364 24.581231,0 19,0 C13.181231,0 1.781231,13.4829545 0,21 Z"></path>
					</g>
				</g>
			</g>
		</svg>
</div>
-->

<!-- <div class="training-container training-type-5">
	<h3 class="training-title">
		<span>Страна</span>
		<span class="fa fa-volume-up training-btn-play-audio" title="Проиграть"></span>
	</h3>
	<p>Расставь буквы в правильном порядке</p>
	<div class="training-letters-area" data-answer="country"></div>
	<div class="training-letters">
		<span class="training-letters-item" data-letter="c">c</span>
		<span class="training-letters-item" data-letter="o">o</span>
		<span class="training-letters-item" data-letter="u">u</span>
		<span class="training-letters-item" data-letter="n">n</span>
		<span class="training-letters-item" data-letter="t">t</span>
		<span class="training-letters-item" data-letter="r">r</span>
		<span class="training-letters-item" data-letter="y">y</span>
	</div>
</div> -->

<div class="training-controls">
    <button class="stm-btn stm-btn_outline stm-btn_orange stm-btn_md stm-btn_icon-left training-controls-item" data-api-method="prev">
        <span class="fa fa-chevron-left"></span>
        <span>Назад</span>
    </button>
    <button class="stm-btn stm-btn_outline stm-btn_orange stm-btn_md stm-btn_icon-left training-controls-item" data-api-method="next">
        <span>Вперед</span>
        <span class="fa fa-chevron-right"></span>
    </button>
<!--    <button class="stm-btn stm-btn_outline stm-btn_orange stm-btn_md stm-btn_icon-left training-controls-item" data-api-method="mix-and-start">-->
<!--        <span class="fa fa-random"></span>-->
<!--        <span>Перемешать список и начать сначала</span>-->
<!--    </button>-->
    <button class="stm-btn stm-btn_outline stm-btn_orange stm-btn_md stm-btn_icon-left training-controls-item" data-api-method="beginning">
        <span class="fa fa-backward"></span>
        <span>В начало</span>
    </button>
<!--    <button class="stm-btn stm-btn_outline stm-btn_orange stm-btn_md stm-btn_icon-left training-controls-item" data-api-method="delete">-->
<!--        <span class="fa fa-trash-alt"></span>-->
<!--        <span>Удалить слово из списка</span>-->
<!--    </button>-->
    <button class="stm-btn stm-btn_outline stm-btn_orange stm-btn_md stm-btn_icon-left training-btn-options">
        <span class="fa fa-cogs"></span>
        <span>Настройки</span>
    </button>
</div>


<div class="training-options">
    <div class="training-examples-table" style="margin-top: 30px;">
        <h3>Настройки:</h3>
        <?php
        $form = ActiveForm::begin([
            'id' => 'options-form',
        ]);
        ?>

        <p>Упражнения:</p>
        <?= $form->field($model, 'type_exercises_1')->checkbox() ?>
        <?= $form->field($model, 'type_exercises_2')->checkbox() ?>
        <?= $form->field($model, 'type_exercises_3')->checkbox() ?>
        <?= $form->field($model, 'type_exercises_4')->checkbox() ?>
        <?= $form->field($model, 'type_exercises_5')->checkbox() ?>

        <?= $form->field($model, 'autoPlayAudio')->checkbox() ?>
        <!--    --><?//= $form->field($model, 'autoPlayVideo')->checkbox() ?>
        <?= $form->field($model, 'portionsOfWords')->textInput() ?>
        <div class="form-group">
            <?= Html::submitButton('Сохранить', ['class' => 'stm-btn stm-btn_outline stm-btn_orange stm-btn_md']) ?>
        </div>
        <?php ActiveForm::end() ?>
    </div>
</div>

<br><br><br>
<div class="text-center"><a href="/site/signup" class="stm-btn stm-btn_outline stm-btn_orange stm-btn_md stm-btn_icon-left">Создай свой список слов (12500 слов)</a></div>

<script type="text/javascript" src="/static/js/training-player.js"></script>