<script>

    $(document).ready(function () {
        function empty_blocks() {

            $("#word,#transcription,#translate,#translation_options,#examples,#definitions").empty()
            // alert('reload cart called');
        }

        function countprogressbar() {

            $.ajax({
                type: 'POST',
                url: '/play-words/count-progress-bar',
                data: {id: '<?php echo $list_id; ?>'},
                success: function (data) {
                    // const parseData = JSON.parse(data);
                    $(".vc_bar_val").html(data + "%");
                    $('.vc_bar').attr('data-percentage-value', data);
                    $('.vc_bar').attr('style', "min-width: 3em; width: " + data + "%;");
                    $('.vc_bar').attr('data-value', data);


                }
            });

        }

        countprogressbar();


        function fill_blocks(respond_my) {
            countprogressbar();


            $("#word").html(respond_my[1]);

            if (respond_my[2] != null) {
                $("#transcription").css("display", "block");
                $("#transcription").html('[' + respond_my[2] + ']');
            } else {
                $("#transcription").css("display", "none");
            }


            $("#translate").html(respond_my[5]);

            if (respond_my[12] != "") {
                $("#trans_wrap").css("display", "block");
                $("#translation_options").html(respond_my[12]);
            } else {
                $("#trans_wrap").css("display", "none");
            }

            if (respond_my[13] != "") {
                $("#ex_wrap").css("display", "block");
                $("#examples").html(respond_my[13]);
            } else {
                $("#ex_wrap").css("display", "none");
            }

            if (respond_my[14] != "") {
                $("#def_wrap").css("display", "block");
                $("#definitions").html(respond_my[14]);
            } else {
                $("#def_wrap").css("display", "none");
            }

            // $("#examples").html(respond_my[13]);
            // $("#definitions").html(respond_my[14]);


            // console.log(respond_my);

        }

        function return_ajax(respond_my) {
            empty_blocks();
            fill_blocks(respond_my);


        }


        $("#next").click(function () {
            // $("span#itogo").empty();
            //     var param = $('meta[name="csrf-param"]').attr("content");
            //     var token = $('meta[name="csrf-token"]').attr("content");

            $.ajax({
                type: 'POST',
                url: '/play-words/next-word',
                dataType: 'json',
                data: {id: '<?php echo $list_id; ?>'},
                success: function (data) {
                    // const parseData = JSON.parse(data);
                    return_ajax(data);


                }
            });
        })


        $("#prev").click(function () {
            // $("span#itogo").empty();

            $.ajax({
                type: 'POST',
                url: '/play-words/prev-word',
                data: {id: '<?php echo $list_id; ?>'},
                success: function (data) {
                    // const parseData = JSON.parse(data);
                    return_ajax(data);


                }
            });
        })


        $("#tothebegin").click(function () {
            // $("span#itogo").empty();

            $.ajax({
                type: 'POST',
                url: '/play-words/to-the-begining',
                data: {id: '<?php echo $list_id; ?>'},
                success: function (data) {
                    // const parseData = JSON.parse(data);
                    return_ajax(data);


                }
            });
        })


        $("#learnanddel").click(function () {
            // $("span#itogo").empty();

            $.ajax({
                type: 'POST',
                url: '/play-words/learn-and-del',
                data: {id: '<?php echo $list_id; ?>'},
                success: function (data) {
                    // const parseData = JSON.parse(data);
                    return_ajax(data);


                }
            });
        })


        $("#mixandstart").click(function () {
            // $("span#itogo").empty();

            $.ajax({
                type: 'POST',
                url: '/play-words/mix-and-start',
                data: {id: '<?php echo $list_id; ?>'},
                success: function (data) {
                    // const parseData = JSON.parse(data);
                    return_ajax(data);


                }
            });
        })


    });


</script>
