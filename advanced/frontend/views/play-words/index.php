<?php

/* @var $this yii\web\View */

$this->title = 'Учить слова';

$this->params['breadcrumbs'][] = array(
    'label' => 'Мои листы',
    'url' => '/my-play-list/index'
);

$this->params['breadcrumbs'][] = $get_list_name->name;

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$list_id = $_GET["list"];
$list_id = (int)$list_id;


?>


<script>



    $(document).ready(function () {
        function empty_blocks() {

            $("#word,#transcription,#translate").empty();
            $("#1456032341159-f2e644fc-117b,#1456032341266-f4dc5125-bc0c,#1456032341368-b0effc54-48c0").css("display", "none");
            // alert('reload cart called');
        }

        function countprogressbar() {

            $.ajax({
                type: 'POST',
                url: '/play-words/count-progress-bar',
                data: {id: '<?php echo $list_id; ?>'},
                success: function (data) {
                    // const parseData = JSON.parse(data);
                    $(".vc_bar_val").html(data + "%");
                    $('.vc_bar').attr('data-percentage-value', data);
                    $('.vc_bar').attr('style', "min-width: 3em; width: " + data + "%;");
                    $('.vc_bar').attr('data-value', data);


                }
            });

        }

        countprogressbar();


        function fill_blocks(respond_my) {
            countprogressbar();
            $("#pathmp3").html('/uploads/audio_robo/' + respond_my[15] + '.mp3');


            $("#video").html('<video src="/uploads/audio/' + respond_my[15] + '.mp4" width="640px" height="380px" controls ></video>');


            $("#word").append(respond_my[1]);

            if (respond_my[2] != null) {
                $("#transcription").css("display", "block");
                $("#transcription").html('[' + respond_my[2] + ']');
            } else {
                $("#transcription").css("display", "none");
            }


            $("#translate").html(respond_my[5]);

            if (respond_my[12] != "") {
                $("#1456032341159-f2e644fc-117b").css("display", "block");
                $("#translation_options").html(respond_my[12]);
            } else {
                $("#1456032341159-f2e644fc-117b").css("display", "none");
            }

            if (respond_my[13] != "") {
                $("#1456032341266-f4dc5125-bc0c").css("display", "block");
                $("#examples").html(respond_my[13]);
            } else {
                $("#1456032341266-f4dc5125-bc0c").css("display", "none");
            }

            if (respond_my[14] != "") {
                $("#1456032341368-b0effc54-48c0").css("display", "block");
                $("#definitions").html(respond_my[14]);
            } else {
                $("#1456032341368-b0effc54-48c0").css("display", "none");
            }

            // $("#examples").html(respond_my[13]);
            // $("#definitions").html(respond_my[14]);


            // console.log(respond_my);

        }

        function return_ajax(respond_my) {
            empty_blocks();
            fill_blocks(respond_my);


        }


        $("#next").click(function () {
            // $("span#itogo").empty();
            //     var param = $('meta[name="csrf-param"]').attr("content");
            //     var token = $('meta[name="csrf-token"]').attr("content");

            $.ajax({
                type: 'POST',
                url: '/play-words/next-word',
                dataType: 'json',
                data: {id: '<?php echo $list_id; ?>'},
                success: function (data) {
                    // const parseData = JSON.parse(data);
                    return_ajax(data);


                }
            });
        })


        $("#prev").click(function () {
            // $("span#itogo").empty();

            $.ajax({
                type: 'POST',
                url: '/play-words/prev-word',
                data: {id: '<?php echo $list_id; ?>'},
                success: function (data) {
                    // const parseData = JSON.parse(data);
                    return_ajax(data);


                }
            });
        })


        $("#tothebegin").click(function () {
            // $("span#itogo").empty();

            $.ajax({
                type: 'POST',
                url: '/play-words/to-the-begining',
                data: {id: '<?php echo $list_id; ?>'},
                success: function (data) {
                    // const parseData = JSON.parse(data);
                    return_ajax(data);


                }
            });
        })


        $("#learnanddel").click(function () {
            // $("span#itogo").empty();

            $.ajax({
                type: 'POST',
                url: '/play-words/learn-and-del',
                data: {id: '<?php echo $list_id; ?>'},
                success: function (data) {
                    // const parseData = JSON.parse(data);
                    return_ajax(data);


                }
            });
        })


        $("#mixandstart").click(function () {
            // $("span#itogo").empty();

            $.ajax({
                type: 'POST',
                url: '/play-words/mix-and-start',
                data: {id: '<?php echo $list_id; ?>'},
                success: function (data) {
                    // const parseData = JSON.parse(data);
                    return_ajax(data);


                }
            });
        })


    });


</script>


<div class="site-index" id="learn_area">


    <div class="vc_column-inner">
        <div class="wpb_wrapper">
            <div class="vc_progress_bar wpb_content_element vc_progress-bar-color-white vc_progress-bar_view_large">
                <small class="vc_label">Прогресс</small>
                <div class="vc_general vc_single_bar vc_progress-bar-color-bar_blue"><span class="vc_bar"
                                                                                           data-percentage-value="87"
                                                                                           data-value="87"
                                                                                           style="width: 87%;"><span
                                class="vc_bar_val">87%</span></span></div>


            </div>
        </div>
    </div>

    <script>
        $(document).ready(function() {

            $('#play').click(function() {

                var voice = $('#pathmp3').html();

                var audio = new Audio(voice);
                audio.play();



            });


        });


    </script>
<span id="pathmp3" style="display: none">/uploads/audio_robo/<?php echo $select_word->id; ?>.mp3</span>


    <div>
        <div class="row">
            <div class="col-sm-3 text-center" style="font-size: 36px;" >
                <div style="font-size: 12px; color: darkgrey;">АНГЛИЙСКИЙ</div>
                <div id="word" style="display: inline;" ><?php echo $select_word->word; ?></div>
                <div style="display: inline;"><a href="#learn_area" id="play" title="Проиграть" ><img src="/static/img/gromko.png" width="50" alt="Проиграть." style="display: inline;"></a></div>



                    <br><br>

                <?php if ($select_word->transcription != "") { ?>
                <div style="font-size: 12px; color: darkgrey;">ТРАНСКРИПЦИЯ</div>
                <div id="transcription">[<?php echo $select_word->transcription; ?>] <br><br></div>
                <?php } ?>


                <div style="font-size: 12px; color: darkgrey;">РУССКИЙ</div>
                <div id="translate"><?php echo $select_word->translate; ?></div>




                <br><br><br>

            </div>
            <div class="col-sm-9">





                <div id="video">
                    <video src="/uploads/audio/<?php echo $select_word->id; ?>.mp4" width="100%"     controls ></video>
                    <br><br><br>

                </div></div>


        </div>


        <div  class="">

            <div class="row">
                <div class="col-sm-4"> <a href="#learn_area" target="_self" class="stm-btn stm-btn_outline stm-btn_orange stm-btn_md" id="tothebegin" style="width: 200px;">В
                        начало</a><br><br></div>
                <div class="col-sm-4"><a href="#learn_area" target="_self" class="stm-btn stm-btn_outline stm-btn_orange stm-btn_md" id="prev" style="width: 200px;">Назад</a><br><br></div>
                <div class="col-sm-4"><a href="#learn_area" target="_self" class="stm-btn stm-btn_outline stm-btn_orange stm-btn_md" id="next" style="width: 200px;">Далее</a><br><br></div>


            </div>

            <div class="row">
                <div class="col-sm-6"> <a href="#learn_area" target="_self" class="stm-btn stm-btn_outline stm-btn_orange stm-btn_md" id="mixandstart"  >Перемешать
                        слова и начать сначала</a> <br><br></div>
                <div class="col-sm-6"> <a href="#learn_area" target="_self" class="stm-btn stm-btn_outline stm-btn_orange stm-btn_md" id="learnanddel"  >Выучено,
                        удалить из списка</a></div>
                </div>










            <br><br><br>




            <div style="height: 40px;"></div>

        </div>



        <div class="text-center button_pleer">

            <span class="glyphicon glyphicon-backward"></span> <span class="glyphicon glyphicon-play"></span> <span
                    class="glyphicon glyphicon-forward"></span>
        </div>


        <div class="vc_column-inner">
            <div class="wpb_wrapper">
                <div class="vc_tta-container" data-vc-action="collapse">
                    <div class="vc_general vc_tta vc_tta-accordion vc_tta-o-shape-group vc_tta-gap-4 vc_custom_1456037177307 vc_tta-accordion_style_outline vc_tta-accordion_color_yellow">
                        <div class="vc_tta-panels-container">
                            <div class="vc_tta-panels">
                                <?php if ( $select_word->translation_options_clean != "" ) { ?>

                                <div class="vc_tta-panel vc_active" id="1456032341159-f2e644fc-117b"
                                     data-vc-content=".vc_tta-panel-body">
                                    <div class="vc_tta-panel-heading"><h4
                                                class="vc_tta-panel-title vc_tta-controls-icon-position-right"><a
                                                    href="#1456032341159-f2e644fc-117b" data-vc-accordion=""
                                                    data-vc-container=".vc_tta-container"><span
                                                        class="vc_tta-title-text">Варианты перевода</span><i
                                                        class="vc_tta-controls-icon vc_tta-controls-icon-plus"></i></a>
                                        </h4></div>
                                    <div class="vc_tta-panel-body" style="">
                                        <div class="wpb_text_column wpb_content_element">
                                            <div class="wpb_wrapper"><p><span style="letter-spacing: -0.35px;" id="translation_options"><?php echo $select_word->translation_options_clean; ?></span>
                                                </p></div>
                                        </div>
                                    </div>
                                </div>

                                <?php } ?>

                                <?php if ( $select_word->examples_clean != "" ) { ?>

                                <div class="vc_tta-panel" id="1456032341266-f4dc5125-bc0c"
                                     data-vc-content=".vc_tta-panel-body">
                                    <div class="vc_tta-panel-heading"><h4
                                                class="vc_tta-panel-title vc_tta-controls-icon-position-right"><a
                                                    href="#1456032341266-f4dc5125-bc0c" data-vc-accordion=""
                                                    data-vc-container=".vc_tta-container"><span
                                                        class="vc_tta-title-text">Примеры</span><i
                                                        class="vc_tta-controls-icon vc_tta-controls-icon-plus"></i></a>
                                        </h4></div>
                                    <div class="vc_tta-panel-body" style="height: 0px;">
                                        <div class="wpb_text_column wpb_content_element">
                                            <div class="wpb_wrapper"><p><span style="letter-spacing: -0.35px;" id="examples"><?php echo $select_word->examples_clean; ?></span>
                                                </p></div>
                                        </div>
                                    </div>
                                </div>

                                <?php } ?>

                                <?php if ( $select_word->definitions_clean != "" ) { ?>

                                <div class="vc_tta-panel" id="1456032341368-b0effc54-48c0"
                                     data-vc-content=".vc_tta-panel-body">
                                    <div class="vc_tta-panel-heading"><h4
                                                class="vc_tta-panel-title vc_tta-controls-icon-position-right"><a
                                                    href="#1456032341368-b0effc54-48c0" data-vc-accordion=""
                                                    data-vc-container=".vc_tta-container"><span
                                                        class="vc_tta-title-text">Определения</span><i
                                                        class="vc_tta-controls-icon vc_tta-controls-icon-plus"></i></a>
                                        </h4></div>
                                    <div class="vc_tta-panel-body" style="height: 0px;">
                                        <div class="wpb_text_column wpb_content_element">
                                            <div class="wpb_wrapper"><p><span style="letter-spacing: -0.35px;" id="definitions"><?php echo $select_word->definitions_clean; ?></span>
                                                </p></div>
                                        </div>
                                    </div>
                                </div>

                                <?php } ?>





                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <!--        <hr>-->
        <!--        <div class="pleer_setup">-->
        <!--            Настройки плеера слов<br>-->
        <!---->
        <!--            <input type="checkbox" name="option2" value="a2">Автовоспроизведение<br>-->
        <!--            <input type="checkbox" name="option3" value="a3">Проигрывать видео к словам<br>-->
        <!--            <input type="checkbox" name="option3" value="a3">Озвучка слов роботом<br>-->
        <!--            <input type="checkbox" name="option4" value="a4">Набор слова с клавиатуры<br>-->
        <!--            <input type="checkbox" name="option4" value="a4">Выбрать правильный ответ<br>-->
        <!--            <input type="checkbox" name="option4" value="a4">Набор с клавиатуры на слух<br>-->
        <!--            Пауза между словами в секундах <input type="text" value="10">-->
        <!---->
        <!---->
        <!--        </div>-->


        <!---->
        <!--        <div style="font-size: 24px;">-->
        <!--            Советы по изучению<br>-->
        <!---->
        <!---->
        <!--        </div>-->



    </div>
