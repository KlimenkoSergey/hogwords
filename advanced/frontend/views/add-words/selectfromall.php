<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\LinkPager;


$this->title = "Выбрать из всех по популярности";
$this->params['breadcrumbs'][] = $this->title;
//$this->registerMetaTag(['name' => 'description', 'content' => 'мой супер пупер текст']);
//print_r($words);
$jquery_srting = "";
foreach ($words as $key => $value) {
    $jquery_srting .= "#word" . $value->id . ",";

}
$jquery_srting = substr($jquery_srting, 0, -1);


$jquery_srting2 = "";
foreach ($words as $key => $value) {
    $jquery_srting2 .= "#translate_" . $value->id . ",";

}
$jquery_srting2 = substr($jquery_srting2, 0, -1);





?>

<script>

    $(document).ready(function () {
        $("<? echo $jquery_srting; ?>").change(function () {

            console.log($(this).val());
            // console.log('Привет от JavaScript!');

            // $("span#itogo").empty();

            $.ajax({
                type: 'POST',
                url: '/add-words/add-word-in-list',
                data: {id: $(this).val()},
                success: function (html) {
                    // $("#itogo").append(html);
                }
            });
        })
        //


        $("#add_all").click(function () {
            $("input[type=checkbox]").prop('checked', true);

            var checkboxes = [];
            $('input:checkbox:checked').each(function () {
                //добавляем значение каждого флажка в этот массив
                checkboxes.push(this.value);
            });
            console.log(checkboxes.join(','));
            // alert(checkboxes.join(','))

            $.ajax({
                type: 'POST',
                url: '/add-words/check-all',
                data: {id: checkboxes.join('-')},
                success: function (html) {
                    // $("#itogo").append(html);
                }
            });


        });

        //

        $("#unchek_all").click(function () {
            $("input[type=checkbox]").prop('checked', false);

            var checkboxes = [];
            $('input:checkbox').each(function () {
                //добавляем значение каждого флажка в этот массив
                checkboxes.push(this.value);
            });
            console.log(checkboxes.join(','));
            // alert(checkboxes.join(','))

            $.ajax({
                type: 'POST',
                url: '/add-words/uncheck-all',
                data: {id: checkboxes.join('-')},
                success: function (html) {
                    // $("#itogo").append(html);
                }
            });


        });

        // function view_translates(data) {
        //
        //     $('<li role="presentation" class="active"><a href="#">Home</a></li>').appendTo( target )
        //
        // }


        $("<? echo $jquery_srting2; ?>").click(function () {
            $('.popup-fade').fadeIn();


            var clickId = $(this).attr('data');
			var i;
			var j;
			var h;
            var h_two;
			
			var d_block;

			$(".trns-tabs").empty();
			$(".trns-for span").empty();
            $.ajax({
                type: 'POST',
                url: '/add-words/get-translate',
                data: {id: clickId },
                success: function (data) {
                    // $("#itogo").append(html);
					$(".trns-for span").append(data.name);
					for(i=1; i <= Object.keys(data.params).length; i++) {
						j=i*10;
						var cls;

                        console.log(data.params[j]);

						if(data.params[j]["Прил."]) {
							if(Object.keys(data.params[j]) == "Прил.") {
								cls = "trns-pril";
								
								if(j == 10) {
									d_block = 'style="display: block;"';
								} else {
									d_block = 'style="display: none"';
								}

								for(h = 1; h <= Object.keys(data.params[j]["Прил."]).length; h++) {
                                    h_two = h*10;

									$('<div class="trns-pril-block trnsbl" '+d_block+'><p><input type="checkbox" value="'+data.params[j]["Прил."][h_two]["id"]+'" '+(data.params[j]["Прил."][h_two]['check'] ? 'checked' : '')+' name="word" class="word trns-word-inp" id="word_'+data.params[j]["Прил."][h_two]["id"]+'"><label for="word_'+data.params[j]["Прил."][h_two]["id"]+'">'+data.params[j]["Прил."][h_two]["value"]+'</label></p></div>').appendTo(".trns-cont");
								}
							}
						}
                        if(data.params[j]["Нареч."]) {
                            if(Object.keys(data.params[j]) == "Нареч.") {
                                cls = "trns-narech";
								
								if(j == 10) {
									d_block = 'style="display: block"';
								} else {
									d_block = 'style="display: none"';
								}

                                for(h = 1; h <= Object.keys(data.params[j]["Нареч."]).length; h++) {
                                    h_two = h*10;

                                    $('<div class="trns-narech-block trnsbl" '+d_block+'><p><input type="checkbox" value="'+data.params[j]["Нареч."][h_two]["id"]+'" '+(data.params[j]["Нареч."][h_two]['check'] ? 'checked' : '')+' name="word" class="word trns-word-inp" id="word_'+data.params[j]["Нареч."][h_two]["id"]+'"><label for="word_'+data.params[j]["Нареч."][h_two]["id"]+'">'+data.params[j]["Нареч."][h_two]["value"]+'</label></p></div>').appendTo(".trns-cont");
                                }
                            }
                        }
                        if(data.params[j]["Сущ."]) {
                            if(Object.keys(data.params[j]) == "Сущ.") {
                                cls = "trns-susch";
								
								if(j == 10) {
									d_block = 'style="display: block"';
								} else {
									d_block = 'style="display: none"';
								}

                                for(h = 1; h <= Object.keys(data.params[j]["Сущ."]).length; h++) {
                                    h_two = h*10;

                                    $('<div class="trns-susch-block trnsbl" '+d_block+'><p><input type="checkbox" value="'+data.params[j]["Сущ."][h_two]["id"]+'" '+(data.params[j]["Сущ."][h_two]['check'] ? 'checked' : '')+' name="word" class="word trns-word-inp" id="word_'+data.params[j]["Сущ."][h_two]["id"]+'"><label for="word_'+data.params[j]["Сущ."][h_two]["id"]+'">'+data.params[j]["Сущ."][h_two]["value"]+'</label></p></div>').appendTo(".trns-cont");
                                }
                            }
                        }
                        if(data.params[j]["Другое"]) {
                            if(Object.keys(data.params[j]) == "Другое") {
                                cls = "trns-drugoe";
								
								if(j == 10) {
									d_block = 'style="display: block;"';
								} else {
									d_block = 'style="display: none;"';
								}

                                for(h = 1; h <= Object.keys(data.params[j]["Другое"]).length; h++) {
                                    h_two = h*10;

                                    $('<div class="trns-drugoe-block trnsbl" '+d_block+'><p><input type="checkbox" value="'+data.params[j]["Другое"][h_two]["id"]+'" '+(data.params[j]["Другое"][h_two]['check'] ? 'checked' : '')+' name="word" class="word trns-word-inp" id="word_'+data.params[j]["Другое"][h_two]["id"]+'"><label for="word_'+data.params[j]["Другое"][h_two]["id"]+'">'+data.params[j]["Другое"][h_two]["value"]+'</label></p></div>').appendTo(".trns-cont");
                                }
                            }
                        }
						if(data.params[j]["Глагол"]) {
                            if(Object.keys(data.params[j]) == "Глагол") {
                                cls = "trns-glagol";
								
								if(j == 10) {
									d_block = 'style="display: block;"';
								} else {
									d_block = 'style="display: none;"';
								}

                                for(h = 1; h <= Object.keys(data.params[j]["Глагол"]).length; h++) {
                                    h_two = h*10;

                                    $('<div class="trns-glagol-block trnsbl" '+d_block+'><p><input type="checkbox" value="'+data.params[j]["Глагол"][h_two]["id"]+'" '+(data.params[j]["Глагол"][h_two]['check'] ? 'checked' : '')+' name="word" class="word trns-word-inp" id="word_'+data.params[j]["Глагол"][h_two]["id"]+'"><label for="word_'+data.params[j]["Глагол"][h_two]["id"]+'">'+data.params[j]["Глагол"][h_two]["value"]+'</label></p></div>').appendTo(".trns-cont");
                                }
                            }
                        }
						
						
						
						
						
						$('<li role="presentation" class="trns-li '+cls+'"><a href="#">'+Object.keys(data.params[j])+'</a></li>').appendTo(".trns-tabs");
					}
					$(".trns-li:nth(0)").addClass("active");
					
					
					$(".trns-pril").on("click", function() {
						$(".trns-pril-block").show();
						$(".trns-pril").addClass("active");
						
						$(".trns-narech-block").hide();
						$(".trns-narech").removeClass("active");
						
						$(".trns-susch-block").hide();
						$(".trns-susch").removeClass("active");
						
						$(".trns-drugoe-block").hide();
						$(".trns-drugoe").removeClass("active");
						
						$(".trns-glagol-block").hide();
						$(".trns-glagol").removeClass("active");
					});
					
					$(".trns-narech").on("click", function() {
						$(".trns-pril-block").hide();
						$(".trns-pril").removeClass("active");
						
						$(".trns-narech-block").show();
						$(".trns-narech").addClass("active");
						
						$(".trns-susch-block").hide();
						$(".trns-susch").removeClass("active");
						
						$(".trns-drugoe-block").hide();
						$(".trns-drugoe").removeClass("active");
						
						$(".trns-glagol-block").hide();
						$(".trns-glagol").removeClass("active");
					});
					
					$(".trns-susch").on("click", function() {
						$(".trns-pril-block").hide();
						$(".trns-pril").removeClass("active");
						
						$(".trns-narech-block").hide();
						$(".trns-narech").removeClass("active");
						
						$(".trns-susch-block").show();
						$(".trns-susch").addClass("active");
						
						$(".trns-drugoe-block").hide();
						$(".trns-drugoe").removeClass("active");
						
						$(".trns-glagol-block").hide();
						$(".trns-glagol").removeClass("active");
					});
					
					$(".trns-drugoe").on("click", function() {
						$(".trns-pril-block").hide();
						$(".trns-pril").removeClass("active");
						
						$(".trns-narech-block").hide();
						$(".trns-narech").removeClass("active");
						
						$(".trns-susch-block").hide();
						$(".trns-susch").removeClass("active");
						
						$(".trns-drugoe-block").show();
						$(".trns-drugoe").addClass("active");
						
						$(".trns-glagol-block").hide();
						$(".trns-glagol").removeClass("active");
					});
					
					$(".trns-glagol").on("click", function() {
						$(".trns-pril-block").hide();
						$(".trns-pril").removeClass("active");
						
						$(".trns-narech-block").hide();
						$(".trns-narech").removeClass("active");
						
						$(".trns-susch-block").hide();
						$(".trns-susch").removeClass("active");
						
						$(".trns-drugoe-block").hide();
						$(".trns-drugoe").removeClass("active");
						
						$(".trns-glagol-block").show();
						$(".trns-glagol").addClass("active");
					});


                    $(".trnsbl").click(function () {

						var trans_word_id = $(this).find("p").find("input").val();

                        let $container = $(this).closest('.trns-cont');
                        let $checkboxes = $container.find('input:checked');
                        let $el = $('#word'+clickId);

                        console.log($checkboxes.length);

                        if ($checkboxes.length > 0) {
                            $el.prop('checked', true);
                        } else {
                            $el.prop('checked', false);
                        }
                        
						//var trans_word_name = $(this).find("p").find("label").text();

						$.ajax({
							type: 'POST',
							url: '/add-words/put-translate',
							data: {
								id: clickId,
								id_two: trans_word_id
							},
							success: function (html) {
								console.log(html);
							}
						});
                    });



                }
            });

            // alert(clickId);



            $('.popup-close').click(function() {
                $(this).parents('.popup-fade').fadeOut();
				$(".trns-cont").empty();
                return false;
            });

            $(document).keydown(function(e) {
                if (e.keyCode === 27) {
                    e.stopPropagation();
                    $('.popup-fade').fadeOut();
                }
            });

            $('.popup-fade').click(function(e) {
                if ($(e.target).closest('.popup').length == 0) {
                    $(this).fadeOut();
					$(".trns-cont").empty();
                }
            });

        })


    });


</script>
<div>
    <button type="button" class="stm-btn stm-btn_outline stm-btn_orange stm-btn_md" id="add_all">Выделить все на экране</button>

    <button type="button" class="stm-btn stm-btn_outline stm-btn_orange stm-btn_md" id="unchek_all">Снять все на экране</button>


</div>

<form action="">


    <?


    $array = array_chunk($words, 6); ?>
    <br><br>

    <table>
        <?php foreach ($array as $items): ?>
            <tr>
                <?php foreach ($items as $row): ?>
                    <td><?= $this->render('_viewchekbox', [
                            'value' => $row,
                            'words_id' => $words_id,

                        ]) ?></td>
                <?php endforeach; ?>
            </tr>
        <?php endforeach; ?>
    </table>

    <? foreach ($words as $key => $value) { ?>


    <? } ?>

</form>

<!--<div class="text-center">--><? //= LinkPager::widget([
//        'pagination' => $pages,
//        'firstPageLabel' => 'Первая',
//        'lastPageLabel' => 'Последняя'
//
//    ]); ?><!--</div>-->

<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
    <nav class="navigation paging-navigation paging_view_posts-list" role="navigation">
        <div class="text-center">
            <div class="pagination loop-pagination">


                <? $lins_pager = LinkPager::widget([
                    'pagination' => $pages,
                    'hideOnSinglePage' => true,
                    'prevPageLabel' => 'prev',
                    'nextPageLabel' => 'next',

                    'firstPageLabel' => 'first',
                    'lastPageLabel' => 'last',
                    'maxButtonCount' => 5,

                    // Настройки контейнера пагинации
                    'options' => [
                        'tag' => 'ul',
                        'class' => 'pagination',
                        'id' => 'pager-container',
                    ],

                    // Настройки классов css для ссылок
                    'linkOptions' => ['class' => 'mylink'],
                    'activePageCssClass' => 'myactive',
                    'disabledPageCssClass' => 'mydisable',

                    // Настройки для навигационных ссылок
                    'prevPageCssClass' => 'mypre',
                    'nextPageCssClass' => 'mynext',
                    'firstPageCssClass' => 'myfirst',
                    'lastPageCssClass' => 'mylast',


                ]);


                $lishki = explode("<li", $lins_pager);
                unset($lishki[0]);
                if ($_GET["page"] == 1 ) {  unset($lishki[1]);
                    unset($lishki[2]);  unset($lishki[3]);
                }

//                echo $lins_pager;

                ?>
            </div>
        </div>
    </nav>
</div>

<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
    <nav class="navigation paging-navigation paging_view_posts-list" role="navigation">
        <div class="pagination loop-pagination">
            <?php foreach ($lishki as $val ) {
                $subject = $val ;
                $pattern = '/href="(.*)"/U';

                $get_url = preg_match($pattern, $subject, $matches);
                $get_url = $matches[1];


                $subject = $val;
                $pattern = '!data-page="\d*">(.*)<\/a>!';
                unset($matches);


                $get_links_text = preg_match($pattern, $subject, $matches);
                $get_links_text = $matches[1];

                $subject = $val;
                $pattern = '!class="(.*)"!U';
                unset($matches);

                $get_class = preg_match($pattern, $subject, $matches);
                $get_class = $matches[1];

                if ($get_links_text != "prev" and  $get_links_text != "next" and  $get_links_text != "first" and  $get_links_text != "last" ) {
                $pages_list[] = ['text_page' => $get_links_text, 'url_links' =>  $get_url , 'class' => $get_class  ]; }
                if ($get_links_text == "prev" ) { $next_page_url = $get_url; }
                if ($get_links_text == "next" ) { $prev_page_url = $get_url; }
                if ($get_links_text == "last" ) { $last_page_url = $get_url;

//                echo $val;

                    $subject = $val;
                    $pattern = '!href="/add-words/select-from-all\?page=(.*)&amp;per-page=100"!';
//                    echo $pattern;

                    unset($matches);


                $last_page_text = preg_match($pattern, $subject, $matches);
//                    print_r($matches);
                    $last_page_text = $matches[1];
                }

//                print_r($matches);

//                echo $get_links_text;

//                print_r($matches);
//                echo $val;
                ?>




            <? }






            $mystring = $val;
            $findme   = 'class="mylast mydisable">';
            $pos = strpos($mystring, $findme);
            if ($pos) {
                unset($pages_list[count($pages_list) - 1]);
                unset($pages_list[count($pages_list) - 1]); }








//            print_r($pages_list);




            ?>
<!--            <span class="page-numbers current">1</span>-->
<!--            <a class="page-numbers" href="№">2</a>-->
<!--            <a class="next page-numbers" href="https://smartyschool.stylemixthemes.com/kindergarten-two/donations-grid/page/2/">→</a>-->
            <?php
            if ($_GET["page"] > 4 or  $_GET["page"] == 1  ) {
            if ($_GET["page"] == 1 ) { ?><span class="page-numbers current">1</span><? }
            else { ?><a class="page-numbers" href="/add-words/select-from-all?page=1&amp;per-page=100">1</a><? } } ?>
<!--            ПЕРВАЯ СТРАНИЦА-->

           <?php if ($_GET["page"] != 1   ) { ?>  <a class="prev page-numbers" href="<?php echo  $next_page_url; ?>">←</a> <?php } ?>

            <?php foreach ($pages_list as $val_list) {
                if (  $val_list["class"]  == "myactive") { ?><span class="page-numbers current"><?php echo $val_list["text_page"]; ?></span><? }
                else { ?><a class="page-numbers" href="<?php echo $val_list["url_links"]; ?>"><?php echo $val_list["text_page"]; ?></a><? } ?>
            <?  }

            if (!$pos) {

            ?>

            <a class="next page-numbers" href="<?php echo $prev_page_url; ?>">→</a>
            <?php } ?>

            <!--            ПОСЛЕДНЯЯ  СТРАНИЦА-->
            <a class="page-numbers" href="<?php echo $last_page_url; ?>"><?php echo $last_page_text; ?></a>



        </div>
    </nav>
</div>


<?php
$form = ActiveForm::begin([
    'id' => 'add_list',
//'options' => ['class' => 'form-horizontal'],
]) ?>

<?= $form->field($form_list, 'play_list_name') ?>

<div class="form-group">
    <div>
        <?= Html::submitButton('Сохранить список', ['class' => 'stm-btn stm-btn_outline stm-btn_orange stm-btn_md']) ?>
    </div>
</div>
<?php ActiveForm::end() ?>

<style>
    .popup-fade {
        display: none;
    }
    .popup-fade:before {
        content: '';
        background: #000;
        position: fixed;
        left: 0;
        top: 0;
        width: 100%;
        height: 100%;
        opacity: 0.7;
        z-index: 9999;
    }
    .popup {
        position: fixed;
        top: 20%;
        left: 50%;
        padding: 20px;
        width: 550px;
        margin-left: -250px;
        background: #fff;
        border: 1px solid orange;
        border-radius: 4px;
        z-index: 99999;
        opacity: 1;
    }
    .popup-close {
        position: absolute;
        top: 10px;
        right: 10px;
    }
	.trnsbl {
		margin: 0 10px;
	}
	.trnsbl label {
		margin-left: 5px;
	}
	.trns-drugoe-block {
		display: none;
		width: 100px;
	}
	.trns-susch-block {
		display: none;
	}
	.trns-narech-block {
		display: none;
	}


</style>





<div class="popup-fade">
    <div class="popup">
        <a class="popup-close" href="#"><img src="/static/img/2/close.png" alt="Закрыть" width="20"></a>
    <p class="trns-for">Выбрать переводы для слова - <span></span></p>

        <ul class="nav nav-tabs trns-tabs">
            
        </ul>

        <div class="trns-cont" style="padding-top: 10px; display: flex; align-items: center; flex-wrap: wrap;"></div>


    </div>
</div>
