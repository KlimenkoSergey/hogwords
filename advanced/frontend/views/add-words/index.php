<?php

/* @var $this yii\web\View */

$this->title = 'Указать свои слова к изучению';
$this->params['breadcrumbs'][] = $this->title;

use yii\helpers\Html;
use yii\widgets\ActiveForm; ?>
<div class="site-index">

    <h1>Создать список из своих слов</h1>
    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'play_list_name')->textInput() ?>

    <?= $form->field($model, 'words')->textarea(['rows' => 20, 'class' => 'wpcf7-textarea' ,  'placeholder' => 'Слова, каждое с новой строки. *'  ]) ?>



    <div class="form-group">
        <?= Html::submitButton('Добавить слова', ['class' => 'stm-btn stm-btn_outline stm-btn_orange stm-btn_md']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
