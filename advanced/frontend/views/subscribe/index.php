<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\captcha\Captcha;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Alert;



$this->title = 'Оформить подписку ';
$this->params['breadcrumbs'][] = $this->title;
?>

<h3>Оформить подписку</h3>

<script>
    $(document).ready(function(){

        // alert(jQuery.fn.jquery);



        $('select').on('change', function() {
            // alert( this.value );
            var count_mounth = this.value;
            $('#itogo').empty();

            $.ajax({
                type: 'GET',
                url: '/subscribe/get-price',
                data: {id:count_mounth},
                success: function (html) {
                    $("#itogo").append(html);
                }
            });   /////////// AJAX POST


        });



    });
</script>


<div class="row">
    <div class="col-lg-5">
        <?php $form = ActiveForm::begin(['id' => 'subscribe']); ?>

        <?php

        echo $form->field($model, 'count_mounths')->dropDownList([

            '' => 'Выберите...',
            '1' => '1 месяц',
            '2' => '2 месяца',
            '3'=>'3 месяца',
            '4'=>'4 месяца',
            '5'=>'5 месяцев',
            '6'=>'6 месяцев',
            '7'=>'7 месяцев',
            '8'=>'8 месяцев',
            '9'=>'9 месяцев',
            '10'=>'10 месяцев',
            '11'=>'11 месяцев',
            '12'=>'1 год',
            '13'=>'Вечный аккаунт',
        ]);

        ?>
        <div id="itogo" style="font-size: 22px; color: green; font-weight: 700;"></div>


        <?php

        echo $form->field($model, 'cashbox')->dropDownList([

//            '1' => 'Interkassa',
            '2' => 'Freekassa',
//            '3' => 'Primepayments',

        ]);

        ?>



            <?= Html::submitButton('Оплатить', ['class' => 'stm-btn stm-btn_outline stm-btn_orange stm-btn_md', 'name' => 'contact-button']) ?>


        <?php ActiveForm::end(); ?>
    </div>
</div>
