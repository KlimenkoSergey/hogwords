<?php


namespace frontend\controllers;


use common\models\Playlistwords;
use common\models\PlaylistwordsTranslate;
use Yii;
use yii\filters\AccessControl;

class ExercisesApiMethodsController extends AppController
{

    public function behaviors()
    {
        return [

//            'access' => [
//                'class' => AccessControl::className(),
//                'rules' => [
//                    [
//                        'roles' => ['@'],
//                        'allow' => true,
//                    ],
//
//                ],
//            ],


        ];
    }



    public function actionAnswer($id, $cursor_id)
    {
        $scale = self::getScale($id);

//        print_r($scale);
        $where_staying = $cursor_id;
        $get_data = ExercisesApiController::getData($where_staying, $scale);
//        $session->set('bookmark_' . $id . '', $where_staying);

        self::putBookmark($id, $cursor_id);
        self::sendJson($get_data);
    }


    public function actionCurrent($id)
    {


        $scale = self::getScale($id);
        $where_staying = self::getBookmark($id);
        $get_data = ExercisesApiController::getData($where_staying, $scale);
//        $session->set('bookmark_' . $id . '', $where_staying);
        self::sendJson($get_data);
    }


    public function actionNext($id)
    {


        $scale = self::getScale($id);

        $max = count($scale);
        $max = $max - 1;

//        echo $max;
//        print_r($scale);
        $where_staying = self::getBookmark($id);
//        print_r($where_staying);
        if ($where_staying == $max) {
            $where_staying = $max;
        } else {
            $where_staying = $where_staying + 1;
        }

        

        $get_data = ExercisesApiController::getData($where_staying, $scale);
        self::putBookmark($id, $where_staying);
        self::sendJson($get_data);
    }

    public function actionPrev($id)
    {
        $scale = self::getScale($id);
        $max = count($scale);
        $max = $max - 1;
        $where_staying = self::getBookmark($id);

        if ($where_staying == 0) {
            $where_staying = 0;
        } else {
            $where_staying = $where_staying - 1;
        }
        $get_data = ExercisesApiController::getData($where_staying, $scale);
        self::putBookmark($id, $where_staying);
        self::sendJson($get_data);


    }

    public function actionBeginning($id)
    {
        $scale = self::getScale($id);
        $where_staying = 0;
        self::putBookmark($id, 0);
        $get_data = ExercisesApiController::getData($where_staying, $scale);
//        $session->set('bookmark_' . $id . '', $where_staying);
        self::sendJson($get_data);


    }

    public function actionHardWord()
    {
    }

    public function actionDelete($id)
    {
        $scale = self::getScale($id);
        $where_staying = self::getBookmark($id);
        $massiv_kotoriy_udalit = $scale[$where_staying];

        $id_playlistwords = $massiv_kotoriy_udalit["id_playlistwords"];
        $current_playlistwords = Playlistwords::findOne($id_playlistwords);
//        echo $id;
//        print_r($current_playlistwords);

        $get_nex_id = Playlistwords::find()->where([ '>','wordId', $current_playlistwords->wordId])->orderBy('sort')->one();
        $delete_words = AppController::deleteWordFromList($id_playlistwords);
//        var_dump($delete_words);

//        var_dump($where_staying);
//        print_r($get_nex_id->wordId);
//        die();
        $scale = ExercisesApiController::generatePlayList($id);

//        print_r($scale);
//
//        die();
//        foreach ($scale as $key => $value) {
//
//            print_r($value);
//            var_dump($get_nex_id->wordId);
//
//            if ($value["word_id"] == $get_nex_id->wordId) {
//
////                echo $value["word_id"]; echo " - "; echo $get_nex_id->wordId;
//                self::putBookmark($id, $key);
//
//
//
//
//                self::actionCurrent($id);
//
//
//
//
//                break;
//
//            }
//
//
//
////            print_r($value);
//
//
//        }

        self::actionBeginning($id);



    }

    public function actionMixAndStart($id)
    {

        $mix_sort = Playlistwords::find()->where(['playListId' => $id])->all();
        foreach ($mix_sort as $value) {
            $get_play_list = Playlistwords::findOne($value->id);
            $get_play_list->sort = rand(100, 20000);
            $get_play_list->save();
        }
        $scale = ExercisesApiController::generatePlayList($id);
        self::actionBeginning($id);

    }

    public function actionCountProgressBar($id)
    {

        $scale = self::getScale($id);
        $where_staying = self::getBookmark($id);
//        print_r($scale);
        $vsego = count($scale);
        $vsego = $vsego -1 ;

        $odin_procent = $vsego / 100;
        $count_proideno_procent = $where_staying / $odin_procent;

        $count_proideno_procent = round($count_proideno_procent);
//        $count_proideno_procent = 100 - $count_proideno_procent;
        return $count_proideno_procent;


    }

    public function actionGetOptions($id)
    {

        $session = Yii::$app->session;
        $id = (int)$id;
        $options = $session->get('options_' . $id . '');
        if (!$options) {
            if (empty($options)) {

                $options = [

                    'type_exercises' => [1, 2, 3, 4, 5],
                    'autoPlayWords' => '0',
                    'autoPlayVideo' => '0',
                    'autoPlayAudio' => '1',
                    'pauseBetweenWords' => '0',
                    'portionsOfWords' => 5,


                ];
//                $scale = ExercisesApiController::generatePlayList($id);
            }
        }


        // [ 'type_exercises' => [1,2,3,4,5]],
        // 'autoPlayWords' => 'On',
        // 'autoPlayVideo' => 'Off',
        // 'autoPlayAudio' => 'On',
        // 'pauseBetweenWords' => '20',
        // 'portionsOfWords' => 5,
        self::sendJson($options);


    }

    public function actionGetScale($id)
    {
        $scale = self::getScale($id);
        self::sendJson($scale);
    }

    public function actionGetBookmark($id)
    {
        $where_staying = self::getBookmark($id);
        self::sendJson($where_staying);
    }

    public function actionPutBookmark($id, $bookmark_id)
    {
        self::putBookmark($id, $bookmark_id);
        return true;
    }


    public static function getScale($id)
    {

        $session = Yii::$app->session;
//        $session->remove('scale_' . $id . '');

        $scale = $session->get('scale_' . $id . '');
        if (!$scale) {
            if (empty($scale)) {
                $scale = ExercisesApiController::generatePlayList($id);
            }
        }

//        print_r($scale);

        return $scale;
    }

    public static function getBookmark($id)
    {
        $session = Yii::$app->session;
        $where_staying = $session->get('bookmark_' . $id . '');
        if (!$where_staying) {
            if (empty($where_staying)) {
                $where_staying = 0;
            }
        }
        return $where_staying;


    }

    public static function putBookmark($id, $bookmark_id)
    {

        $session = Yii::$app->session;
        $session->set('bookmark_' . $id . '', $bookmark_id);
        return true;


    }

    public static function sendJson($array)
    {
        $json = json_encode($array);
        header('Content-type: application/json');
        echo $json;
        exit;
    }


}