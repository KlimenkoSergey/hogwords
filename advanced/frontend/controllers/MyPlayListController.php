<?php


namespace frontend\controllers;


use common\models\PlayList;
use common\models\Playlistwords;
use Yii;
use yii\filters\AccessControl;

class MyPlayListController extends AppController
{

    public function behaviors()
    {
        return [

            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'roles' => ['@'],
                        'allow' => true,
                    ],

                ],
            ],


        ];
    }

    public function actionIndex() {

        $lists = PlayList::find()->where(['user_id' => Yii::$app->user->getId() ])->all();

//        print_r($lists);

        return $this->render('index' , [
            'lists' => $lists,

        ]);

    }

    public  static function getLists() {
        $lists = PlayList::find()->where(['user_id' => Yii::$app->user->getId() ])->all();
        return $lists;


    }

    public function actionDelete($id) {


        $id = (int) $id;
        $del_words = AppController::delPlaylistwordsWithTranslates($id);


//        Playlistwords::deleteAll(['playListId' => $id ]);
//
//        $del_list = PlayList::findOne($id);
//        $del_list->delete();

        Yii::$app->session->setFlash('success', 'Удалено!');

        return $this->redirect(Yii::$app->request->referrer);





    }

}