<?php


namespace frontend\controllers;


use common\models\PartsOfSpeech;
use common\models\PartsOfSpeechWords;
use common\models\PlayList;
use common\models\Playlistwords;
use common\models\PlaylistwordsTranslate;
use common\models\Translate;
use common\models\Words;
use frontend\models\forms\AddWordsListForm;
use frontend\models\forms\SelectFromAllForm;
use phpDocumentor\Reflection\Types\Null_;
use Yii;
use yii\data\Pagination;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;

class AddWordsController extends AppController
{

    public $error;
    const  STATUS_WORD_QUEUE = 40;
    const LANG_RU = 2;
    const LANG_EN = 1;



    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => \yii\filters\VerbFilter::class,
                'actions' => [
                    'AddWordInList' => ['POST'],
//                    'view'   => ['GET'],
//                    'create' => ['GET', 'POST'],
//                    'update' => ['GET', 'PUT', 'POST'],
//                    'delete' => ['POST', 'DELETE'],
                ],
            ],

            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'roles' => ['@'],
                        'allow' => true,
                    ],

                ],
            ],


        ];
    }


    public function actionIndex()
    {

        $model = new AddWordsListForm();

        if ($model->load(Yii::$app->request->post())) {
            $count_sovpadeniya = 0;


            $words = explode(PHP_EOL, $model->words);
            foreach ($words as $word) {
                if (strlen($word) > 1001) {
                    $this->error[] = "Длина строки не должна превышать 1000 символов.";
                }
                $word = self::handlingWords($word);
                $word_id = self::chekUnic($word);
                if ($word_id  !== false) { $play_list[] = $word_id; $count_sovpadeniya = $count_sovpadeniya + 1; }

            }

            $play_list_uniq = array_unique($play_list);

            if ($this->error) {
                $count_error = count($this->error);
            } else {
                $count_error = 0;
            }


            if ($count_error == 0) {

                $add_list = new PlayList();
                $add_list->name = $model->play_list_name;
                $add_list->user_id = Yii::$app->user->getId();
                $add_list->save();
                $user_id = Yii::$app->user->getId();
                self::AddPlayListWord( $play_list_uniq , $add_list->id  );



                    Yii::$app->session->setFlash('success', "Слова добавлены. Найдено ".$count_sovpadeniya." слов.");
                    return Yii::$app->getResponse()->redirect(Yii::$app->getRequest()->getUrl() );




            } else {
                $viewError = self::viewsErrorFromArray($this->error);
                Yii::$app->session->setFlash('danger', $viewError);
            }


        }


        return $this->render('index',
            [
                'model' => $model,
            ]
        );

    }

    public  static function AddPlayListWord($words_id, $list_id) {
        $sort = 10;
        foreach ($words_id as $word) {
            $add_list_word = new Playlistwords();

            $add_list_word->wordId  = $word;
            $add_list_word->playListId = $list_id;
            $add_list_word->sort = $sort;
            $add_list_word->save();

            $sort = $sort + 10;
        }




    }


    public function actionAddAll()
    {


        return $this->render('addall',
            [
//                'model' => $model,
            ]
        );

    }

    public function actionSelectFromAll()
    {

//        $words = Words::find()
//            ->where(['status' => 30])
//            ->orderBy('sort DESC')
//            ->all();


//        $list_add = new PlayList();
//        $last_id_list = PlayList::find()->orderBy('id DESC')->one();
//        $name_list = "Список ".$last_id_list->id."";
//        $list_add->name = $name_list;
//        $list_add->user_id = Yii::$app->user->getId();
//        $list_add->save();


        //$posts = Post::find()->all();
        $session = Yii::$app->session;
        $words_id = $session->get('words_id');



//        $words_id[800] = ['word_id' => 6,
//            'translates' => [1,2,3,4,5,]
//            ] ;
//
//
//        print_r($words_id);
//        die();

        $query = Words::find()->where(['status' => 30])->with('translates');
        $pages = new Pagination(['totalCount' => $query->count(),  'pageSize' => 50]);
        $words = $query->offset($pages->offset)

            ->limit($pages->limit)
            ->all();


        $form_list = new SelectFromAllForm();
        if ($form_list->load(Yii::$app->request->post()) && $form_list->validate()) {

            $us_id = Yii::$app->user->getId();

            $add_list  = new PlayList();
            $add_list->user_id = $us_id;
            $add_list->name = $form_list->play_list_name;
            $add_list->save();

            $sort = 10;
//            print_r($words_id);
//
//            die();

            foreach ($words_id as $key => $value) {

                $add_word_list  = new Playlistwords();
                $add_word_list->wordId = $key;
                $add_word_list->playListId  = $add_list->id ;
                $add_word_list->sort = $sort;
                $add_word_list->save();
                $sort = $sort + 10;

//                echo count($words_id[$key]);
//                die();

                if (is_array($words_id[$key]) and count($words_id[$key]) > 0 ) {

//                    echo "asdasd";
//                    die();
                    $sort_translate = 10;
                    foreach ($words_id[$key] as $translate) {
                        $insert_translate = new PlaylistwordsTranslate();
                        $insert_translate->playlistwords_id = $add_word_list->id;
                        $insert_translate->translate_id = $translate;
                        $insert_translate->sort = $sort_translate;
                        $insert_translate->save();
                        $sort_translate = $sort_translate + 10;
                    }


                }


            }

            $session->remove('words_id');
            $session->setFlash('success', 'Список создан! <a href="/training/index?id='.$add_list->id.'">Приступить к изучению.</a>');
            return Yii::$app->getResponse()->redirect(Yii::$app->getRequest()->getUrl() );








        }




        return $this->render('selectfromall',
            [
                'words' => $words,
                'pages' => $pages,
//                'list_add' => $list_add,
                'words_id' => $words_id,
                'form_list' => $form_list,


            ]
        );

    }

    public function actionPutTranslate() {
        if (Yii::$app->request->isAjax) {
            $request = Yii::$app->request;
            $word_id = $request->getBodyParam('id');
            $word_id = (int) $word_id;
            $translate_id =  $request->getBodyParam('id_two');
            $translate_id = (int) $translate_id;
            $session = Yii::$app->session;
            $words = $session->get('words_id');



            $key = array_search($translate_id,  $words[$word_id]);
//            var_dump($translate_id);
            if ($key === false or $key === NULL  ) {


                if (is_array( $words[$word_id])) {
//                    echo "Есть массив";

                    $count_trans = count($words[$word_id]);
//                    var_dump($count_trans);
                    $plus_trunslate = [ $count_trans => $translate_id] ;

//                    foreach ($words[$word_id] as $key => $val) {
//
//
//
//                    }
//
//                    $words[$word_id] = $translate_id ;

                    $temp_array = $words[$word_id];
                    $temp_array2 = array_merge ($temp_array, $plus_trunslate);
                    $words[$word_id] = $temp_array2;


                }
                else {
//                    echo "Нету массива";
                    $words[$word_id] = [ 0 => $translate_id]; }



                 }
            else {
                unset($words[$word_id][$key]);

                if (is_array($words[$word_id]) and empty($words[$word_id])) { unset($words[$word_id]); }
               }



            $session->set('words_id', $words);
//            $session->remove('words_id');
            print_r($words);




//                print_r ($_SESSION["words_id"]);
            die();

        }
    }

    public function actionGetTranslate()
    {
        if (Yii::$app->request->isAjax) {


            $request = Yii::$app->request;
            $param = $request->getBodyParam('id');
            $param = (int) $param;
            $session = Yii::$app->session;
            $words = $session->get('words_id');  // масиив из сессии
//            print_r($words);

            $get_word = Words::findOne($param);


            $parts_of_speech_words = PartsOfSpeechWords::find()->where(['word_id'=> $param])->with('partsOfSpeech')->all();
            if ($parts_of_speech_words) {
                foreach ($parts_of_speech_words as $part) {
//                    echo '['.$part["partsOfSpeech"]->value.']';

					$get_translate = Translate::find()->where(['word_id'=>  $part->word_id ])->andWhere(['parts_of_speech_id' => $part["partsOfSpeech"]->id ])->orderBy('sort')->all();
					if ($get_translate )   {
					
						foreach ($get_translate as $value) {
                            if (!isset($words[$get_word->id])) { $words[$get_word->id] = $get_word->id; }
                            $check_uncheck = self::checkTranslate($value->id ,   $words[$get_word->id]);
                            $param_translate = [
                                'id' => $value->id,
                                'value' => $value->value,
                                'check' => $check_uncheck,
                            ];
							$dlya_jsone[$part->sort][$part["partsOfSpeech"]->value][$value->sort] =  $param_translate;
	//                    echo $value->value; echo '<br>';
						}
					} 
				}
            $ves_massiv = [
                'name' => $get_word->word,
                'id' => $get_word->id,
                'params' => $dlya_jsone

            ];


                $json = json_encode($ves_massiv);
                header('Content-type: application/json');
                echo $json;
                exit;

            }

            return false;

            }


    die();
    }

    public function actionCheckTranslateWord() {
        if (Yii::$app->request->isAjax) {

            $request = Yii::$app->request;
            $id = $request->getBodyParam('id');
            $id_two = $request->getBodyParam('id_two');

            $list = $id."-".$id_two;

            return $list;
        }
    }

    public static function checkTranslate($id_translate,  $words ) {
//        print_r($id_translate);
//        print_r($words);

//        die();
        $key = array_search($id_translate, $words);
        if ($key === false or $key === NULL ) { return 0; }
        else {  return 1;  }

    }

    public function addPlayList(array $play_list_uniq)
    {


        $id_user = Yii::$app->user->id;
        foreach ($play_list_uniq as $value) {
            $add_pl_list = new PlayList();
            $add_pl_list->user_id = $id_user;

        }


    }


    public function viewsErrorFromArray(array $error)
    {
        foreach ($error as $value) {

            $viewError .= "" . $value . "<br>";


        }

        return $viewError;


    }

    public function handlingWords($word)
    {

        $word = trim($word);
        $word = htmlspecialchars($word);
//        $word = mysqli_real_escape_string($word);


        return $word;


    }

    public function chekUnic($word_add)
    {

        $word = Words::find()->where(['word' => $word_add])->one();
        if (!$word) {
//            $word = new Words();
//
//            $word->word = $word_add;
//            $word->status = self::STATUS_WORD_QUEUE;
//            $word->language_source = self::LANG_EN;
//            $word->language_to = self::LANG_RU;
//            $word->save();
//            var_dump($word->getErrors());

//            die();

            return false;


        }
        return $word->id;

//        print_r($word);
//        die();

    }

    public function actionAddWordInList()
    {
        if (Yii::$app->request->isAjax) {


            $request = Yii::$app->request;
            $param = $request->getBodyParam('id');
            $param = (int)$param;
            $session = Yii::$app->session;

//            $session->remove('words_id');



            $words = $session->get('words_id');

//            var_dump($words);
//            die();

            $words = self::add_in_array_checkboxes( $words , $param );
            $session->set('words_id', $words);

//            print_r($words);





        }
        die();


    }



    public function actionCheckAll()
    {
        if (Yii::$app->request->isAjax) {

            $request = Yii::$app->request;
            $param = $request->getBodyParam('id');


            $params = explode('-', $param );



            $session = Yii::$app->session;
            $words = $session->get('words_id');

            foreach ($params as $val) {
                $val = (int)$val;
                $words = self::add_in_array_checkboxes_for_select_all( $words , $val );
            }

//            print_r($words);
             $session->set('words_id', $words);


        }
        die();


    }



    public function actionUncheckAll()
    {
        if (Yii::$app->request->isAjax) {

            $request = Yii::$app->request;
            $param = $request->getBodyParam('id');


            $params = explode('-', $param );



            $session = Yii::$app->session;
            $words = $session->get('words_id');

            foreach ($params as $val) {
                $val = (int)$val;
                unset($words[$val]);
//                $words = self::add_in_array_checkboxes( $words , $val );
            }

//            print_r($words);
            $session->set('words_id', $words);
//            $session->remove('words_id');


        }
        die();


    }

    public  static function add_in_array_checkboxes( $words , $param ) {

        if ($words === null) {
            $words[$param] = $param;
        } else {
            if (isset($words[$param])) { unset($words[$param]); }
            else { $words[$param] = $param; }

//            foreach ($words as $val ) {
//
//                if (in_array( $param,  $val )) {
//                    $key = array_search($param, $words);
//                    unset($words[$key]);
//                }
//                else { $words[] = $param; }
//
//
//
//            }


        }


        return $words;
    }


    public  static function add_in_array_checkboxes_for_select_all( $words , $param ) {

        if ($words === null) {
            $words[$param] = $param;
        } else {
            if (isset($words[$param])) {
//                unset($words[$param]);
            }
            else { $words[$param] = $param; }

//            foreach ($words as $val ) {
//
//                if (in_array( $param,  $val )) {
//                    $key = array_search($param, $words);
//                    unset($words[$key]);
//                }
//                else { $words[] = $param; }
//
//
//
//            }


        }


        return $words;
    }





}