<?php


namespace frontend\controllers;


use common\models\PlayList;
use common\models\Playlistwords;
use common\models\Words;
use Yii;
use yii\filters\AccessControl;

class PlayWordsController extends AppController
{


    public function behaviors()
    {
        return [

            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'roles' => ['@'],
                        'allow' => true,
                    ],

                ],
            ],


        ];
    }


    public function beforeAction($action)
    {
        if (in_array($action->id, ['next-word' , 'count-progress-bar' , 'prev-word' , 'to-the-begining' , 'mix-and-start' , 'learn-and-del'  ])) {
            $this->enableCsrfValidation = false;
        }
        return parent::beforeAction($action);
    }


    public function actionIndex()
    {

        $request = Yii::$app->request;

        $list = $request->get('list');
        $get_list_name = PlayList::findOne($list);

        $list = (int) $list;
        $word_id = AjaxStaticFunctionController::getCurrentWordForIndex($list);
        $select_word = Words::findOne($word_id);
        return $this->render('index',
            [
                'select_word' => $select_word,
                'get_list_name' => $get_list_name,
            ]
        );


    }

    public function actionNextWord()
    {


        $request = Yii::$app->request;
        $list_id = $request->post('id');



        $list_id = (int)$list_id;
        $word_id = AjaxStaticFunctionController::getCurrentWord($list_id);
        $select_word = Words::findOne( $word_id );
//        $select_word =  json_encode( $select_word);
//        print_r($select_word);
        $json = AjaxStaticFunctionController::preparationData($select_word);
        header('Content-type: application/json');
        echo $json;
        exit;


    }


    public function actionPrevWord()
    {
        $request = Yii::$app->request;
        $list_id = $request->post('id');
        $list_id = (int)$list_id;
        $word_id = AjaxStaticFunctionController::getCurrentWordForPrev($list_id );
        $select_word = Words::findOne( $word_id );
//        $select_word =  json_encode( $select_word);
//        print_r($select_word);
        $json = AjaxStaticFunctionController::preparationData($select_word);
        header('Content-type: application/json');
        echo $json;
        exit;


    }

    public function actionToTheBegining()
    {
        $request = Yii::$app->request;
        $list_id = $request->post('id');
        $list_id = (int)$list_id;
        $word_id = AjaxStaticFunctionController::getCurrentWordForBegin($list_id );
        $select_word = Words::findOne( $word_id );
//        $select_word =  json_encode( $select_word);
//        print_r($select_word);
        $json = AjaxStaticFunctionController::preparationData($select_word);
        header('Content-type: application/json');
        echo $json;
        exit;


    }


    public function actionLearnAndDel()
    {
        $request = Yii::$app->request;
        $list_id = $request->post('id');
        $list_id = (int)$list_id;
        $word_id = AjaxStaticFunctionController::getCurrentWordForLearnAndDel($list_id );
        $select_word = Words::findOne( $word_id );
//        $select_word =  json_encode( $select_word);
//        print_r($select_word);
        $json = AjaxStaticFunctionController::preparationData($select_word);
        header('Content-type: application/json');
        echo $json;
        exit;


    }


    public function actionMixAndStart()
    {
        $request = Yii::$app->request;
        $list_id = $request->post('id');
        $list_id = (int)$list_id;
        $word_id = AjaxStaticFunctionController::getCurrentWordForMixAndStart($list_id );
        $select_word = Words::findOne( $word_id );
//        $select_word =  json_encode( $select_word);
//        print_r($select_word);
        $json = AjaxStaticFunctionController::preparationData($select_word);
        header('Content-type: application/json');
        echo $json;
        exit;


    }

    public function actionCountProgressBar()
    {

        $request = Yii::$app->request;
        $list_id = $request->post('id');
        $list_id = (int)$list_id;

        $procent = AjaxStaticFunctionController::countProgressBar($list_id);
        header('Content-type: application/json');
        echo $procent;
        exit;

    }

    public function actionExercises2() {





    }








}