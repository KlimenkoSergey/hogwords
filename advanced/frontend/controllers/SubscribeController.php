<?php


namespace frontend\controllers;


use frontend\models\forms\SubscribeForm;
use Yii;
use yii\filters\AccessControl;

class SubscribeController extends AppController
{


    public function behaviors()
    {
        return [

            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'roles' => ['@'],
                        'allow' => true,
                    ],

                ],
            ],


        ];
    }

    public function actionIndex() {


        $model = new SubscribeForm();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $summa = self::calcSumma($model->count_mounths);
            $session = Yii::$app->session;
            $session->set('add_balance', $summa);
            $session->set('period', $model->count_mounths);


            if ($model->cashbox == 1 ) { return $this->redirect('/interkassa/index');  }
            if ($model->cashbox == 2 ) { return $this->redirect('/freekassa/index');  }
            if ($model->cashbox == 3) { return $this->redirect('/primepayments/index');  }







        }

        return $this->render('index', [
            'model' => $model,
        ]);


    }

    public function actionGetPrice($id) {

        $id = (int) $id;

        if ($id < 12) { $price = $id*9.99; }
        if ($id == 12) { $price = 72; }
        if ($id == 13) { $price = 200; }


        echo "<br>";
        echo "Стоимость: "; echo $price; echo "USD";

        echo "<br>";  echo "<br>";

        die();


    }

    public  static function calcSumma($id) {
        $id = (int) $id;

        if ($id < 12) { $price = $id*9.99; }
        if ($id == 12) { $price = 72; }
        if ($id == 13) { $price = 200; }

        return $price;




    }
    

}