<?php


namespace frontend\controllers;


use common\models\Playlistwords;
use Yii;
use yii\filters\AccessControl;

class AjaxStaticFunctionController extends AppController
{

    public function behaviors()
    {
        return [

            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'roles' => ['@'],
                        'allow' => true,
                    ],

                ],
            ],


        ];
    }


    public static function preparationData($select_word)
    {


        $data[] = $select_word->sort;
        $data[] = $select_word->word;
        $data[] = $select_word->transcription;
        $data[] = $select_word->video;
        $data[] = $select_word->status;
        $data[] = $select_word->translate;
        $data[] = $select_word->language_source;
        $data[] = $select_word->language_to;
        $data[] = "translation_options";
        $data[] = "examples";
        $data[] = "definitions";
        $data[] = $select_word->synonyms;
        $data[] = $select_word->translation_options_clean;
        $data[] = $select_word->examples_clean;
        $data[] = $select_word->definitions_clean;
        $data[] = $select_word->id;
        $json = json_encode($data);


        return $json;


    }


    public static function getCurrentWord($list_id   )
    {



        $request = Yii::$app->request;
        $session = Yii::$app->session;
        $curr_word = $session->get('curr_word');



        $list_id = (int)$list_id;
        if ($curr_word[$list_id]['prev_word'] == "") {
            $select_word_id = Playlistwords::find()->where(['playListId' => $list_id])
                ->orderBy('sort')->one();

        } else {
            $get_prev_sort = Playlistwords::find()->where(['playListId' => $list_id])->andWhere(['wordId' => $curr_word[$list_id]['prev_word']])->one();
            $select_word_id = Playlistwords::find()->where(['playListId' => $list_id])
                ->andWhere([">", 'sort', $get_prev_sort->sort])->orderBy('sort')->limit(1)->one();
        }

        if (!$select_word_id) { $select_word_id = Playlistwords::find()->where(['playListId' => $list_id])
            ->orderBy('sort DESC')->one(); }


        $curr_word = [
            $list_id => [
                'prev_word' => $select_word_id->wordId,
            ]

        ];

        $session->set('curr_word', $curr_word);
        return $select_word_id->wordId;


    }



    public static function getCurrentWordForPrev($list_id   )
    {



        $request = Yii::$app->request;
        $session = Yii::$app->session;
        $curr_word = $session->get('curr_word');
        $list_id = (int)$list_id;
        if ($curr_word[$list_id]['prev_word'] == "") {
            $select_word_id = Playlistwords::find()->where(['playListId' => $list_id])
                ->orderBy('sort')->one();

        } else {
            $get_prev_sort = Playlistwords::find()->where(['playListId' => $list_id])->andWhere(['wordId' => $curr_word[$list_id]['prev_word']])->one();
            $select_word_id = Playlistwords::find()->where(['playListId' => $list_id])
                ->andWhere(["<", 'sort', $get_prev_sort->sort])->orderBy('sort DESC')->limit(1)->one();
        }

        if (!$select_word_id) { $select_word_id = Playlistwords::find()->where(['playListId' => $list_id])
            ->orderBy('sort')->one(); }


        $curr_word = [
            $list_id => [
                'prev_word' => $select_word_id->wordId,
            ]

        ];

        $session->set('curr_word', $curr_word);
        return $select_word_id->wordId;


    }


    public static function getCurrentWordForIndex($list_id   )
    {



        $request = Yii::$app->request;
        $session = Yii::$app->session;
        $curr_word = $session->get('curr_word');

//        print_r($curr_word);
//        die();


        $list_id = (int)$list_id;
        if ($curr_word) {
        if ($curr_word[$list_id]['prev_word'] == "") {
            $select_word_id = Playlistwords::find()->where(['playListId' => $list_id])
                ->orderBy('sort')->one();

        } else {
            $get_prev_sort = Playlistwords::find()->where(['playListId' => $list_id])->andWhere(['wordId' => $curr_word[$list_id]['prev_word']])->one();
            $select_word_id = Playlistwords::find()->where(['playListId' => $list_id])
                ->andWhere(["=", 'sort', $get_prev_sort->sort])->orderBy('sort DESC')->limit(1)->one();
        }
        }

        if (!$select_word_id) { $select_word_id = Playlistwords::find()->where(['playListId' => $list_id])
            ->orderBy('sort')->one(); }



        $curr_word = [
            $list_id => [
                'prev_word' => $select_word_id->wordId,
            ]

        ];

        $session->set('curr_word', $curr_word);
        return $select_word_id->wordId;


    }


    public static function getCurrentWordForBegin($list_id   )
    {



        $request = Yii::$app->request;
        $session = Yii::$app->session;
        $curr_word = $session->get('curr_word');
        $list_id = (int)$list_id;
        $select_word_id = Playlistwords::find()->where(['playListId' => $list_id])
            ->orderBy('sort')->one();


        $curr_word = [
            $list_id => [
                'prev_word' => $select_word_id->wordId,
            ]

        ];

        $session->set('curr_word', $curr_word);
        return $select_word_id->wordId;


    }



    public static function getCurrentWordForLearnAndDel($list_id   )
    {



        $request = Yii::$app->request;
        $session = Yii::$app->session;
        $curr_word = $session->get('curr_word');
        $list_id = (int)$list_id;
        if ($curr_word[$list_id]['prev_word'] == "") {
            $select_word_id = Playlistwords::find()->where(['playListId' => $list_id])
                ->orderBy('sort')->one();

        } else {
            $get_prev_sort = Playlistwords::find()->where(['playListId' => $list_id])->andWhere(['wordId' => $curr_word[$list_id]['prev_word']])->one();
            $select_word_id = Playlistwords::find()->where(['playListId' => $list_id])
                ->andWhere([">", 'sort', $get_prev_sort->sort])->orderBy('sort')->limit(1)->one();
        }



        $del_from_list = Playlistwords::find()->where(['playListId' => $list_id])->andWhere(['wordId' => $curr_word[$list_id]['prev_word']])->one();
//        print_r($del_from_list);
//        echo "asdasd";
//        die();
        $del_from_list->delete();


        if (!$select_word_id) { $select_word_id = Playlistwords::find()->where(['playListId' => $list_id])
            ->orderBy('sort')->one(); }


        $curr_word = [
            $list_id => [
                'prev_word' => $select_word_id->wordId,
            ]

        ];

        $session->set('curr_word', $curr_word);
        return $select_word_id->wordId;


    }


    public static function getCurrentWordForMixAndStart($list_id   )
    {



        $request = Yii::$app->request;
        $session = Yii::$app->session;
        $curr_word = $session->get('curr_word');
        $list_id = (int)$list_id;
//        if ($curr_word[$list_id]['prev_word'] == "") {
//            $select_word_id = Playlistwords::find()->where(['playListId' => $list_id])
//                ->orderBy('sort')->one();
//
//        } else {
//            $get_prev_sort = Playlistwords::find()->where(['playListId' => $list_id])->andWhere(['wordId' => $curr_word[$list_id]['prev_word']])->one();
//            $select_word_id = Playlistwords::find()->where(['playListId' => $list_id])
//                ->andWhere([">", 'sort', $get_prev_sort->sort])->orderBy('sort')->limit(1)->one();
//        }

        $mix_sort =  Playlistwords::find()->where(['playListId' => $list_id])->all();
        foreach ($mix_sort as $value) {
        $get_play_list = Playlistwords::findOne($value->id);
            $get_play_list->sort = rand(100,20000);
//            print_r($get_play_list->sort );
            $get_play_list->save();




        }




        $select_word_id = Playlistwords::find()->where(['playListId' => $list_id])
        ->orderBy('sort')->one();


        $curr_word = [
            $list_id => [
                'prev_word' => $select_word_id->wordId,
            ]

        ];

        $session->set('curr_word', $curr_word);
        return $select_word_id->wordId;


    }

    public static function countProgressBar($list_id ) {
        $request = Yii::$app->request;
        $session = Yii::$app->session;
        $curr_word = $session->get('curr_word');
        $list_id = (int)$list_id;

        $count_all = Playlistwords::find()
            ->where(['playListId' => $list_id])
            ->count();

        $get_prev_sort = Playlistwords::find()->where(['playListId' => $list_id])->andWhere(['wordId' => $curr_word[$list_id]['prev_word']])->one();

        $count_proideno = Playlistwords::find()->where(['playListId' => $list_id])
            ->andWhere([">", 'sort', $get_prev_sort->sort])->orderBy('sort')->count();

        $odin_procent  = $count_all / 100;
        $count_proideno_procent = $count_proideno / $odin_procent;

        $count_proideno_procent = round($count_proideno_procent);
        $count_proideno_procent = 100 - $count_proideno_procent;
        return $count_proideno_procent;







    }

}