<?php


namespace frontend\controllers;


use common\models\User;
use common\models\UserOptions;
use yii\filters\AccessControl;

class BalanceController extends AppController
{

    const ETERNAL_ACCOUNT = 100;
    const PRO_ACCOUNT = 20;
    const REGULAR_ACCOUNT = 10;

    public function behaviors()
    {
        return [

            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'roles' => ['@'],
                        'allow' => true,
                    ],

                ],
            ],


        ];
    }


    public function beforeAction($action)
    {
        if (in_array($action->id, ['interactions'])) {
            $this->enableCsrfValidation = false;
        }
        return parent::beforeAction($action);
    }


    public function actionAdd() {

        return $this->render('add', [
//            'model' => $model,
        ]);

    }

    public function actionSuccess() {

        return $this->render('success', [
//            'model' => $model,
        ]);

    }


    public function actionDeny() {

        return $this->render('deny', [
//            'model' => $model,
        ]);

    }


    public function actionWait() {

        return $this->render('wait', [
//            'model' => $model,
        ]);

    }

    public static function addBalanceUser($user_id, $summa) {

        $user = UserOptions::findOne($user_id);

        if ($summa > 199 ) { $user->status_account = self::ETERNAL_ACCOUNT;
            $user->subscription_int = $user->subscription_int + 3600*24*365*10;
        }

        if ($summa > 71) { $user->status_account = self::PRO_ACCOUNT;
        $user->subscription_int = $user->subscription_int + 3600*24*365; }

        if ($summa < 72) {
            $stoit_den = 0.33;
            $kol_vo_dney = $summa/$stoit_den;
            $kol_vo_dney = round($kol_vo_dney);

            $user->status_account = self::PRO_ACCOUNT;
            $user->subscription_int = $user->subscription_int + 3600*24*$kol_vo_dney;

        }


        $user->save();







    }

    public static function periodToWord($period) {
        if ($period == 13) { $string = "Вечный аккаунт"; }
        else { $string = $period." мес ";}
        return $string;


    }




}