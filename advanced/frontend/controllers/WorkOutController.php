<?php

namespace frontend\controllers;

use common\models\PlayList;
use frontend\models\forms\OptionsForm;
use Yii;

class WorkOutController extends AppController
{
    public function actionIndex($id)
    {

        $get_list_name = PlayList::findOne($id);
        $scale = ExercisesApiMethodsController::getScale(12);

        $session = Yii::$app->session;
        $id = (int) $id;
        $options = $session->get('options_' . $id . '');
        if (!$options) {
            if (empty($options)) {

                $options = [

                    'type_exercises' => [1,2,3,4,5],
                    'autoPlayWords' => '0',
                    'autoPlayVideo' => '0',
                    'autoPlayAudio' => '1',
                    'pauseBetweenWords' => '1',
                    'portionsOfWords' => 1,


                ];
//                $scale = ExercisesApiController::generatePlayList($id);
            }
        }
        $model = new OptionsForm();

        $model->portionsOfWords = $options["portionsOfWords"];
        $model->pauseBetweenWords = $options["pauseBetweenWords"];
        $model->autoPlayAudio = $options["autoPlayAudio"];
        $model->autoPlayVideo = $options["autoPlayVideo"];
        $model->autoPlayWords = $options["autoPlayWords"];


        if (in_array(1 , $options["type_exercises"])) { $model->type_exercises_1 = 1; }
        else { $model->type_exercises_1 = 0; }
        if (in_array(2 , $options["type_exercises"])) { $model->type_exercises_2 = 1; }
        else { $model->type_exercises_2 = 0; }
        if (in_array(3 , $options["type_exercises"])) { $model->type_exercises_3 = 1; }
        else { $model->type_exercises_3 = 0; }
        if (in_array(4 , $options["type_exercises"])) { $model->type_exercises_4 = 1; }
        else { $model->type_exercises_4 = 0; }
        if (in_array(5 , $options["type_exercises"])) { $model->type_exercises_5 = 1; }
        else { $model->type_exercises_5 = 0; }





        if ($model->load(Yii::$app->request->post())) {
            if ($model->type_exercises_1 == 1 ) { $type_exercises[] = 1; }
            if ($model->type_exercises_2 == 1 ) { $type_exercises[] = 2; }
            if ($model->type_exercises_3 == 1 ) { $type_exercises[] = 3; }
            if ($model->type_exercises_4 == 1 ) { $type_exercises[] = 4; }
            if ($model->type_exercises_5 == 1 ) { $type_exercises[] = 5; }



            $save_to_session = $options = [

                'type_exercises' => $type_exercises,
                'autoPlayWords' => $model->autoPlayWords,
                'autoPlayVideo' => $model->autoPlayVideo,
                'autoPlayAudio' => $model->autoPlayAudio,
                'pauseBetweenWords' => $model->pauseBetweenWords,
                'portionsOfWords' => $model->portionsOfWords,


            ];

            $options = $session->get('options_' . $id . '');
            $session->set('options_' . $id . '', $save_to_session);
            $session->set('bookmark_' . $id . '', 0);
            $bild_new_scale = ExercisesApiController::generatePlayList($id);

            Yii::$app->session->setFlash('success', "Сохранено. Сдвинуто в начало. ");





        }





        return $this->render('index' ,  [
            'model' => $model,
            'scale' => $scale,
            'get_list_name' => $get_list_name,




        ]);
    }

}
