<?php


namespace frontend\controllers;


use common\models\PlayList;
use common\models\Playlistwords;
use common\models\PlaylistwordsTranslate;
use common\models\Translate;
use Yii;
use yii\web\Controller;

class AppController extends Controller
{

    public static function getPregMatchValue ($pattern , $subject) {



        $get_value = preg_match($pattern, $subject, $matches);
        $get_value = $matches[1];

        return $get_value;




    }

    public static function deleteWordFromList($playlistwords_id) {

        $user_id = Yii::$app->user->getId();
        $playlistwords = Playlistwords::findOne($playlistwords_id);
        $get_list = PlayList::findOne($playlistwords->playListId);
        if ($get_list->user_id == $user_id ) {
            $playlistwords_translate = PlaylistwordsTranslate::deleteAll(['playlistwords_id' => $playlistwords_id]);
            $playlistwords->delete();
            return true;
        }
        else { return false; }
    }


    public static function delPlaylistwordsWithTranslates($id) {
        $user_id = Yii::$app->user->getId();

        $play_list = PlayList::findOne($id);
        if ($play_list->user_id == $user_id ) {
        $playlistwords = Playlistwords::find()->where(['playListId' => $id])->all();
        foreach ($playlistwords as $val ) {
            $playlistwords_translate = PlaylistwordsTranslate::deleteAll(['playlistwords_id' => $val->id]);
        }
        $del_playlistwords = Playlistwords::deleteAll(['playListId' => $id]);
        $play_list->delete();
            return true;
        }
        else { return false; }
    }

    public static function getTranslatesUserPick ($id_playlistwords) {

        $chek_translate = PlaylistwordsTranslate::find()->where(['playlistwords_id' => $id_playlistwords])->orderBy('sort')->all();

//        print_r($chek_translate);
//        die();


        if ($chek_translate) {
             unset($trans_us_pick);
            foreach ($chek_translate as $translate_user_pick) {


                $get_translates = Translate::find()->where(['id' => $translate_user_pick->translate_id])->with('partsOfSpeech')->one();




                $trans_us_pick .= "" . $get_translates->value . "<span style='font-size: 16px'>[" . $get_translates->partsOfSpeech->value . "]</span>; ";
            }
//            print_r($trans_us_pick);
//            die();


            return $trans_us_pick;
        }
        else { return false;}


    }

}