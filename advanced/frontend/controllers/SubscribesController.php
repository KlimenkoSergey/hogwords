<?php

namespace frontend\controllers;

use common\models\Subscribes;
use frontend\models\forms\SubscribeForm;
use Yii;

class SubscribesController extends AppController
{
    public function beforeAction($action)
    {
        if (in_array($action->id, ['add'  ])) {
            $this->enableCsrfValidation = false;
        }
        return parent::beforeAction($action);
    }


    public function actionAdd()
    {

        if (isset($_POST["EMAIL"])) {
        $insert_email = new Subscribes();
        $insert_email->email_user = $_POST["EMAIL"];
            $insert_email->status = 10;
            $insert_email->created_at = time();
            $insert_email->save();
            Yii::$app->session->setFlash('success', "Спасибо.  Вы подписаны на рассылку.");

        }



        return $this->render('add');
    }

    public function actionIndex()
    {
        return $this->render('index');
    }

}
