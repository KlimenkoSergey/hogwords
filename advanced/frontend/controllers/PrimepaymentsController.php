<?php

namespace frontend\controllers;

class PrimepaymentsController extends \yii\web\Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionInteractions()
    {
        return $this->render('interactions');
    }

}
