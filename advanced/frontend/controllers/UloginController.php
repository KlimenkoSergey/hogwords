<?php


namespace frontend\controllers;


use common\models\User;
use common\models\UserOptions;
use Yii;

class UloginController extends AppController
{

    public function beforeAction($action)
    {
        if (in_array($action->id, ['index'  ])) {
            $this->enableCsrfValidation = false;
        }
        return parent::beforeAction($action);
    }



    public function actionIndex() {


        $s = file_get_contents('http://ulogin.ru/token.php?token=' . $_POST['token'] . '&host=' . $_SERVER['HTTP_HOST']);



        if (isset($_POST['token'])) {
            $user = json_decode($s, true);

//            print_r($user);

            $if_exist = User::find()->where(['identity' => $user['identity'] ])->andWhere(['network' => $user['network'] ])->one();
            if ($if_exist) {
                $identity = $if_exist;
                Yii::$app->user->login($identity);

            }
            else {

                $username = "".$user['first_name']."_".$user['last_name']."_".$user['uid']."";
                $password = '$2y$13$VMg./Z/pWpa9aEEBfvt.k.PuQo2dgYdhLLXdBuxq39HOwVKjTWBsK';
                $email = "".$user['first_name']."_".$user['last_name']."@".$user['uid']."";

                $insert = new UserOptions();
                $insert->username = $username;
                $insert->auth_key = Yii::$app->security->generateRandomString();
                $insert->password_hash = $password;
                $insert->email = $email;
                $insert->status = 10;
                $insert->created_at = time();
                $insert->updated_at = time();
                $insert->identity = $user['identity'];
                $insert->network = $user['network'];
                $insert->full_name = "".$user['last_name']." ".$user['first_name']."";
//                $insert->save();
                if (!$insert->save()) {var_dump($insert->getErrors()); }


                $identity = User::findOne(['username' => $username]);
                Yii::$app->user->login($identity);



//                $duration = 3600*24*30;
//                Yii::app()->user->login($identity,$duration);






            }

            return $this->redirect('/add-words/select-from-all?page=1&per-page=100');







        }

//        $user['network'] - соц. сеть, через которую авторизовался пользователь
//        $user['identity'] - уникальная строка определяющая конкретного пользователя соц. сети
//        $user['first_name'] - имя пользователя
//        $user['last_name'] - фамилия пользователя




    }

}