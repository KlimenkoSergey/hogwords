<?php

namespace frontend\controllers;

use common\models\PlayList;
use Yii;
use yii\filters\AccessControl;

class MyWordsListController extends AppController
{

    public function behaviors()
    {
        return [

            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'roles' => ['@'],
                        'allow' => true,
                    ],

                ],
            ],


        ];
    }


    public function actionIndex()
    {
        $get_lists = PlayList::find()->where(['user_id' => Yii::$app->user->getId()])->orderBy('sort')->all();


        return $this->render('index',
            [
               'get_lists' => $get_lists,

            ]
            );
    }

}
