<?php
namespace frontend\controllers\blocks\exercises;


class GenDataController extends \frontend\controllers\AppController
{

    public static function genData() {
        $data = ['type_exercises' => self::TYPE_EX_THEORY,
            'options_theory' => [],
            'options_choose_from_four_by_word' => [],
            'options_choose_from_four_by_audio' => [],
            'options_word_byword set' => [],
            'count_step' => [],


            'word_id' => self::getCurrWord(50)
        ];

        $curr_step = [];






        return $data;
    }



    public static function preparationData($select_word)
    {


        $data[] = $select_word->sort;
        $data[] = $select_word->word;
        $data[] = $select_word->transcription;
        $data[] = $select_word->video;
        $data[] = $select_word->status;
        $data[] = $select_word->translate;
        $data[] = $select_word->language_source;
        $data[] = $select_word->language_to;
        $data[] = "translation_options";
        $data[] = "examples";
        $data[] = "definitions";
        $data[] = $select_word->synonyms;
        $data[] = $select_word->translation_options_clean;
        $data[] = $select_word->examples_clean;
        $data[] = $select_word->definitions_clean;
        $data[] = $select_word->id;
        $json = json_encode($data);


        return $json;


    }

}