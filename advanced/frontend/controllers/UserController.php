<?php


namespace frontend\controllers;


use Yii;

class UserController extends AppController
{

    public function actionLogout() {


        Yii::$app->user->logout();

        return $this->goHome();


    }


}