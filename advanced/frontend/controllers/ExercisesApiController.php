<?php


namespace frontend\controllers;


use common\models\Examples;
use common\models\IrregularVerbsWords;
use common\models\Playlistwords;
use common\models\PlaylistwordsTranslate;
use common\models\Translate;
use common\models\Words;
use frontend\controllers\blocks\exercises\GenDataController;
use frontend\models\AppModel;
use Yii;
use yii\filters\AccessControl;

class ExercisesApiController extends AppController
{




    public function behaviors()
    {
        return [

            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'roles' => ['@'],
                        'allow' => true,
                    ],

                ],
            ],


        ];
    }


    public static function getData($cursor, $scale)
    {

        $cursor = (int)$cursor;


        $get_params = $scale[$cursor];
        $type = $get_params['type'];
        $word_id = $get_params['word_id'];
        $id_playlistwords = $get_params['id_playlistwords'];
        $right_answer = $get_params['right_answer'];


        $get_word_obshie = Words::findOne($word_id);
        switch ($type) {
            case 1:



                $response['type'] = 1;
                $response['id'] = $get_word_obshie->id;
                $response['word'] = $get_word_obshie->word;
                if ($get_word_obshie->transcription != '') {
                    $response['transcription'] = $get_word_obshie->transcription;
                }

                if ($get_word_obshie->video != '') {
                    $response['video'] = "/uploads/audio/" . $get_word_obshie->video;
                }


                $chek_translate = PlaylistwordsTranslate::find()->where(['playlistwords_id' => $id_playlistwords])->orderBy('sort')->all();
                // verbs

                $irregular_verbs_words = IrregularVerbsWords::find()->where(['word_id' => $word_id ])->with('irregularVerbs')->one();
                if ($irregular_verbs_words) {




                    $response['irregularVerbs'] = [
                        'infinitive' => $irregular_verbs_words["irregularVerbs"]->infinitive,
                        'transcription_1' => $irregular_verbs_words["irregularVerbs"]->transcription_1,

                        'past_simple_active' => $irregular_verbs_words["irregularVerbs"]->past_simple_active,
                        'transcription_2' => $irregular_verbs_words["irregularVerbs"]->transcription_2,

                        'past_participle' => $irregular_verbs_words["irregularVerbs"]->past_participle,
                        'transcription_3' => $irregular_verbs_words["irregularVerbs"]->transcription_3,

                        'translation' => $irregular_verbs_words["irregularVerbs"]->translation,

                    ];


                }


                if ($chek_translate) {






                    foreach ($chek_translate as $val) {


                        $get_translate = Translate::find()->where(['id' => $val->translate_id])->with('partsOfSpeech', 'examples')->one();
                        $translate_word = $get_translate->value;

                        if ($get_translate["examples"]) {
                            foreach ($get_translate["examples"] as $exam) {
                                $exam->en_text = self::stringRepalce($exam->en_text , $get_word_obshie->word);
                                $exam->ru_text =  self::stringRepalce($exam->ru_text , $translate_word);


                                $exam_response[] = [
                                    'id' => $exam->id,
                                    'en_text' => $exam->en_text,
                                    'ru_text' => $exam->ru_text,

                                ];
                            }
                        }
                        $response['translate_many'][] = ['translate' => $get_translate->value,
                            'id' => $get_translate->id,
                            'partsOfSpeech' => $get_translate["partsOfSpeech"]->value,
                            'examples' => $exam_response,
                        ];

                        unset($exam_response);


                    }
                } else {
                    $response['translate'] = $get_word_obshie->translate;
                }
                break;
            case 2:

                $get_word = $word_id;
                $this_word = Words::findOne($get_word);
                $other_word = GenerateExercisesController::getWordsByEnWord($get_word , $id_playlistwords);
                $chek_translate = PlaylistwordsTranslate::find()->where(['playlistwords_id' => $id_playlistwords])->orderBy('sort')->all();
                if ($chek_translate) { $answer = AppController::getTranslatesUserPick($id_playlistwords);  }
                else {  $answer = $this_word->translate; }

                $this_word = Words::findOne($get_word);


                $response['type'] = 2;
                $response['params'] = [
                    'question' => $this_word->word,
                    'answer' => $answer,
                    'variants' => $other_word,

                ];
                break;
            case 3:

                $get_word = $word_id;
                $other_word = GenerateExercisesController::getWordsByRuWord($get_word , $id_playlistwords);
                $this_word = Words::findOne($get_word);
                $get_trans_us_pick = AppController::getTranslatesUserPick($id_playlistwords);
                if ($get_trans_us_pick === false) { $question = $this_word->translate;  }
                else { $question = $get_trans_us_pick; }


                $answer = $this_word->word;
                $response['type'] = 3;
                $response['params'] = [
                    'question' => $question,
                    'answer' => $answer,
                    'variants' => $other_word,

                ];

                break;
            case 4:
                // 3 на русском  по англискому аудио

//                    $get_word = array_shift($curr_step['words_4']);
                $get_word = $word_id;

                $other_word = GenerateExercisesController::getWordsByEnWord($get_word , $id_playlistwords);
                $this_word = Words::findOne($get_word);

                $chek_translate = PlaylistwordsTranslate::find()->where(['playlistwords_id' => $id_playlistwords])->orderBy('sort')->all();
                if ($chek_translate) { $answer = AppController::getTranslatesUserPick($id_playlistwords);  }
                else {  $answer = $this_word->translate; }
//                $answer = $this_word->translate;

                $response['type'] = 4;
                $response['params'] = [
                    'question' => "uploads/audio_robo/" . $this_word->id . ".mp3",
                    'answer' => $answer,
                    'variants' => $other_word,
                ];
                break;

            case 5:
                $get_word = $word_id;
                $this_word = Words::findOne($get_word);
                $answer = $this_word->word;

                $get_trans_us_pick = AppController::getTranslatesUserPick($id_playlistwords);
                if ($get_trans_us_pick === false) { $question = $this_word->translate;  }
                else { $question = $get_trans_us_pick; }


                $response['type'] = 5;
                $response['params'] = [
                    'question' => $question,
                    'answer' => $answer,
                ];

                break;


            case 6:
                $get_word = $word_id;
                $this_word = Words::findOne($get_word);
                $answer = $this_word->word;

                $response['type'] = 6;
                $response['message'] = 'Поздравляем вы изучили весь список,  <a href="#" id="start_over">начать сначала</a>.';


                $response['params'] = [
                    'question' => $this_word->translate,
                    'answer' => $answer,
                ];

                break;


//
        }

        if ($response['type'] != 6) {
        $response['audio'] = "/uploads/audio_robo/" . $get_word_obshie->id . ".mp3";
        $response['word_id'] = $word_id;
        $response['cursor_id'] = $cursor;
        if ($type != 1) {$response['right_answer_id'] = $right_answer; }}

        if ($type == 1) {
            unset($response['right_answer_id']);
            unset($response['cursor_id']);

              }






//        print_r($response);

//        die();

        return $response;


    }



    public static function getCurrWord($list_id, $playlistwords_id = null)
    {
        if ($playlistwords_id === null) {
            $first_word_id = Playlistwords::find()->where(['playListId' => $list_id])->orderBy('sort')->limit(1)->one();

        } else {

            $first_word_id = Playlistwords::find()->where(['playListId' => $list_id])->andWhere(['>', 'id', $playlistwords_id])->orderBy('sort')->limit(1)->one();


        }
        $get_word = Words::findOne($first_word_id->wordId);
        $array['word'] = $get_word;
        $array['PlaylistwordsId'] = $first_word_id->id;

        if (!$first_word_id) {
            $array['word'] = null;
            $array['PlaylistwordsId'] = null;

        }


        return $array;


    }

    public static function generatePlayList($list_id)
    {

        $session = Yii::$app->session;
        $get_options = $session->get('options_' . $list_id . '');
        if (!$get_options) {

            $get_options = [
                'type_exercises' => [1,2,3,4,5],
                'autoPlayWords' => '0',
                'autoPlayVideo' => '0',
                'autoPlayAudio' => '1',
                'pauseBetweenWords' => '0',
                'portionsOfWords' => 1,

            ];
        }


//        print_r($get_options["type_exercises"]);
//        die();

        $get_playlistwords = Playlistwords::find()->where(['playListId' => $list_id])->orderBy('sort')->asArray()->all();
        if ($get_playlistwords) {

            $delim_po_kuskam = array_chunk($get_playlistwords, $get_options["portionsOfWords"]);
            // Мотаем по куска слов

            $key = 0;


            foreach ($delim_po_kuskam as $po_nesk_slov) {




                // идем по типу заданий

//                unset($right_answer);

                foreach ($get_options["type_exercises"] as $type) {


                    if ($type != 1) {
                        shuffle($po_nesk_slov);

                    }

//                    print_r($po_nesk_slov);
//
//                    die();

                    $counter_words = 1;
                    // идем по конкретному слову
                    foreach ($po_nesk_slov as $word) {
                        if ($type == 1 ) {
//                            $right_answer_array[] = $po_nesk_slov;
                            $right_answer[$word["wordId"]] = $key; }

                        $scale[] = ['type' => $type,
                            'word_id' => $word["wordId"],
                            'playListId' => $word["playListId"],
                            'id_playlistwords' => $word["id"],
                            'sort' => $word["sort"],
                            'right_answer' => $right_answer[$word["wordId"]],
                            'key' => $key,
                            'counter_words' => $counter_words,


                        ];
                        $key = $key + 1;
                        $counter_words = $counter_words + 1;

                    }

                }

            }

            $scale[] = [
                'type' => 6,
                'message' => 'Поздравляем вы изучили весь список,  <a href="#" id="start_over">начать сначала</a>.',
//                'autoPlayVideo' => '0',
//                'autoPlayAudio' => '1',
//                'pauseBetweenWords' => '0',
//                'portionsOfWords' => 5,

            ];

//            print_r($scale);
//            die();
            $session = Yii::$app->session;
            $session->set('scale_' . $list_id . '', $scale);
            return $scale;

        }

    }

    public static function stringRepalce ($string , $translate_word) {
        $string = trim($string);

        $patern = array("&#x27;");
        $string = str_replace($patern, "'", $string);


        $string2 = str_replace($translate_word, "<i>".$translate_word."</i>", $string);

        return $string2;



    }

}