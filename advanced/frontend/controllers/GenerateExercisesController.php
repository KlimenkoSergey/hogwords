<?php


namespace frontend\controllers;


use common\models\PlaylistwordsTranslate;
use common\models\Translate;
use common\models\Words;
use common\models\WordsContsTranslates;
use yii\db\Expression;
use yii\filters\AccessControl;

class GenerateExercisesController extends AppController
{

    public function behaviors()
    {
        return [

            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'roles' => ['@'],
                        'allow' => true,
                    ],

                ],
            ],


        ];
    }

    public function actionGenerateListenEnglishPickRussian () {

        $get_words = Words::find()->limit(1)->all();

        foreach ($get_words as $val ) {
            $dlina_stroki = strlen($val->word);
            echo $val->word;
            echo $dlina_stroki;
            echo "<br>";
            $other_words = self::getWordsByLength($dlina_stroki);

            foreach ($other_words as $insert_word) { }

        }


//        foreach ($other_word1 as $val ) {
//
//            echo $val->word; echo "<br>";
//
//        }

    die();
    }


//    public static function getWordsByEnWord($word_id) {
//        $word = Words::findOne($word_id);
//        $dlina_stroki = strlen($word->word);
//
//        $other_words = self::getWordsByLength($dlina_stroki , 1);
//        $other_words[] = $word->translate;
//        shuffle($other_words);
//
//        return $other_words;
//
//    }


    public static function getWordsByEnWord($word_id , $id_playlistwords) {


        $word = Words::findOne($word_id);
        $chek_translate = PlaylistwordsTranslate::find()->where(['playlistwords_id' => $id_playlistwords])->orderBy('sort')->all();
        if ($chek_translate) {  $count_translte = count($chek_translate);
            $other_words = self::getWordsWithTranslate($count_translte);
            unset($trans_us_pick);
            foreach ($chek_translate as $translate_user_pick) {
                $get_translates = Translate::find()->where(['id' => $translate_user_pick->translate_id ])->with('partsOfSpeech')->one();
                $trans_us_pick .= "".$get_translates->value."<span style='font-size: 16px'>[".$get_translates->partsOfSpeech->value."]</span>; ";



            }



            $other_words[] = $trans_us_pick;
            shuffle($other_words);
            return $other_words;
        }
        else {
                    $dlina_stroki = strlen($word->word);

        $other_words = self::getWordsByLength($dlina_stroki , 1);
        $other_words[] = $word->translate;
        shuffle($other_words);

        return $other_words;
 }





//        return $other_words;


    }

//    public function actionFillTable() {
//        $words = Words::find()->with('translates')->all();
////        print_r($words);
//
//        foreach ($words as $word)  {
//
//            $count_translates = count($word["translates"]);
//
////            var_dump($word["translates"]);
//            if ($count_translates > 0 ) {
//                $insert = new WordsContsTranslates();
//                $insert->word_id = $word->id;
//                $insert->count_translates = $count_translates;
//                $insert->save();
//
//                echo $word->id; echo " - "; echo $count_translates; echo "<br>";
//
//
//
//
//            }
//
//
//
//        }
//
//
//
//
//
//    die();
//    }

    public static function getWordsWithTranslate($count_translte) {

        $words_conts_translates = WordsContsTranslates::find()->where([ '>', 'count_translates', $count_translte ])
            ->orderBy(new Expression('rand()'))
            ->limit(3)->all();
//        print_r($words_conts_translates);
//
//        die();
        foreach ($words_conts_translates as $val ) {


            $word = Words::find()->with('translates' , 'translates.partsOfSpeech' )
            ->where(['id' => $val->word_id])->one();

//            print_r($word->translates);
//            die();



            $count_tr = 1;
//            $return_array[] = ['words' => $val->word ];
            unset($translate_list);
            foreach ($word->translates as $translate) {
//                print_r();
//                die();

                $translate_list .= "".$translate->value."<span style='font-size: 16px'>[".$translate->partsOfSpeech->value."]</span>; ";

                if ( $count_tr >= $count_translte ) { break; }
                $count_tr = $count_tr + 1;


            }

            $words_return[] =  $translate_list;

        }




        return $words_return;



    }


    public static function getWordsByRuWord($word_id  ) {
        $word = Words::findOne($word_id);
        $dlina_stroki = strlen($word->word);

        $other_words = self::getWordsByLength($dlina_stroki , 2 );
        $other_words[] = $word->word;
        shuffle($other_words);
        return $other_words;

    }

    public static function getWordsByEnAudio($word_id  ) {
        $word = Words::findOne($word_id);
        $dlina_stroki = strlen($word->word);

        $other_words = self::getWordsByLength($dlina_stroki , 1 );
        $other_words[] = $word->translate;
        shuffle($other_words);

        return $other_words;

    }





//    public function actionViewAllWords () {
//        $words = Words::find()->all();
//
//        foreach ($words as $val) {
//
//            echo $val->id; echo " - ";   echo $val->word; echo "<br>";
//
//        }
//
//        die();
//
//
//    }

    public static function getWordsByLength($length, $en_or_ru) {
        if ($length == 1) { $length = 2; }

        if ($length < 4) { $length_end = 1; }
        else {$length_end = 4;}


//        echo "<br>";

        $other_word1 = (new \yii\db\Query())
            ->select(['word' ,'translate'])
            ->from('words')
            ->where(['<=', 'CHAR_LENGTH(word)', $length])
            ->andWhere(['>=', 'CHAR_LENGTH(word)', $length_end])->limit(50)
            ->all();

            if($length > 4) { $rand_select = rand(1,12000);

                $other_word1 = (new \yii\db\Query())
                    ->select(['word' ,'translate'])
                    ->from('words')
                    ->where(['<=', 'CHAR_LENGTH(word)', $length])
                    ->andWhere(['>=', 'CHAR_LENGTH(word)', $length_end])->limit(50)
                    ->andWhere(['>', 'id', $rand_select])
                    ->all();



            }


//        $other_word1->limit(50)
//            ->all();

        $rand_keys = array_rand($other_word1, 3);
//        print_r($rand_keys) ;

        foreach ($rand_keys as $value) {
            if ($en_or_ru == 1 ) { $arraychik[] = $other_word1[$value]["translate"]; }
            else { $arraychik[] = $other_word1[$value]["word"];  }


//            echo $other_word1[$value]["word"]; echo "<br>";

        }


//        echo count($other_word1);
//        echo "<br>";
//
//        foreach ($other_word1 as  $val) {
//
////            print_r($val);
//            echo $val["word"]; echo "<br>";
//
//        }




       return $arraychik;





    }

}