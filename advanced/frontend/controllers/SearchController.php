<?php


namespace frontend\controllers;


use common\models\Pages;

class SearchController extends AppController
{

    public function actionIndex ($s) {

        $s = htmlspecialchars($s);

        $model = Pages::find()->where(['like', 'content', $s])->all();

        return $this->render('index',
            [
                'model' => $model,
                's' => $s,
            ]
        );




    }

}