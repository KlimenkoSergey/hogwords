<?php


namespace frontend\models\forms;


use frontend\models\AppModel;

class SelectFromAllForm extends AppModel
{

    public $play_list_name;


    public function rules()
    {
        return [

            [['play_list_name' ], 'required'],
            [['play_list_name'], 'string', 'max' => 255],
//            [['words'], 'string', 'max' => 15000],

        ];
    }


    public function attributeLabels()
    {
        return [
//            'words' => 'Слово',
            'play_list_name' => 'Название списка',
        ];
    }


}