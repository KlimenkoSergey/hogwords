<?php
namespace frontend\models\forms;

use frontend\models\AppModel;


class AddWordsListForm extends AppModel
{

    public $words;
    public $play_list_name;

    public function rules()
    {
        return [

            [['words' , 'play_list_name'], 'required'],
            [['play_list_name'], 'string', 'max' => 255],
            [['words'], 'string', 'max' => 15000],

        ];
    }


    public function attributeLabels()
    {
        return [
            'words' => 'Слова (В одной строчке одно слово)',
            'play_list_name' => 'Название плейлиста',
        ];
    }



}