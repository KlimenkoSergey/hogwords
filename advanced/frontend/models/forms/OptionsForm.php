<?php


namespace frontend\models\forms;


use frontend\models\AppModel;

class OptionsForm extends AppModel
{
    public $type_exercises_1;
    public $type_exercises_2;
    public $type_exercises_3;
    public $type_exercises_4;
    public $type_exercises_5;
    public $autoPlayWords;
    public $autoPlayVideo;
    public $autoPlayAudio;
    public $pauseBetweenWords;
    public $portionsOfWords;



    public function rules()
    {
        return [

//            [['play_list_name' ], 'required'],
            [['autoPlayWords', 'autoPlayVideo', 'autoPlayAudio' , 'pauseBetweenWords' , 'portionsOfWords',
                'type_exercises_1','type_exercises_2','type_exercises_3','type_exercises_4','type_exercises_5',
                ], 'integer'],
            ['portionsOfWords', 'integer', 'min' => 1],
            ['portionsOfWords', 'integer', 'max' => 50],
            ['pauseBetweenWords', 'integer', 'max' => 300],



        ];
    }


    public function attributeLabels()
    {
        return [

            'type_exercises_1' => 'Упражнение - Теория',
            'type_exercises_2' => 'Упражнение - с английского на русский, тесты ',
            'type_exercises_3' => 'Упражнение - с русского на английский, тесты ',
            'type_exercises_4' => 'Упражнение - Аудирование,  с английского на русский, тесты  ',
            'type_exercises_5' => 'Упражнение - Письмо,  с русского на английский',
            'autoPlayWords' => 'Автопролистывание слов',
            'autoPlayVideo' => 'Автовоспроизведение  видео',
            'autoPlayAudio' => 'Включить / отключить звук ',
            'pauseBetweenWords' => 'Пауза между словами (сек)',
            'portionsOfWords' => 'По сколько слов брать',
        ];
    }

}