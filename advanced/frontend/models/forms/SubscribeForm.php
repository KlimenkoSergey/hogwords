<?php


namespace frontend\models\forms;


use frontend\models\AppModel;

class SubscribeForm extends AppModel
{
    public $count_mounths;
    public $cashbox;

    public function rules()
    {
        return [

            [['count_mounths' ,'cashbox' ], 'required'],
            [['count_mounths' , 'cashbox'], 'integer'],
            ['count_mounths', 'integer', 'min' => 1],
            ['count_mounths', 'integer', 'max' => 100],




        ];
    }


    public function attributeLabels()
    {
        return [

            'count_mounths' => 'Кол-во месяцев',
            'cashbox' => 'Выберите кассу',

        ];
    }



}