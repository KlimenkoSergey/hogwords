<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use vova07\imperavi\Widget;


/* @var $this yii\web\View */
/* @var $model common\models\Words */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="words-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

<!--    --><?//= $form->field($model, 'sort')->textInput() ?>

    <?= $form->field($model, 'word')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'transcription')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'video')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'status')->textInput() ?>

    <?= $form->field($model, 'translate')->textInput(['maxlength' => true]) ?>

<!--    --><?//= $form->field($model, 'language_source')->textInput() ?>
<!---->
<!--    --><?//= $form->field($model, 'language_to')->textInput() ?>

<!--    --><?//= $form->field($model, 'translation_options')->textarea(['rows' => 6]) ?>
<!---->
<!--    --><?//= $form->field($model, 'examples')->textarea(['rows' => 6]) ?>
<!---->
<!--    --><?//= $form->field($model, 'definitions')->textarea(['rows' => 6]) ?>
<!---->
<!--    --><?//= $form->field($model, 'synonyms')->textarea(['rows' => 6]) ?>

    <?php echo $form->field($model, 'synonyms')->widget(Widget::className(), [
        'settings' => [
            'lang' => 'ru',
            'minHeight' => 200,
            'plugins' => [
                'clips',
                'fullscreen',
            ],
            'clips' => [
                ['Lorem ipsum...', 'Lorem...'],
                ['red', '<span class="label-red">red</span>'],
                ['green', '<span class="label-green">green</span>'],
                ['blue', '<span class="label-blue">blue</span>'],
            ],
        ],
    ]);  ?>

    <?= $form->field($model, 'file')->fileInput() ?>

    <?php echo $form->field($model, 'del_file')
        ->checkbox([
//            'label' => 'Неактивный чекбокс',
            'labelOptions' => [
                'style' => 'padding-left:20px;'
            ],
//            'disabled' => true
        ]); ?>


    <?= $form->field($model, 'translation_options_clean')->textarea(['rows' => 6]) ?>



    <?= $form->field($model, 'examples_clean')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'definitions_clean')->textarea(['rows' => 6]) ?>




    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
