<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\search\WordsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="words-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'sort') ?>

    <?= $form->field($model, 'word') ?>

    <?= $form->field($model, 'transcription') ?>

    <?= $form->field($model, 'video') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'translate') ?>

    <?php // echo $form->field($model, 'language_source') ?>

    <?php // echo $form->field($model, 'language_to') ?>

    <?php // echo $form->field($model, 'translation_options') ?>

    <?php // echo $form->field($model, 'examples') ?>

    <?php // echo $form->field($model, 'definitions') ?>

    <?php // echo $form->field($model, 'synonyms') ?>

    <?php // echo $form->field($model, 'translation_options_clean') ?>

    <?php // echo $form->field($model, 'examples_clean') ?>

    <?php // echo $form->field($model, 'definitions_clean') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
