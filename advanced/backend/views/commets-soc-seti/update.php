<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\CommetsSocSeti */

$this->title = 'Update Commets Soc Seti: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Commets Soc Setis', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="commets-soc-seti-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
