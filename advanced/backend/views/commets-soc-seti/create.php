<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\CommetsSocSeti */

$this->title = 'Create Commets Soc Seti';
$this->params['breadcrumbs'][] = ['label' => 'Commets Soc Setis', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="commets-soc-seti-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
