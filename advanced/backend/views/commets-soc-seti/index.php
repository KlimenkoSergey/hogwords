<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\CommetsSocSetiSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Commets Soc Setis';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="commets-soc-seti-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Commets Soc Seti', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'comments:ntext',
            'input1',
            'input2',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
