<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\CommetsSocSeti */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="commets-soc-seti-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'comments')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'input1')->dropDownList([ 'Мальчик' => 'Мальчик', 'Девочка' => 'Девочка', 'Мужчина' => 'Мужчина', 'Женщина' => 'Женщина', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'input2')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
