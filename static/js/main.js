$(document).ready(function() {
	setTimeout(function() {
		let preload = $("#preloader");
		preload.css("display", "none");
		$("body").removeClass("ovf_hidden");
	}, 1000);
	
	$(".video_block").on("click", function(e) {
		e.stopPropagation();
	});
	$(".stm_fancy-iframe").on("click", function(e) {
		e.stopPropagation();
	});
	$("stm_slider_play").on("click", function(e) {
		e.stopPropagation();
	});
	$(".fancybox-content").on("click", function(e) {
		e.stopPropagation();
	});
	$(".video_block iframe").on("click", function(e) {
		e.stopPropagation();
	});
	
	$(".pv-b").on("click", function() {
		var url = $(this).parent(".play_video-but").data("url");
		
		$(".video-background").fadeIn(300)
		$(".video-background").css("display", "flex")
		
		$("html,body").css("overflow","hidden");
		
		$(".video-background .video_block").append('<iframe width="956" height="538" src="'+url+'" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>');
	});
	
	$(".video-background").on("click", function() {
		$(this).fadeOut(300);
		$(".video-background .video_block").empty();
		
		$("html,body").css("overflow","auto");
	});
});