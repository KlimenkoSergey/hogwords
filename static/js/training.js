$(document).ready(init);

const DEFAULT_SECONDS = 40;

let trainingId, options, timer, audio, player, $screen, $progressBar;

function init() {
	trainingId = getTrainingId();

	audio = new Audio();

	$screen = $('.training-screen');
	$progressBar = $('.progress_bar');

	viewProgressBar(trainingId);

	$.get('/exercises-api-methods/get-options', { id: trainingId },
		data => {
			options = data;
			options.lettersIsShow = true;

			$.get('/exercises-api-methods/current', { id: trainingId },
				data => {
					viewTraining(data, options);
				}
			);
		}
	);

	$('.training-btn-options').click(() => {
		$('.training-options').toggle();
	});

	$('.training-controls-item').click(function () {
		clearInterval(timer);

		let apiMethod = $(this).data('api-method');

		$.get(`/exercises-api-methods/${apiMethod}`, { id: trainingId },
			data => {
				viewTraining(data, options);
			}
		);
	});

	$screen.on('click', '.training-translates-item', function () {
		let wordId = $(this).data('word_id');

		$('.training-examples').hide();
		$(`.training-examples[data-for="${wordId}"]`).show();

		$('.training-examples-controls').hide();
	});

	$screen.on('click', '.training-btn-play-audio', function () {
		audio.play();
	});

	$screen.on('click', '#start_over', function () {
		$('.training-controls-item[data-api-method="beginning"]').trigger('click');
	});

	// $screen.on('click', '.training-video', function () {
	// 	let $video = $(this).find('.training-video-item');

	// 	$(this).find('.training-video-overlay').hide();

	// 	$video.on('ended', () => {
	// 		$(this).find('.training-video-overlay').show();			
	// 	});

	// 	$video[0].play();
	// });

	$screen.on('click', '.training-choices-item', function () {
		let answer = $(this).data('answer');
		let variant = $(this).data('variant');
		let rightAnswerId =  $(this).data('right_answer_id');

		if (variant == answer) {
			$(this).addClass('training-item_correct');
			
			$('.training-btn-play-audio').trigger('click');

			setTimeout(() => {	
				$('.training-controls-item[data-api-method="next"]').trigger('click');
			}, (audio.currentTime ?? 0) + 1000);
		} else {
			$(this).addClass('training-item_incorrect');

			$(`.training-choices-item[data-variant="${answer}"]`).addClass('training-item_correct');

			setTimeout(() => {
				$.get(`/exercises-api-methods/answer?id=${trainingId}&cursor_id=${rightAnswerId}`,
					data => {
						viewTraining(data, options);
					}
				);
			}, 1000);
		}

		clearInterval(timer);
	});

	$screen.on('click', '.training-examples-show', function () {
		$(this).hide();
		$('.training-examples-hide').show();

		$('.training-examples-table').removeClass('training-examples-table_more');
	});

	$screen.on('click', '.training-examples-hide', function () {
		$(this).hide();
		$('.training-examples-show').show();

		$('.training-examples-table').addClass('training-examples-table_more');
	});

	$screen.on('click', '.training-letters-hide', function (e) {
		e.preventDefault();

		$(this).hide();
		$('.training-letters-show').show();

		$('.training-letters').hide();

		options.lettersIsShow = false;
	});

	$screen.on('click', '.training-letters-show', function (e) {
		e.preventDefault();

		$(this).hide();
		$('.training-letters-hide').show();

		$('.training-letters').show();

		options.lettersIsShow = true;
	});

	$screen.on('click', '.training-letters .training-letters-item', function () {
		$(this).clone().appendTo('.training-letters-area');
		$(this).addClass('training-letters-item_active');

		trainingLettersCallback();
	});

	$(document).keyup((e) => {
		let letter = e.key;

		if (new RegExp('backspace', 'i').test(letter)) {
			console.log(123);
			$('.training-letters-area .training-letters-item').eq(-1).trigger('click');
		}

		if (letter.length > 1) return false;

		if (options.lettersIsShow) {
			let $letter = $(`.training-letters .training-letters-item[data-letter=${letter}]:not(.training-letters-item_active)`).eq(0);

			$letter.trigger('click');
		} else {
			$('.training-letters-area').append(renderLetter(letter));
			$(`.training-letters .training-letters-item[data-letter=${letter}]:not(.training-letters-item_active)`).eq(0).addClass('training-letters-item_active');
		
			trainingLettersCallback();
		}
	});

	$screen.on('click', '.training-letters-area .training-letters-item', function () {
		let letter = $(this).data('letter');

		$(this).remove();
		$(`.training-letters-item[data-letter=${letter}]:is(.training-letters-item_active)`).eq(0).removeClass('training-letters-item_active');
	});
}

function trainingLettersCallback() {
	let $area = $('.training-letters-area');

	let rightAnswerId = $area.data('right_answer_id');

	let answer = $area.data('answer');
	
	if ($area.children().length === answer.length) {
		$area.children().each((i, item) => {
			let $item = $(item);
			let letter = $item.data('letter');
			
			if (letter == answer[i]) {
				$item.addClass('training-item_correct');
			} else {
				$item.addClass('training-item_incorrect');
			}
		});

		clearInterval(timer);

		if ($area.find('.training-item_incorrect').length > 2) {
			$area.addClass('training-letters-area_active');

			setTimeout(() => {
					$.get(`/exercises-api-methods/answer?id=${trainingId}&cursor_id=${rightAnswerId}`,
						data => {
							viewTraining(data, options);
						}
					);
			}, 1000);
		} else {
			$('.training-btn-play-audio').trigger('click');

			setTimeout(() => {
				$('.training-controls-item[data-api-method="next"]').trigger('click');
			}, (audio.currentTime ?? 0) + 1000);
		}	
	}
}

function getTrainingId() {
	let searchParams = new URLSearchParams(window.location.search);

	return searchParams.has('id') ? searchParams.get('id') : 1; 
}

function viewTraining(data, options) {
	$screen.html('');

	if (data.type !== 4) {
		if (options.autoPlayAudio == 0) {
			delete data.audio;
		}
	}

	console.log(data);

	switch (data.type) {
		case 1:
			$screen.html(renderTrainingType1(data));

			$('[data-api-method="prev"], [data-api-method="next"]').show();
			$('.training-controls').show();
			break;
		case 2:
		case 3:
		case 4:
			data.timer = renderTimer(DEFAULT_SECONDS);

			$screen.html(renderTrainingType(data));
			break;
		case 5:
			data.timer = renderTimer(DEFAULT_SECONDS);

			$screen.html(renderTrainingType5(data));
			break;
		case 6:
			$screen.html(renderTrainingType6(data));

			$('.training-controls').hide();
			break;
	}

	if (data.audio) {
		audio.src = data.audio;
	}

	if (data.video) {
		player = new Playerjs({ id: 'training-video', file: data.video });

		if (options.autoPlayVideo == 1 && options.autoPlayAudio == 1) {
			audio.addEventListener('ended', function() {
				player.api('play');
			});
		} else {
			if (options.autoPlayVideo == 1) {
				player.api('play');
			}
		}
	}

	if (data.type !== 1 && data.type !== 6) {
		$('[data-api-method="prev"], [data-api-method="next"]').hide();

		let timerSeconds = DEFAULT_SECONDS;

		if (data.type === 5) {
			if (options.lettersIsShow === false) {
				$('.training-letters').hide();
			}
		}

		timer = setInterval(() => {
			timerSeconds--;

			$('.training-timer-seconds').text(timerSeconds);

			if (timerSeconds !== 0) return;

			clearInterval(timer);

			let rightAnswerId;

			if (data.type === 5) {
				let $area = $('.training-letters-area');

				rightAnswerId = $area.data('right_answer_id');

				let answer = $area.data('answer');
				
				$area.children().each((i, item) => {
					let $item = $(item);
					let letter = $item.data('letter');
					
					if (letter == answer[i]) {
						$item.addClass('training-item_correct');
					} else {
						$item.addClass('training-item_incorrect');
					}
				});
			} else {
				let $items = $('.training-choices-item');
				let answer = $items.data('answer');
				
				rightAnswerId = $items.data('right_answer_id');

				$items.each((i, item) => {
					let $item = $(item);
					let variant = $item.data('variant');

					if (variant == answer) {
						$item.addClass('training-item_correct');
					} else {
						$item.addClass('training-item_incorrect');
					}
				});
			}

			setTimeout(() => {
				$.get(`/exercises-api-methods/answer?id=${trainingId}&cursor_id=${rightAnswerId}`,
					data => {
						viewTraining(data, options);
					}
				);
			}, 1000);
		}, 1000);
	}

	$('html, body').animate({scrollTop: $progressBar.offset().top}, 500, 'swing', () => {
		// setTimeout(() => { $('.training-btn-play-audio').trigger('click'); }, 500);

		if (data.type !== 3 && data.type !== 5) {
			$('.training-btn-play-audio').trigger('click');
		}
	});

	viewProgressBar(trainingId);
}

function viewProgressBar(trainingId) {
	$.get('/exercises-api-methods/count-progress-bar', { id: trainingId },
		data => {
			$(".vc_bar_val").text(`${data}%`);
			$('.vc_bar').attr('data-percentage-value', data);
			$('.vc_bar').attr('style', `width: ${data}%;`);
			$('.vc_bar').attr('data-value', data);

			if (data == 99) {
				$('.vc_bar').css('border-radius', '30px');
			}
		}
	);
}