$(document).ready(function() {
  const basePageUrl = $(location).attr('search');
  const basePageId = Number(basePageUrl.split('=')[1]);

  const theory = $('.itheory__wrap');
  const engToRus = $('.iengToRus');
  const rusToEng = $('.irusToEng');
  const audio = $('.iaudio');
  const write = $('.iwrite');

  let step = 0;
  let data = {};
  let theoryWordId = '';
  let rightAnswerId = '';

  function hideBlock(element) {element.css('display', 'none')};
  function showBlock(element, prop) {element.css('display', prop)};

  async function getAllSettings() {
    let response = null;

    await $.ajax({
      type: 'GET',
      url: `/exercises-api-methods/get-options?id=${basePageId}`,
      success: function (result) {
        response = {...result};
      }
    });

    return response;
  }

  async function getProgressBar() {
    let response = null;

    await $.ajax({
      type: 'GET',
      url: `/exercises-api-methods/count-progress-bar?id=${basePageId}`,
      success: function (result) {
        response = {...result};
      }
    });

    return response;
  }

  let mainSettings = getAllSettings();
  let mainProgressBar = getProgressBar();

  async function next() {
    await $.ajax({
      type: 'GET',
      url: `/exercises-api-methods/next?id=${basePageId}`,
      success: function (result) {
        step = result.type;
        conditions(result);
      }
    })
  }

  function autoRun() {
    mainSettings.then(result => {
      if (result.autoPlayWords === '1') {
        setTimeout(async () => {await next()}, Number(result.pauseBetweenWords)*1000);
      }
    });
  }

  $('.iwrite-showAnswer').click(function() {
    $.ajax({
      type: 'GET',
      url: `/exercises-api-methods/answer?id=${basePageId}&cursor_id=${rightAnswerId}`,
      success: function(result) {
        step = result.type;
        conditions(result);
        console.log(result);
      }
    });
  });

  $('.irusToEng-showAnswer').click(function() {
    $.ajax({
      type: 'GET',
      url: `/exercises-api-methods/answer?id=${basePageId}&cursor_id=${rightAnswerId}`,
      success: function(result) {
        step = result.type;
        conditions(result);
      }
    });
  });

  $('.iengToRus-showAnswer').click(function() {
    $.ajax({
      type: 'GET',
      url: `/exercises-api-methods/answer?id=${basePageId}&cursor_id=${rightAnswerId}`,
      success: function(result) {
        step = result.type;
        conditions(result);
      }
    });
  });

  $('.iaudio-showAnswer').click(function() {
    $.ajax({
      type: 'GET',
      url: `/exercises-api-methods/answer?id=${basePageId}&cursor_id=${rightAnswerId}`,
      success: function(result) {
        step = result.type;
        conditions(result);
      }
    });
  });

  $('.itheory__btns-next').click(async function() {
    await next();
  });

  $('.itheory__btns-prev').click(function() {
    $.ajax({
      type: 'GET',
      url: `/exercises-api-methods/prev?id=${basePageId}`,
      success: function(result) {
        step = result.type;
        conditions(result);
      }
    })
  });

  $('.itheory__btns-mix').click(function() {
    $('.itheory__btns-mix').text('Загрузка, пожалуйста подождите...');
    $.ajax({
      type: 'GET',
      url: `/exercises-api-methods/mix-and-start?id=${basePageId}`,
      success: function(result) {
        step = result.type;
        conditions(result);
        Swal.fire({
          title: 'Список успешно перемешан',
          text: 'Хотите продолжить?',
          icon: 'success',
          confirmButtonText: 'Продолжить',
          width: 450,
        });
        $('.itheory__btns-mix').text('Перемешать список и начать сначала');
      }
    })
  });

  $('.itheory__btns-start').click(function() {
    $.ajax({
      type: 'GET',
      url: `/exercises-api-methods/beginning?id=${basePageId}`,
      success: function(result) {
        step = result.type;
        conditions(result);
      }
    })
  });

  $('.itheory__btns-delete').click(function() {
    $('.itheory__btns-delete').text('Загрузка...');
    $.ajax({
      type: 'GET',
      url: `/exercises-api-methods/delete?id=${basePageId}&word_id=${theoryWordId}`,
      success: async function(result) {
        step = result.type;
        conditions(result);
        await next();
        $.ajax({
          type: 'GET',
          url: `/exercises-api-methods/prev?id=${basePageId}`,
          success: function(result) {
            step = result.type;
            conditions(result);
            Swal.fire({
              title: 'Слово успешно удалено',
              text: 'Хотите продолжить?',
              icon: 'success',
              confirmButtonText: 'Продолжить',
              width: 450,
            });
            $('.itheory__btns-delete').text('Удалить слово из списка');
          }
        })
      }
    })
  });

  $('.idone-start').click(function() {
    $.ajax({
      type: 'GET',
      url: `/exercises-api-methods/beginning?id=${basePageId}`,
      success: function(result) {
        step = result.type;
        conditions(result);
      }
    })
  });

  function notActiveSystem(mainElement, rightAnswerIndex) {
    $(`.${mainElement}__wrap-item`).each(function() {
      $(this).removeClass('iengToRus-wrong iengToRus-right');
    });

    const rightElement = document.querySelectorAll(`.${mainElement}__wrap-item`)[rightAnswerIndex];

    $(rightElement).addClass('iengToRus-right');

    document.querySelectorAll(`.${mainElement}__wrap-item`).forEach((item, i) => {
      if (i !== rightAnswerIndex) {$(item).addClass('iengToRus-wrong')};
    });

    setTimeout(() => {
      $.ajax({
        type: 'GET',
        url: `/exercises-api-methods/answer?id=${basePageId}&cursor_id=${rightAnswerId}`,
        success: function(result) {
          step = result.type;
          conditions(result);
        }
      });
    }, 2000);
  }

  function renderTheory(data) {
    showBlock(theory, 'block');
    hideBlock(engToRus);
    hideBlock(rusToEng);
    hideBlock(audio);
    hideBlock(write);

    $('.idone').css('display', 'none');

    $('.hourglass__wrap').css('opacity', '0');

    theoryWordId = data.word_id;
    theoryData = {...data};

    console.log(data);

    mainProgressBar.then(result => {

      $(".vc_bar_val").html(result[0] + "%");
      $('.vc_bar').attr('data-percentage-value', result[0]);
      $('.vc_bar').attr('style', "min-width: 3em; width: " + result[0] + "%;");
      $('.vc_bar').attr('data-value', result[0]);


      // $('.iprogress__bar-inner').css('width', `${result[0]}%`);
      // $('.vc_bar_val').text(`${result[0]}%`);

      const audio = new Audio(data.audio);

      $('.itheory-audio').remove();
      $('.iaudio-listen').remove();
      const audioBtn = document.createElement('img');
      audioBtn.src = "/static/img/gromko.png";
      $(audioBtn).addClass('itheory-audio');
      $('.itheory__wrap-listen').append(audioBtn);
      $('.itheory-audio').click(function () {audio.play()});

      $('.itheory__li-p').text(data.word);
      $('.itheory__video-item').attr('src', data.video);

      if (!data.transcription) {
        $('.itheory-transcription').css('display', 'none');
        $('.itheory-transcription-title').css('display', 'none');
      } else {
        $('.itheory-transcription').css('display', 'block');
        $('.itheory-transcription-title').css('display', 'block');
        $('.itheory-transcription').css('text-align', 'center');
        $('.itheory-transcription-title').css('text-align', 'center');
        $('.itheory-transcription').text(data.transcription);
      }

      $('.itheory-translate p').each(function() {
        $(this).remove();
      });

      $('.itable').css('display', 'none');

      if (data.translate_many) {
        for (let index = 0; index < data.translate_many.length; index++) {
          const p = document.createElement('p');
          $(p).text(`${data.translate_many[index].translate}`);
          const span = document.createElement('span');
          $(span).text(` [${data.translate_many[index].partsOfSpeech}]`);
          $(p).append(span);
          $('.itheory-translate').append(p);
  
          $(p).css('cursor', 'pointer');

          $('.itable tr').each(function() {
            $(this).remove();
          });
  
          $(p).click(function() {
            $('.itable').css('display', 'block');

            $('.itable tr').each(function() {
              $(this).remove();
            });
  
            $('.itable').html(`<tr><th>Английский</th><th>Русский</th></tr>`);
  
            const tableData = data.translate_many[index].examples;
  
            for (let j = 0; j < tableData.length; j++) {
              const tr = document.createElement('tr');
              const tdEng = document.createElement('td');
              const tdRus = document.createElement('td');
  
              // $(tdEng).css('border', '1px solid rgba(0,0,0,.15)');
              // $(tdRus).css('border', '1px solid rgba(0,0,0,.15)');
              // $(tdEng).css('border-left', '1px solid rgba(0,0,0,.15)');
              // $(tdRus).css('border-right', '0px solid rgba(0,0,0,.15)');
              $(tdEng).css('padding', '15px');
              $(tdRus).css('padding', '15px');
  
              $(tdEng).html(tableData[j].en_text);
              $(tdRus).html(tableData[j].ru_text);
  
              $(tr).append(tdEng, tdRus);
  
              $('.itable').append(tr);
            }
          });
        }
      } else {
        const p = document.createElement('p');
        $(p).text(data.translate);
        $(p).css('font-family', '"Quicksand", sans-serif')
        $('.itheory-translate').append(p);
      }
      

      autoRun();
    });
  }

  function renderEngToRus(data) {
    hideBlock(theory);
    showBlock(engToRus, 'flex');
    hideBlock(rusToEng);
    hideBlock(audio);
    hideBlock(write);

    $('.idone').css('display', 'none');

    $('.itable').css('display', 'none');

    $('.hourglass__wrap').css('opacity', '1');

    let rightAnswerIndex = 0;
    rightAnswerId = data.right_answer_id;

    const warningTimer = setTimeout(() => {notActiveSystem('iengToRus', rightAnswerIndex)}, 40000);


    mainProgressBar.then(result => {
      $(".vc_bar_val").html(result[0] + "%");
      $('.vc_bar').attr('data-percentage-value', result[0]);
      $('.vc_bar').attr('style', "min-width: 3em; width: " + result[0] + "%;");
      $('.vc_bar').attr('data-value', result[0]);

      // $('.iprogress__bar-inner').css('width', `${result[0]}%`);
      //
      // $('.iprogress__bar-inner').text(`${result[0]}%`);
    });

    $('.iengToRus-question').text(data.params.question);

    $('.iengToRus__wrap-item').each(function() {$(this).remove()});

    for (let index = 0; index < 4; index++) {
      const div = document.createElement('div');
      $(div).addClass('iengToRus__wrap-item');
      $(div).html(`<p>${index + 1}</p><p>${data.params.variants[index]}</p>`);
      $('.iengToRus__wrap').append(div);

      if (data.params.variants[index] === data.params.answer) {
        rightAnswerIndex = index;
        $(div).click(async function() {
          clearInterval(warningTimer);
          $(this).addClass('iengToRus-right');
          mainSettings.then(result => {if (result.autoPlayAudio === '1') {
            const audio = new Audio(data.audio);
            setTimeout(() => {audio.play()}, 1000);
          }});
          setTimeout(async () => {await next()}, 2000);
        });
      } else {
        $(div).click(function() {
          $('.iengToRus__wrap-item').each(function() {
            $(this).removeClass('iengToRus-wrong iengToRus-right');
          });
  
          const rightElement = document.querySelectorAll('.iengToRus__wrap-item')[rightAnswerIndex];
  
          $(rightElement).addClass('iengToRus-right');
  
          document.querySelectorAll('.iengToRus__wrap-item').forEach((item, i) => {
            if (i !== rightAnswerIndex) {$(item).addClass('iengToRus-wrong')};
          });

          setTimeout(() => {
            $.ajax({
              type: 'GET',
              url: `/exercises-api-methods/answer?id=${basePageId}&cursor_id=${rightAnswerId}`,
              success: function(result) {
                step = result.type;
                conditions(result);
              }
            });
          }, 2000);
        });
      }
    }
  }

  function renderRusToEng(data) {
    hideBlock(theory);
    hideBlock(engToRus);
    showBlock(rusToEng, 'flex');
    hideBlock(audio);
    hideBlock(write);

    $('.idone').css('display', 'none');

    $('.itable').css('display', 'none');

    $('.hourglass__wrap').css('opacity', '1');
    
    let rightAnswerIndex = 0;
    rightAnswerId = data.right_answer_id;

    const warningTimer = setTimeout(() => {notActiveSystem('irusToEng', rightAnswerIndex)}, 40000);


    mainProgressBar.then(result => {
      $(".vc_bar_val").html(result[0] + "%");
      $('.vc_bar').attr('data-percentage-value', result[0]);
      $('.vc_bar').attr('style', "min-width: 3em; width: " + result[0] + "%;");
      $('.vc_bar').attr('data-value', result[0]);

      // $('.iprogress__bar-inner').css('width', `${result[0]}%`);
      // $('.iprogress__bar-inner').text(`${result[0]}%`);
    });

    $('.irusToEng-question').text(data.params.question);

    $('.irusToEng__wrap-item').each(function() {$(this).remove()});

    for (let index = 0; index < 4; index++) {
      const div = document.createElement('div');
      $(div).addClass('irusToEng__wrap-item');
      $(div).html(`<p>${index + 1}</p><p>${data.params.variants[index]}</p>`);
      $('.irusToEng__wrap').append(div);

      if (data.params.variants[index] === data.params.answer) {
        rightAnswerIndex = index;
        $(div).click(async function() {
          clearInterval(warningTimer);
          $(this).addClass('iengToRus-right');
          mainSettings.then(result => {if (result.autoPlayAudio === '1') {
            const audio = new Audio(data.audio);
            setTimeout(() => {audio.play()}, 1000);
          }});
          setTimeout(async () => {await next()}, 2000);
        });
      } else {
        $(div).click(function() {
          $('.irusToEng__wrap-item').each(function() {
            $(this).removeClass('iengToRus-wrong iengToRus-right');
          });
  
          const rightElement = document.querySelectorAll('.irusToEng__wrap-item')[rightAnswerIndex];
  
          $(rightElement).addClass('iengToRus-right');
  
          document.querySelectorAll('.irusToEng__wrap-item').forEach((item, i) => {
            if (i !== rightAnswerIndex) {$(item).addClass('iengToRus-wrong')};
          });

          setTimeout(() => {
            $.ajax({
              type: 'GET',
              url: `/exercises-api-methods/answer?id=${basePageId}&cursor_id=${rightAnswerId}`,
              success: function(result) {
                step = result.type;
                conditions(result);
              }
            });
          }, 2000);
        });
      }
    }
  }

  function renderAudio(data) {
    hideBlock(theory);
    hideBlock(engToRus);
    hideBlock(rusToEng);
    showBlock(audio, 'flex');
    hideBlock(write);

    $('.idone').css('display', 'none');

    $('.itable').css('display', 'none');

    $('.hourglass__wrap').css('opacity', '1');

    let rightAnswerIndex = 0;
    rightAnswerId = data.right_answer_id;

    const warningTimer = setTimeout(() => {notActiveSystem('iaudio', rightAnswerIndex)}, 40000);

    console.log(data);


    mainProgressBar.then(result => {
      // $('.iprogress__bar-inner').css('width', `${result[0]}%`);
      // $('.iprogress__bar-inner').text(`${result[0]}%`);
      $(".vc_bar_val").html(result[0] + "%");
      $('.vc_bar').attr('data-percentage-value', result[0]);
      $('.vc_bar').attr('style', "min-width: 3em; width: " + result[0] + "%;");
      $('.vc_bar').attr('data-value', result[0]);


      const audio = new Audio(data.audio);

      $('.itheory-audio').remove();
      $('.iaudio-listen').remove();
      const audioBtn = document.createElement('img');
      audioBtn.src = "/static/img/gromko.png";
      $(audioBtn).addClass('iaudio-listen');
      $('.iaudio__question').append(audioBtn);
      $('.iaudio-listen').click(function () {audio.play()});
    });

    $('.iaudio__wrap-item').each(function() {$(this).remove()});

    for (let index = 0; index < 4; index++) {
      const div = document.createElement('div');
      $(div).addClass('iaudio__wrap-item');
      $(div).html(`<p>${index + 1}</p><p>${data.params.variants[index]}</p>`);
      $('.iaudio__wrap').append(div);

      if (data.params.variants[index] === data.params.answer) {
        rightAnswerIndex = index;
        $(div).click(async function() {
          clearInterval(warningTimer);
          $(this).addClass('iengToRus-right');
          mainSettings.then(result => {if (result.autoPlayAudio === '1') {
            const audio = new Audio(data.audio);
            setTimeout(() => {audio.play()}, 1000);
          }});
          setTimeout(async () => {await next()}, 2000);
        });
      } else {
        $(div).click(function() {
          $('.iaudio__wrap-item').each(function() {
            $(this).removeClass('iengToRus-wrong iengToRus-right');
          });
  
          const rightElement = document.querySelectorAll('.iaudio__wrap-item')[rightAnswerIndex];
  
          $(rightElement).addClass('iengToRus-right');
  
          document.querySelectorAll('.iaudio__wrap-item').forEach((item, i) => {
            if (i !== rightAnswerIndex) {$(item).addClass('iengToRus-wrong')};
          });

          setTimeout(() => {
            $.ajax({
              type: 'GET',
              url: `/exercises-api-methods/answer?id=${basePageId}&cursor_id=${rightAnswerId}`,
              success: function(result) {
                step = result.type;
                conditions(result);
              }
            });
          }, 2000);
        });
      }
    }
  };

  function renderWrite(data) {
    hideBlock(theory);
    hideBlock(engToRus);
    hideBlock(rusToEng);
    hideBlock(audio);
    showBlock(write, 'block');

    $('.idone').css('display', 'none');

    $('.itable').css('display', 'none');

    $('.hourglass__wrap').css('opacity', '1');

    let activeWordIndex = 0;

    rightAnswerId = data.right_answer_id;

    const warningTimer = setTimeout(() => {
      $.ajax({
        type: 'GET',
        url: `/exercises-api-methods/answer?id=${basePageId}&cursor_id=${rightAnswerId}`,
        success: function(result) {
          step = result.type;
          conditions(result);
        }
      });
    }, 40000);

    mainProgressBar.then(result => {
      // $('.iprogress__bar-inner').css('width', `${result[0]}%`);
      // $('.iprogress__bar-inner').text(`${result[0]}%`);

      $(".vc_bar_val").html(result[0] + "%");
      $('.vc_bar').attr('data-percentage-value', result[0]);
      $('.vc_bar').attr('style', "min-width: 3em; width: " + result[0] + "%;");
      $('.vc_bar').attr('data-value', result[0]);

    });

    console.log(data);

    $('.iwrite-question').text(data.params.question);

    $('.iwrite__wrap-item').each(function() {$(this).remove()});

    $('.iwrite__btns-item').each(function() {$(this).remove()});

    for (let index = 0; index < data.params.answer.length; index++) {
      const div = document.createElement('div');
      $(div).addClass('iwrite__wrap-item');
      $('.iwrite__wrap').append(div);
    }

    String.prototype.shuffle = function () {
      const a = this.split(''),
          n = a.length;
      
      for (let i = n - 1; i > 0; i--) {
          const j = Math.floor(Math.random() * (i + 1));
          const tmp = a[i];
          a[i] = a[j];
          a[j] = tmp;
      }
      return a.join('');
    }
  
    String.prototype.shuffleWords = function () {
      const a = this.split(' '),
          n = a.length;
      
      for (let index = 0; index < n; index++) {
          a[index] = a[index].shuffle();
      }
      return a.join(' ');
    }

    const firstArr = data.params.answer.split('');
    let answerArr = data.params.answer.shuffleWords();
    answerArr = answerArr.split('');
    let countOfErrors = 0;

    for (let index = 0; index < answerArr.length; index++) {
      const btn = document.createElement('div');
      $(btn).addClass('iwrite__btns-item');
      $(btn).text(answerArr[index].toUpperCase());
      $('.iwrite__btns').append(btn);
    }

    document.addEventListener('keydown', function(event) {
      try {
        clearInterval(warningTimer);
        
        if (event.key.toUpperCase() === firstArr[activeWordIndex].toUpperCase()) {
          const elements = document.querySelectorAll('.iwrite__wrap-item');
          $(elements[activeWordIndex]).text(firstArr[activeWordIndex].toUpperCase());

          const nextWord = firstArr[activeWordIndex].toUpperCase();
          const nextBtn = document.querySelectorAll('.iwrite__btns-item');
          
          $(nextBtn).each(function() {
            if ($(this).text().toUpperCase() === nextWord) {$(this).remove()};
          });
          
          if (activeWordIndex + 1 === firstArr.length) {
            $('.iwrite__wrap-item').css('background', 'rgb(91, 175, 89)');
            mainSettings.then(result => {if (result.autoPlayAudio === '1') {
              const audio = new Audio(data.audio);
              setTimeout(() => {audio.play()}, 1000);
              setTimeout(async () => {await next()}, 2000);
            }});
          }
  
          activeWordIndex++;
          $('.iwrite__btns-item').each(function() {
            $(this).css('background', '#ebeef0');
          });
        } else {
          const nextWord = firstArr[activeWordIndex].toUpperCase();
          const nextBtn = document.querySelectorAll('.iwrite__btns-item');
          
          $(nextBtn).each(function() {
            if ($(this).text().toUpperCase() === nextWord) {
              $(this).css('background', '#f57171');
            }
          });

          if (countOfErrors === 2) {
            $.ajax({
              type: 'GET',
              url: `/exercises-api-methods/answer?id=${basePageId}&cursor_id=${rightAnswerId}`,
              success: function(result) {
                step = result.type;
                conditions(result);
              }
            });
          }

          countOfErrors++;
        }
      } catch (e) {console.log('');}
    });

    $('.iwrite__btns-item').each(function() {
      $(this).click(function() {
        clearInterval(warningTimer);
        if ($(this).text() === firstArr[activeWordIndex].toUpperCase()) {
          const elements = document.querySelectorAll('.iwrite__wrap-item');
          $(elements[activeWordIndex]).text(firstArr[activeWordIndex].toUpperCase());
          $(this).remove();
          
          if (activeWordIndex + 1 === firstArr.length) {
            $('.iwrite__wrap-item').css('background', 'rgb(91, 175, 89)');
            mainSettings.then(result => {if (result.autoPlayAudio === '1') {
              const audio = new Audio(data.audio);
              setTimeout(() => {audio.play()}, 1000);
            }});
            setTimeout(async () => {await next()}, 2000);
          }

          activeWordIndex++;
          $('.iwrite__btns-item').each(function() {
            $(this).css('background', '#ebeef0');
          });
        } else {
          $(this).css('background', '#f57171');

          if (countOfErrors === 2) {
            $.ajax({
              type: 'GET',
              url: `/exercises-api-methods/answer?id=${basePageId}&cursor_id=${rightAnswerId}`,
              success: function(result) {
                step = result.type;
                conditions(result);
              }
            });
          }

          countOfErrors++;
        }
      });
    });
  }

  function conditions(data) {
    mainProgressBar = getProgressBar();
    console.log(mainProgressBar);
    mainProgressBar.then(result => {
      if (result[0] === '100') {
        hideBlock(theory);
        hideBlock(engToRus);
        hideBlock(rusToEng);
        hideBlock(audio);
        hideBlock(write);
        $('.hourglass__wrap').css('opacity', '0');
        $('.iprogress__bar-inner').css('width', `${100}%`);
        $('.iprogress__bar-inner').css('border-radius', `30px`);
        $('.iprogress__bar-inner').text(`${100}%`);
        $('.idone').css('display', 'flex');
      } else {
        if (step === 1) { 
          renderTheory(data);
        } else if (step === 2) {
          renderEngToRus(data);
        } else if (step === 3) {
          renderRusToEng(data);
        } else if (step === 4) {
          renderAudio(data);
        } else if (step === 5) {
          renderWrite(data);
        }
      }
    });
  }
  
  async function firstRender() {
    await $.ajax({
      type: 'GET',
      url: `/exercises-api-methods/current?id=${basePageId}`,
      success: function(result) {
        step = result.type;
        conditions(result);
        console.log(result, basePageId);
      }
    });
  }
  firstRender();

  $('.settings_wrap div').click(function() {
    $('.settings__content').toggleClass('settings-active');
  });
});